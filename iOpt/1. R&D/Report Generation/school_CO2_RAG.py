# Imports

import psycopg2
import psycopg2.extras
import datetime as dt
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from dateutil.relativedelta import relativedelta
import os
from sys import exit

# Creating directory to hold device reading files
# if directory exists there will be an OS error, delete or move directory.
path = 'school_data'
    
try: 
    os.mkdir(path) 
except OSError as error: 
    print(error)
    exit ()

Unique_Refs = pd.read_csv('C:/Users/tomda/Desktop/full_schools_list.csv')

Unique_Refs_values = Unique_Refs['School'].values

today = dt.datetime.today()
one_week_ago = (today + relativedelta(weeks=-1)).strftime('%Y-%m-%d')

# Only for today


#today = '2022-09-01'

#one_week_ago = '2022-08-26'

# Pull lookup tables
# Only pulling required coloumns

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine, columns = ['Id', 'PropertyId', 'Name', 'RoomType', 'SensorId'])
sensors_df = pd.read_sql_table('Sensors', engine, columns = ['Id', 'DeviceEUI'])
properties_df = pd.read_sql_table('Properties', engine, columns = ['Id', 'LandlordId', 'UniqueReference', 'AddressId', ])
addresses_df = pd.read_sql_table('Addresses', engine, columns = ['Id', 'AddressLine1', 'AddressLine2', 'Code', 'City'])

# Get property Ids

print('Finding properties...')

props = pd.DataFrame()

for name in Unique_Refs_values:
    prop_add = properties_df.loc[properties_df['UniqueReference'] == name, ['Id', 'UniqueReference']]
    props = pd.concat([props, prop_add])

props

print('Found', len(props), 'properties.')

# Get list of Devices and readings from property list
print('Collecting sensor data...')
for prop_id in props['Id']:
    

    out_room = rooms_df.query('PropertyId in @prop_id')
    sensors = out_room.loc[:,'SensorId']
    out_sensor = sensors_df.query('Id in @sensors')
    out_sensor['DeviceEUI'].value_counts()
    devices = out_sensor.loc[:,'DeviceEUI']
    device = tuple(devices)
    #print(device)
    
    conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    
    if len(device) == 1:
        query = "SELECT device, time, co2, humidity, temperature FROM readings WHERE time BETWEEN '{one_week_ago}' AND '{today}' AND device = '{device[0]}'".format(device=device, one_week_ago=one_week_ago, today=today)
    elif len(device) > 1:
        query = "SELECT device, time, co2, humidity, temperature FROM readings WHERE time BETWEEN '{one_week_ago}' AND '{today}' AND device IN {device}".format(device=device, one_week_ago=one_week_ago, today=today)
  

    cursor.execute(query, [tuple(device)])
    outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
    with open('school_data/' + prop_id + '.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)

    conn.close()
    
# Combine all devices into one dataframe

all_devs = pd.DataFrame()
for prop in props['Id']:
    prop_devs = pd.read_csv('school_data/' + prop +'.csv')
    all_devs = pd.concat([all_devs, prop_devs])

print('Collected', len(all_devs), 'readings from', all_devs['device'].nunique(), 'devices.')

# Write or read device list to file if required
# all_devs.to_csv('all_devs.csv')
# all_devs = pd.read_csv('all_devs.csv')


all_devs.rename(columns={'device': 'DeviceEUI'}, inplace = True)
edin_schools = all_devs.merge(sensors_df, how = 'inner')
edin_schools.rename(columns={'Id': 'SensorId'}, inplace = True)
edin_schools = edin_schools.merge(rooms_df, how = 'inner')
edin_schools.rename(columns={'Id': 'RoomId'}, inplace = True)
properties_df.rename(columns={'Id': 'PropertyId'}, inplace = True)
edin_schools = edin_schools.merge(properties_df, how = 'inner')
addresses_df.rename(columns={'Id': 'AddressId'}, inplace = True)
edin_schools = edin_schools.merge(addresses_df, how = 'left')

edin_schools['Address'] = edin_schools['AddressLine1'].map(str) + ' ' + edin_schools['AddressLine2'].map(str) + ' ' + edin_schools['City'].map(str) + ' ' + edin_schools['Code'].map(str)

conditions = [
    ((edin_schools['temperature'] == -40) | (edin_schools['humidity'] == 0)),
    #((edin_schools['temperature'].lt(0)) | (edin_schools['temperature'].ge(40)) | (edin_schools['humidity'].lt(15)) | (edin_schools['co2'].lt(250))),
    ((edin_schools['temperature'].lt(0)) | (edin_schools['temperature'].ge(40)) | (edin_schools['humidity'].lt(15))),
    (edin_schools['co2'].ge(10000))
]


# Checking of erroneous data
choices = ['500 Error', 'General Sensor Fault', 'CO2 Module Fault']

edin_schools['error'] = np.select(conditions, choices, default=np.nan)

#faults =  edin_schools.loc[edin_schools['error'] != 'nan']
faults = edin_schools.loc[np.where(edin_schools['error'].isin(choices))]
no_fault =  edin_schools.loc[edin_schools['error'] == 'nan']

device_fault = faults['DeviceEUI'].unique()
edin_schools = edin_schools[edin_schools.DeviceEUI.isin(device_fault) == False]
print(len(device_fault), 'devices with errors found.')

print('Erroneous devices removed leaving', len(edin_schools['DeviceEUI'].unique()), 'devices.')

edin_schools = edin_schools[['time', 'co2', 'Address', 'Name', 'RoomType', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

edin_schools['time'] = pd.to_datetime(edin_schools['time'])
edin_schools = edin_schools.set_index(['time'])

conditions = [
    (edin_schools['co2'].ge(800) & edin_schools['RoomType'].str.contains('use', case=False)),
    ((edin_schools['co2'].lt(700)) & edin_schools['RoomType'].str.contains('use', case=False)),
    ((edin_schools['co2'].ge(700)  & edin_schools['RoomType'].str.contains('use', case=False)) | (edin_schools['co2'].lt(800) & edin_schools['RoomType'].str.contains('use', case=False))),
    (edin_schools['co2'].ge(1400) & edin_schools['RoomType'].str.contains('lass', case=False)),
    (edin_schools['co2'].lt(1100) & edin_schools['RoomType'].str.contains('lass', case=False)),
    ((edin_schools['co2'].ge(1100)  & edin_schools['RoomType'].str.contains('lass', case=False)) | (edin_schools['co2'].lt(1400) & edin_schools['RoomType'].str.contains('lass', case=False)))
]
choices = ['Red', 'Green', 'Amber', 'Red', 'Green', 'Amber']

edin_schools['threshold'] = np.select(conditions, choices)
print(len(edin_schools.loc[edin_schools['threshold'] == '0']), 'readings not allocated rag status.')

# Limiting to school hours
edin_schools_in = edin_schools.between_time('09:00', '15:00')
edin_schools_in = edin_schools_in.loc[edin_schools_in.index.weekday < 5]

print(edin_schools_in['DeviceEUI'].nunique(), 'devices after filtering for school hours.')

edin_schools_in = edin_schools_in.reset_index()
edin_schools = edin_schools.reset_index()


def devicerag(df):
    edin_schools_counted = df.groupby(['UniqueReference', 'Name', 'RoomType', 'threshold']).size().reset_index(name='count')
    edin_schools_threshold_total = df.groupby(['UniqueReference', 'RoomType', 'Name']).size().reset_index(name='count')

    edin_schools_merged = edin_schools_counted.merge(edin_schools_threshold_total, on = ['UniqueReference', 'Name', 'RoomType'], how = 'inner')
    edin_schools_merged['percentage'] = edin_schools_merged['count_x']/edin_schools_merged['count_y']
    edin_schools_merged = edin_schools_merged[['UniqueReference', 'Name', 'RoomType', 'threshold', 'percentage']]
    edin_schools_merged = edin_schools_merged.rename(columns={"UniqueReference": "School", "Name": "Room", "RoomType": "Room Type", "threshold": "Status", "percentage": "Percentage Time"})

    edin_schools_pivoted = edin_schools_merged.pivot(index=['School', 'Room', 'Room Type'], columns = 'Status', values = 'Percentage Time')
    edin_schools_pivoted = edin_schools_pivoted[['Green', 'Amber', 'Red']]
    edin_schools_pivoted = edin_schools_pivoted.fillna(0)
    
    return edin_schools_pivoted


all_hours =  devicerag(edin_schools)
school_hours =  devicerag(edin_schools_in)

school_hours.rename(columns = {'Red':'Red (School hours)','Amber':'Amber (School hours)', 'Green':'Green (School hours)'}, inplace = True)

both = school_hours.merge(all_hours, how='left', on=['School', 'Room', 'Room Type'])

both.to_csv('Edinburgh Schools.csv')

print('Report generated')