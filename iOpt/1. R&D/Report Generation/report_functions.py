
# Returns list of all devices for a landlord
def get_devices(landlord, readings = False):

    import pandas as pd
    import datetime as dt
    from dateutil.relativedelta import relativedelta
    import numpy as np

    import psycopg2
    import psycopg2.extras
    from sqlalchemy import create_engine

    print('Finding properties...')

    engine = create_engine(
        'postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
    rooms_df = pd.read_sql_table('Rooms', engine, columns=[
        'Id', 'PropertyId', 'SensorId', 'Name', 'RoomType', 'RecordState'])
    sensors_df = pd.read_sql_table(
        'Sensors', engine, columns=['Id', 'DeviceEUI'])
    properties_df = pd.read_sql_table('Properties', engine, columns=[
                                      'Id', 'LandlordId', 'AddressId', 'UniqueReference', 'RecordState'])
    landlords_df = pd.read_sql_table('Landlords', engine, columns=[
                                     'Id', 'OrganisationName', 'RecordState'])
    addresses_df = pd.read_sql_table('Addresses', engine, columns=[
                                     'Id', 'AddressLine1', 'AddressLine2', 'Code', 'City'])

    Landlord_Id = landlords_df.loc[(landlords_df['OrganisationName'] == landlord) & (
        landlords_df['RecordState'] == 0), 'Id'].values[0]

    Property_Ids = properties_df.loc[(properties_df['LandlordId'] == Landlord_Id) & (properties_df['RecordState'] == 0), 'Id'].values

    print('Found', len(Property_Ids), 'properties.')

    # One off to get TEMP ACC from North lanarkshire council in place of line above
    #Property_Ids = properties_df.loc[(properties_df['LandlordId'] == Landlord_Id) & (properties_df['PropertyType'] == 'TEMP ACC'), 'Id'].values

    print('Finding devices...')

    out_room = rooms_df.query('PropertyId in @Property_Ids')
    sensors = out_room.loc[:, 'SensorId']
    out_sensor = sensors_df.query('Id in @sensors')
    out_sensor['DeviceEUI'].value_counts()
    devices = out_sensor.loc[:, 'DeviceEUI']
    device = tuple(devices)

    print('Found', len(devices), 'devices.')
    
    if readings:
        
        print('Getting readings...')
        conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    
        query = "SELECT device, time, temperature, humidity, co2, battery, dewpoint FROM readings WHERE device IN {device}".format(device=device)
    
        cursor.execute(query, [tuple(device)])
        outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
        print(cursor)
        with open(landlord+'_device_readings.csv', 'w') as f:
            cursor.copy_expert(outputquery, f)
    
        conn.close()
        
        print('File', landlord+'_device_readings.csv written to working directory.')
    
    return device


# Create a document containing all env sensor data, weather data, for all landlords devices.
# Also checks batteries and devices with no readings.
def monthly_report(landlord):
    
    #Hard set at moment, need long, lat look up
    weather_code = 'Glasgow'
    
    import pandas as pd
    import datetime as dt
    from dateutil.relativedelta import relativedelta
    import numpy as np
    
    import psycopg2
    import psycopg2.extras
    from sqlalchemy import create_engine
    from dash_extensions.snippets import send_data_frame
    
    from pythermalcomfort.models import pmv_ppd
    from pythermalcomfort.utilities import v_relative
    from pythermalcomfort.models import clo_tout
    
    today = dt.datetime.today()
    yesterday = (today + relativedelta(days=-1)).strftime('%Y-%m-%d')
    one_week_ago = (today + relativedelta(weeks=-1)).strftime('%Y-%m-%d')
    one_month_ago = (today + relativedelta(months=-1)).strftime('%Y-%m-%d')
    three_months_ago = (today + relativedelta(months=-3)).strftime('%Y-%m-%d')
    three_months_one_week_ago = (
        today + relativedelta(months=-3, weeks=-1)).strftime('%Y-%m-%d')
    
    print('Finding properties...')
    
    engine = create_engine(
        'postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
    rooms_df = pd.read_sql_table('Rooms', engine, columns=[
        'Id', 'PropertyId', 'SensorId', 'Name', 'RoomType', 'RecordState'])
    sensors_df = pd.read_sql_table(
        'Sensors', engine, columns=['Id', 'DeviceEUI'])
    properties_df = pd.read_sql_table('Properties', engine, columns=[
                                      'Id', 'LandlordId', 'AddressId', 'UniqueReference', 'RecordState'])
    landlords_df = pd.read_sql_table('Landlords', engine, columns=[
                                     'Id', 'OrganisationName', 'RecordState'])
    addresses_df = pd.read_sql_table('Addresses', engine, columns=[
                                     'Id', 'AddressLine1', 'AddressLine2', 'Code', 'City'])
    
    Landlord_Id = landlords_df.loc[(landlords_df['OrganisationName'] == landlord) & (
        landlords_df['RecordState'] == 0), 'Id'].values[0]
    
    Property_Ids = properties_df.loc[(properties_df['LandlordId'] == Landlord_Id) & (properties_df['RecordState'] == 0), 'Id'].values
    
    print('Found', len(Property_Ids), 'properties.')
    
    # One off to get TEMP ACC from North lanarkshire council in place of line above
    #Property_Ids = properties_df.loc[(properties_df['LandlordId'] == Landlord_Id) & (properties_df['PropertyType'] == 'TEMP ACC'), 'Id'].values
    
    print('Finding devices...')
    
    out_room = rooms_df.query('PropertyId in @Property_Ids')
    sensors = out_room.loc[:, 'SensorId']
    out_sensor = sensors_df.query('Id in @sensors')
    out_sensor['DeviceEUI'].value_counts()
    devices = out_sensor.loc[:, 'DeviceEUI']
    device = tuple(devices)
    
    print('Found', len(devices), 'devices.')
    
    print('Getting readings...')
    conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    
    query = "SELECT device, time, temperature, humidity, co2, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_one_week_ago}' AND '{yesterday}' AND device IN {device}".format(
        device=device, three_months_one_week_ago=three_months_one_week_ago, yesterday=yesterday)
    
    cursor.execute(query, [tuple(device)])
    outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
    print(cursor)
    with open(landlord+'_device_readings.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
    
    conn.close()
    
    
    device_readings = pd.read_csv(landlord+'_device_readings.csv', parse_dates=['time'])
    meta = {}
    
    # Adding propertyid
    sensors_df.rename(columns={'DeviceEUI': 'device'}, inplace=True)
    device_readings = device_readings.merge(sensors_df[['Id', 'device']])
    
    device_readings.rename(columns={'Id': 'SensorId'}, inplace=True)
    device_readings = device_readings.merge(rooms_df[['SensorId', 'PropertyId']])
    
    live_sensors = device_readings['device'].nunique()
    meta['Live sensors'] = live_sensors
    
    live_properties = device_readings['PropertyId'].nunique()
    meta['Live proprties'] = live_properties
    
    devlist = devices.values.tolist()
    readlist = device_readings['device'].unique().tolist()
    no_reading = list(set(devlist) - set(readlist))
    
    meta['No. of no reading'] = len(no_reading)
    
    print('Collected', len(device_readings), 'readings from', live_sensors, 'sensors in', live_properties, 'properties.')
    if no_reading != 0:
        print(len(no_reading), 'returned no readings.')
        with open(landlord+'_no_reading.txt', 'w') as filehandle:
            for listitem in no_reading:
                filehandle.write('%s\n' % listitem)
        print('Device ids written to file:', landlord+'_no_reading.txt')
    print()
    print('Readings written to file:', landlord+'_device_readings.csv')
    
    # Drop obviously erronious values
    device_readings = device_readings.drop(device_readings[device_readings.temperature > 50].index)
    device_readings = device_readings.drop(device_readings[device_readings.co2 > 6000].index)
    
    no_of_readings = len(device_readings)
    
    device_readings['time'] = pd.to_datetime(device_readings['time'].round('min'))
    
    #device_readings = device_readings.drop_duplicates(subset=['time', 'device'], keep='last')
    
    #device_readings = device_readings.set_index(['time'])
    
    #print(no_of_readings - len(device_readings),
    #      'duplicates found and removed.')
    
    device_readings = device_readings.reset_index()
    device_readings = device_readings.drop_duplicates(
        subset=['time', 'device'], keep='last')
    device_readings = device_readings.set_index(['time'])
    
    print(no_of_readings - len(device_readings), 'duplicates found and removed.')
    
    def resample_add_grads(df):
        if len(df) < 50:
            print('Sensor', df['device'][0], 'in property', df['PropertyId'][0], 'has only', len(df), 'readings.')
        else:
            df = df.resample('1min').interpolate(method='time')
            df = df.resample('15min').interpolate(method='time')
            df['temp_grad'] = np.gradient(df['temperature'])
            df['co2_grad'] = np.gradient(df['co2'])
            df['hum_grad'] = np.gradient(df['humidity'])
            # Thermal comfort
            v_r = v_relative(v=0.1, met=1.2)
            results = pmv_ppd(tdb=df['temperature'], tr=df['temperature'], vr=v_r, rh=df['humidity'], met=1.2, clo=clo_tout(tout=df['temperature']), wme=0, standard="ISO")
            results = pd.DataFrame.from_dict(results)
            results.index = df.index 
            df = df.join(results)
    
            df = df.loc[(df.index > three_months_ago)]
            df = df.loc[(df.index < yesterday)]
            return df
        
    print('Grouping and resampling data...')

    device_readings = device_readings.drop_duplicates()

    device_readings = device_readings.groupby(
         'device').apply(resample_add_grads)

    device_readings.drop('device', axis=1, inplace=True)
    device_readings.reset_index(inplace=True)
    device_readings.set_index('time', inplace=True)
     
    # Rule based occupancy
    device_readings['Presence_Prediction'] = np.where(((device_readings.co2 > 500)&(device_readings.co2_grad > 5) | (device_readings.co2_grad > 50)) ,1, 0)
     
     
    print('Collecting power data...')

    # # Pull tables in from database
    engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
    # power_df = pd.read_sql_table('solar_readings', engine)
    # # milliamps x 230V then / 1000 / 1000 to get kW then / 2 to take average over an hour to make kWh
     
    # power = power_df.loc[np.where(power_df['property_id'].isin(Property_Ids))]

    # power['reading'] = abs(power['reading']) * 230 / 1000 / 1000 / 2
     
    # # Could check batteries before dropping as with env sensors
    # power = power[['reading', 'timestamp', 'device_id', 'energy_value']]
   
    # no_of_power = len(power)
    # print(no_of_power, 'power readings from', power['device_id'].nunique(), 'devices.')

    # power['timestamp'] = pd.to_datetime(power['timestamp']).round('min')

    # power = power.drop_duplicates(subset = ['timestamp', 'device_id'], keep = 'last')
    # power = power.set_index(['timestamp'])

    # print(no_of_power - len(power), 'duplicates found and removed.')    
     
    # def power_resample(df):
    #     df = df.resample('1min').interpolate(method='time')
    #     df = df.resample('15min').interpolate(method='time')
    #     df['reading_grad'] = np.gradient(df['reading'])
    #     # add proper date cut off here
    #     df = df.loc[(df.index > three_months_ago)]
    #     df = df.loc[(df.index < yesterday)]
    #     return df
    # print('Grouping qnd resampling data...')
    # # # Could do by property ID.
    # power_15 = power.groupby('device_id').apply(power_resample)

    # power_15 =power_15.droplevel(0, axis=0)

    # power_15.drop('device_id', axis=1, inplace = True)


    # power_15.reset_index(inplace=True)
    # power_15['timestamp'] = pd.to_datetime(power_15['timestamp'])
    # power_15['timestamp'] = power_15['timestamp'].dt.date
    # power_15.set_index('timestamp', inplace = True)
    # power_15.index = pd.to_datetime(power_15.index).tz_localize('Etc/UCT')
     
    # device_readings = pd.merge(
    #     device_readings, power_15, left_index=True, right_index=True)
    
    print('Collecting weather data...')
     
    weather_df = pd.read_sql_table('weather', engine, columns=[
                                    'time', 'code', 'temp', 'humidity', 'pressure'])

    # Needs to get code from readings data, hard coded as Plymuth for the moment.
    local_weather = weather_df[weather_df['code'] == weather_code]

    local_weather = local_weather.loc[(
        local_weather['time'] > three_months_one_week_ago)]
    local_weather = local_weather.set_index('time')

    # Dropping  columns not required
    local_weather = local_weather[['temp', 'humidity', 'pressure']]
    local_weather = local_weather.add_suffix('_external')

    # Rounding down to 1min accuracy
    local_weather.index = pd.to_datetime(local_weather.index).round('min')

    local_weather_1min = local_weather.resample(
        '1min').interpolate(method='time')
    local_weather_15min = local_weather_1min.resample(
        '15min').interpolate(method='time')
    #Failing here due to one reading
    local_weather_15min['temp_grad_external'] = np.gradient(
        local_weather_15min['temp_external'])
    local_weather_15min['press_grad_external'] = np.gradient(
        local_weather_15min['pressure_external'])
    local_weather_15min['hum_grad_external'] = np.gradient(
        local_weather_15min['humidity_external'])

    local_weather_15min_month = local_weather_15min.loc[(
        local_weather_15min.index > one_month_ago)]
    local_weather_15min_month = local_weather_15min_month.loc[(
        local_weather_15min_month.index < yesterday)]

    # Could merge checking address for weather station at this point
    device_readings = pd.merge(
        device_readings, local_weather_15min, left_index=True, right_index=True)

    print('Aggregating data..')
    # Need to drop ['SensorId', 'PropertyId'] to avoid warnings.
    #device_readings.drop(['SensorId', 'PropertyId'], inplace=True)
     
    # Need to drop ['SensorId', 'PropertyId'] to avoid warnings.
    device_readings.drop(['SensorId', 'PropertyId'], axis=1, inplace=True)
    
    device_readings_agg = device_readings.groupby('device').aggregate(
        ['max', 'mean', 'min', 'median', 'std', 'var', 'count'])
    device_readings_agg.columns = device_readings_agg.columns.get_level_values(
        0) + '_' + device_readings_agg.columns.get_level_values(1)
    device_readings_agg = device_readings_agg.add_suffix('_3m')
    
    device_readings_1m = device_readings.loc[device_readings.index > one_month_ago]
    device_readings_agg_1m = device_readings_1m.groupby('device').aggregate(
        ['max', 'mean', 'min', 'median', 'std', 'var', 'count'])
    device_readings_agg_1m.columns = device_readings_agg_1m.columns.get_level_values(
        0) + '_' + device_readings_agg_1m.columns.get_level_values(1)
    device_readings_agg_1m = device_readings_agg_1m.add_suffix('_1m')
    
    
    
    full_device_agg = pd.merge(
        device_readings_agg, device_readings_agg_1m, left_index=True, right_index=True)
    
    hum_corr = device_readings.groupby(
        'device')[['humidity', 'humidity_external']].corr().unstack().iloc[:, 1]
    hum_corr = hum_corr.rename('hum corr')
    temp_corr = device_readings.groupby(
        'device')[['temperature', 'temp_external']].corr().unstack().iloc[:, 1]
    temp_corr = temp_corr.rename('temp corr')
    full_device_agg = pd.concat([full_device_agg, hum_corr, temp_corr], axis=1)
    
    full_device_agg.reset_index(inplace=True)
    
    full_device_agg = full_device_agg.merge(sensors_df, how='inner')
    full_device_agg.rename(columns={'Id': 'SensorId'}, inplace=True)
    full_device_agg = full_device_agg.merge(rooms_df, how='inner')
    full_device_agg.rename(columns={'Id': 'RoomId'}, inplace=True)
    properties_df.rename(columns={'Id': 'PropertyId'}, inplace=True)
    full_device_agg = full_device_agg.merge(properties_df, how='inner')
    addresses_df.rename(columns={'Id': 'AddressId'}, inplace=True)
    full_device_agg = full_device_agg.merge(addresses_df, how='left')
    full_device_agg['Address'] = full_device_agg['AddressLine1'].map(str) + ' ' + full_device_agg['AddressLine2'].map(
        str) + ' ' + full_device_agg['City'].map(str) + ' ' + full_device_agg['Code'].map(str)
    
    
    battery = full_device_agg[['battery_mean_1m', 'battery_max_1m'	,'battery_mean_1m', 'battery_min_1m',
               'battery_median_1m', 'battery_std_1m', 'battery_var_1m', 'battery_max_3m', 'battery_mean_3m', 
               'battery_min_3m', 'battery_median_3m', 'battery_std_3m', 'battery_var_3m']].loc[(
    full_device_agg['battery_mean_1m'] < 3)]
    
    if len(battery) == 0:
        print('All batteries above 3')
    else:
        print(len(battery), 'batteries < 3')
        battery.to_csv(landlord+'_batteries.csv')
    
    meta['No. of low batteries'] = len(battery)
    
    # Creating parameteres for 'Issues' column
    conditions = [
        (full_device_agg['co2_mean_1m'].ge(600) &
         full_device_agg['temperature_mean_1m'].lt(14)),
        (full_device_agg['humidity_mean_1m'].ge(70) & full_device_agg['temperature_mean_1m'] -
         full_device_agg['dewpoint_mean_1m'].lt(4) & full_device_agg['co2_mean_1m'].ge(1500)),
        (full_device_agg['humidity_mean_1m'].ge(
           65) & full_device_agg['temperature_mean_1m'] - full_device_agg['dewpoint_mean_1m'].lt(5)),
        (full_device_agg['co2_mean_1m'].ge(1300)),
        (full_device_agg['co2_mean_1m'].lt(500) &
         full_device_agg['temperature_mean_1m'].lt(13)),
        (full_device_agg['temperature_mean_1m'].ge(27))
    ]
    choices = ['fuel poverty / heating issue', 'mould',
                'damp', 'poor vent', 'low temp', 'high temp']
    
    full_device_agg['issue'] = np.select(conditions, choices)
    
    # Creating parameteres for 'Severity' column
    conditions = [
        (full_device_agg['co2_mean_1m'].ge(1600) | (full_device_agg['temperature_mean_1m'].lt(
            14) & full_device_agg['co2_mean_1m'].ge(550)) | full_device_agg['humidity_mean_1m'].ge(70)),
        (full_device_agg['co2_mean_1m'].ge(1300) | (full_device_agg['temperature_mean_1m'].lt(
            15) & full_device_agg['co2_mean_1m'].ge(550)) | full_device_agg['humidity_mean_1m'].ge(65))
    ]
    choices = ['critical', 'medium']
    
    full_device_agg['severity'] = np.select(conditions, choices)
    
    meta['No. of critical'] = full_device_agg['severity'].value_counts()['critical']
    meta['No. of medium'] = full_device_agg['severity'].value_counts()['medium']
    
    full_device_agg.set_index('UniqueReference')
    
    #dropping unrequired coloumns (Whie list or black list?)
    #black list
    to_drop = ['battery_max_1m'	, 'battery_mean_1m', 'battery_min_1m',
               'battery_median_1m', 'battery_std_1m', 'battery_var_1m',
               'battery_count_1m', 'battery_max_3m', 'battery_mean_3m', 
               'battery_min_3m', 'battery_median_3m', 'battery_std_3m',
               'battery_var_3m', 'battery_count_3m', 'dewpoint_count_3m',
               'hum_grad_count_3m', 'Presence_Prediction_max_3m',
               'Presence_Prediction_min_3m', 'Presence_Prediction_median_3m', 'Presence_Prediction_std_3m',
               'Presence_Prediction_var_3m', 'Presence_Prediction_count_3m', 'temp_external_count_3m',
               'humidity_external_count_3m', 'pressure_external_count_3m', 'temp_grad_external_count_3m',
               'press_grad_external_count_3m', 'hum_grad_external_count_3m', 'humidity_count_1m',
               'co2_count_1m', 'battery_max_1m', 'battery_mean_1m', 'battery_min_1m', 'battery_median_1m',
               'battery_std_1m', 'battery_var_1m', 'battery_count_1m', 'dewpoint_count_1m', 'temp_grad_count_1m',
               'co2_grad_count_1m', 'hum_grad_count_1m', 'Presence_Prediction_max_1m',
               'Presence_Prediction_min_1m', 'Presence_Prediction_median_1m', 'Presence_Prediction_std_1m', 
               'Presence_Prediction_var_1m', 'Presence_Prediction_count_1m', 'temp_external_count_1m',
               'humidity_external_count_1m', 'pressure_external_count_1m', 'temp_grad_external_count_1m',
               'press_grad_external_count_1m', 'hum_grad_external_count_1m', 'humidity_count_3m']
    
    full_device_agg.drop(columns=to_drop, inplace=True)
    
    #white list
    #to_keep = []    
    #full_device_agg = full_device_agg[to_keep]
    
    
    full_device_agg.to_csv(landlord+'_report.csv')
    print('Meta data written to file:', landlord+'_report.csv')
    
    summary = full_device_agg[['AddressLine1', 'AddressLine2', 'Name', 'temperature_mean_3m', 'temperature_mean_1m',
                                 'dewpoint_mean_3m', 'dewpoint_mean_1m', 'humidity_mean_3m', 'humidity_mean_1m',
                                 'co2_mean_3m', 'co2_mean_1m', 'hum corr', 'temp corr',
                                 'issue', 'severity', 'Presence_Prediction_mean_3m', 'Presence_Prediction_mean_1m']]
    
    meta_s = pd.Series(meta)
    
    
    with pd.ExcelWriter(landlord + '_summary.xlsx') as writer:
        meta_s.to_excel(writer, sheet_name='Meta data')
        summary.to_excel(writer, sheet_name='Features')
    
    #summary.to_csv(landlord+'_summary.csv')
    print('Selected data written to file:', landlord+'_summary.xlsx')
    
# Creates an insights doc giving rooms ranked by several metrics.
def insights(landlord):
    import pandas as pd
    
    # Setting up cut-offs 
    high_temp_min = 32
    high_temp_mean = 26
    low_temp_mean = 18
    low_temp_max = 15
    
    high_hum_min = 85
    high_hum_mean = 75
    
    high_co2_mean = 1400
    high_co2_max = 2000
    
    hum_corr = 0.6
    temp_corr = 0.6
    
    mean_temp_diff_min = 3
    mean_hum_diff_min = 10
    mean_co2_diff_min = 1000
    
    
    
    #    try:
    report = pd.read_csv(landlord+'_report.csv')
    
    report = report[['temperature_max_1m', 'temperature_mean_1m', 'temperature_min_1m',
                     'temperature_median_1m', 'temperature_std_1m', 'temperature_var_1m',
                     'humidity_max_1m', 'humidity_mean_1m', 'humidity_min_1m',
                     'humidity_median_1m', 'humidity_std_1m', 'humidity_var_1m',
                     'co2_max_1m', 'co2_mean_1m', 'co2_min_1m',
                     'co2_median_1m', 'co2_std_1m', 'co2_var_1m',
                     'dewpoint_max_1m', 'dewpoint_mean_1m', 'dewpoint_min_1m', 
                     'dewpoint_median_1m', 'dewpoint_std_1m', 'dewpoint_var_1m',
                     'Name', 'hum corr', 'temp corr',
                     'Presence_Prediction_mean_1m', 'UniqueReference', 'AddressLine1', 'AddressLine2',
                     'pmv_mean_1m', 'pmv_max_1m', 'pmv_min_1m']]
    
    prop_group = report.groupby('UniqueReference')
    
    outlyer_rooms = pd.DataFrame()
    
    for prop in (prop_group.groups.keys()):
        #print(prop)
        prop_df = prop_group.get_group(prop)
        prop_df = prop_df.reset_index()
        #print(prop, 'has', len(prop_df), 'rooms')
        count = 1
        if len(prop_df) > 1:
            for roomX in range(len(prop_df)-1):
                prop_numbers = prop_df.select_dtypes(include='number')
                for roomY in range(len(prop_df)-count):
                    if roomX < roomY+1:
                        room_diffs = prop_numbers.iloc[roomX] - \
                            prop_numbers.iloc[(roomY+1)]
                        room_diffs.name = prop+' ' + \
                            (prop_df['Name'][roomX] +
                             ' - '+prop_df['Name'][roomY+1])
                        outlyer_rooms = pd.concat(
                            [outlyer_rooms, room_diffs], axis=1, join='outer')
    
    room_differences = outlyer_rooms.T
    
    
    
    rank_length = 20
    
    report.set_index(['UniqueReference', 'Name'], inplace=True)
    
    with pd.ExcelWriter(landlord + '_insights.xlsx') as writer:
        
 
        (report[['AddressLine1', 'AddressLine2', 'temperature_max_1m']].loc[report['temperature_max_1m'] > high_temp_min].sort_values(by ='temperature_max_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='high max temp')
        (report[['AddressLine1', 'AddressLine2', 'temperature_mean_1m']].loc[report['temperature_mean_1m'] > high_temp_mean].sort_values(by ='temperature_mean_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='high mean temp')
        (report[['AddressLine1', 'AddressLine2', 'temperature_mean_1m']].loc[report['temperature_mean_1m'] < low_temp_mean].sort_values(by ='temperature_mean_1m', ascending=True).head(rank_length)).to_excel(writer, sheet_name='low mean temp')
        (report[['AddressLine1', 'AddressLine2', 'temperature_min_1m']].loc[report['temperature_min_1m'] < low_temp_max].sort_values(by ='temperature_min_1m', ascending=True).head(rank_length)).to_excel(writer, sheet_name='low min temp')
    
        (report[['AddressLine1', 'AddressLine2', 'humidity_max_1m']].loc[report['humidity_max_1m'] > high_hum_min].sort_values(by ='humidity_max_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='High max hum')
        (report[['AddressLine1', 'AddressLine2', 'humidity_mean_1m']].loc[report['humidity_mean_1m'] > high_hum_mean].sort_values(by ='humidity_mean_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='High mean hum')

        (report[['AddressLine1', 'AddressLine2', 'co2_max_1m']].loc[report['co2_max_1m'] > high_co2_mean].sort_values(by ='co2_max_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='High CO2 max')
        (report[['AddressLine1', 'AddressLine2', 'co2_mean_1m']].loc[report['co2_mean_1m'] > high_co2_max].sort_values(by ='co2_mean_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='High CO2 mean')
    
        (report[['AddressLine1', 'AddressLine2', 'hum corr']].loc[report['hum corr'] > hum_corr].sort_values(by ='hum corr', ascending=False).head(rank_length)).to_excel(writer, sheet_name='Hum correlation')
        (report[['AddressLine1', 'AddressLine2', 'temp corr']].loc[report['temp corr'] > temp_corr].sort_values(by ='temp corr', ascending=False).head(rank_length)).to_excel(writer, sheet_name='Temp correlation')

        (room_differences[['temperature_mean_1m']].loc[room_differences['temperature_mean_1m'] > mean_temp_diff_min].sort_values(by ='temperature_mean_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='diff in mean temp')
        (room_differences[['humidity_mean_1m']].loc[room_differences['humidity_mean_1m'] > mean_hum_diff_min].sort_values(by ='humidity_mean_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='diff in mean hum')
        (room_differences[['co2_mean_1m']].loc[room_differences['co2_mean_1m'] > mean_hum_diff_min].sort_values(by ='co2_mean_1m', ascending=False).head(rank_length)).to_excel(writer, sheet_name='diff in mean CO2')
    
        (report['Presence_Prediction_mean_1m'].sort_values(ascending=True, key=abs).head(
            rank_length)).to_excel(writer, sheet_name='Presence Prediction mean 1m')
    
        (report['pmv_mean_1m'].sort_values(ascending=False).head(
            rank_length)).to_excel(writer, sheet_name='High thermal comfort mean 1m')
        (report['pmv_mean_1m'].sort_values(ascending=True).head(
            rank_length)).to_excel(writer, sheet_name='Low Thermal comfort mean 1m')
        (report['pmv_max_1m'].sort_values(ascending=False).head(
            rank_length)).to_excel(writer, sheet_name='Thermal comfort max 1m')
        (report['pmv_min_1m'].sort_values(ascending=True).head(
            rank_length)).to_excel(writer, sheet_name='Thermal comfort min 1m') 
    
    print('Insights written to file:', landlord+'_insights.xlsx')
    #   except:
    #      print('No access to '+landlord+'_report.csv file')}

monthly_report('Albyn')
insights('ALbyn')
