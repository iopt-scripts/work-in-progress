"""
Functions to lookup id quickly

eg get all propertiesfrom landlord ID



main function is lookup
takes first argument of the id (id_code)
second argument is the id type
defaults to device but can be


'org'
'land'

'device'
'sensor'
'Room'

'prop'





This also creates a local excel file that can be usedd
Other runs will check for a local file first, check and print date.
If out of date will attempt to create one from db.

function local_file can be run to force update of local file by passing argument True (or fromdb = True).

"""


# Imports

import pandas as pd
import numpy as np
import time
import datetime as dt
import os
import os.path

from sqlalchemy import create_engine

import pathlib



# Set path/filename db engine

path = './'

filename = 'Id_lookup.xlsx'

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')

def local_file(fromdb=False):
    global landlords_df
    global properties_df
    global rooms_df
    global sensors_df
    if os.path.isfile(path+filename) and not fromdb:
        print('Found local file.')
        unix_file_time = pathlib.Path(path+filename).stat().st_mtime
        date = dt.datetime.fromtimestamp(unix_file_time)
        today = int(time.time())
        print("File creation time:", date)
        if (today - unix_file_time) > 2628000:    
            print('\033[31m' + 'File over a month old!' + '\033[m')
        elif (today - unix_file_time) > 604800:
            print('\033[33m' + 'File over a week old!' + '\033[m')
        
        landlords_df = pd.read_excel((path+filename), sheet_name = 'Landlords')
        properties_df =  pd.read_excel((path+filename), sheet_name = 'Properties')
        rooms_df =  pd.read_excel((path+filename), sheet_name = 'Rooms')
        sensors_df =  pd.read_excel((path+filename), sheet_name = 'Sensors')
        addresses_df = pd.read_excel((path+filename), sheet_name = 'Addresses')
    else:
        print('Creating new local file')
        # Create db engine

        engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')

        # Pull from db
        # Could do with error handling if db pull fails. Possibly reattempt after pause.
        try:
            landlords_df = pd.read_sql_table('Landlords', engine, columns=['Id', 'OrganisationName'])
            properties_df = pd.read_sql_table('Properties', engine,  columns=['Id', 'LandlordId', 'UniqueReference', 'AddressId'])
            rooms_df = pd.read_sql_table('Rooms', engine, columns=['Id', 'PropertyId', 'SensorId', 'Name', 'RoomType'])
            sensors_df = pd.read_sql_table('Sensors', engine, columns=['Id', 'DeviceEUI'])
            addresses_df = pd.read_sql_table('Addresses', engine, columns=['Id', 'AddressLine1',  'AddressLine2', 'Longitude', 'Latitude', 'Code', 'City'])
        except:
            print('\033[31m' + 'DataBase look up failed!' + '\033[m')
            return
        # Save local file

        with pd.ExcelWriter(path + filename) as writer:
            landlords_df.to_excel(writer, sheet_name='Landlords')
            properties_df.to_excel(writer, sheet_name='Properties')
            rooms_df.to_excel(writer, sheet_name='Rooms')
            sensors_df.to_excel(writer, sheet_name='Sensors')
            addresses_df.to_excel(writer, sheet_name='Addresses')
        print('New local file created')


def lookup(id_code, id_type = 'device'):
    
    local_file(False)
    
    if id_type =='org' or id_type == 'land':
        
        if id_type =='org':
            Landlord_Id = landlords_df.loc[landlords_df['OrganisationName'] == id_code, 'Id'].values[0]
            print('For Orgaisation name:', id_code)
            print('Landlord ID:\t', Landlord_Id)
            
        else:
            Landlord_Id = id_code
            Org_Name = landlords_df.loc[landlords_df['Id'] ==Landlord_Id, 'OrganisationName'].values[0]
            print('For Landlord Id:', id_code)
            print('Orgaisation name:\t', Org_Name)
            
        
        Properties = properties_df.loc[properties_df['LandlordId'] == Landlord_Id, 'Id'].values
        Prop_ref = properties_df.loc[properties_df['LandlordId'] == Landlord_Id, 'UniqueReference'].values
        
        print('Property Id:\t', Properties)
        print('Property Ref:\t', Prop_ref)
        
        
    if id_type == 'device' or id_type == 'sensor' or id_type == 'room':
        
        if id_type == 'device':    
            Sensor_Id = sensors_df.loc[sensors_df['DeviceEUI'] == id_code, 'Id'].values[0]
            Room_Id = rooms_df.loc[rooms_df['SensorId'] == Sensor_Id, 'Id'].values[0]
            print('\nFor device with ID:', id_code)
            print('Sensor ID:\t', Sensor_Id)
            print('Room ID:\t', Room_Id)
            
        elif id_type == 'sensor':
            Sensor_Id = id_code
            Device_Id = sensors_df.loc[sensors_df['Id'] == Sensor_Id, 'DeviceEUI'].values[0]
            Room_Id = rooms_df.loc[rooms_df['SensorId'] == Sensor_Id, 'Id'].values[0]
            print('\nFor sensor with ID:', id_code)
            print('Device ID:\t', Device_Id)
            print('Room ID:\t', Room_Id)
            
        else:
            Room_Id = id_code            
            Sensor_Id =  rooms_df.loc[rooms_df['Id'] == Room_Id, 'SensorId'].values[0]
            Device_Id = sensors_df.loc[sensors_df['Id'] == Sensor_Id, 'DeviceEUI'].values[0]
            print('\nFor room with ID:', id_code)
            print('Device ID:\t', Device_Id)
            print('Sensor ID:\t', Sensor_Id)

        Room_Name = rooms_df.loc[rooms_df['SensorId'] == Sensor_Id, 'Name'].values[0]
        Room_Type = rooms_df.loc[rooms_df['SensorId'] == Sensor_Id, 'RoomType'].values[0]
        Property_Id = rooms_df.loc[rooms_df['SensorId'] == Sensor_Id, 'PropertyId'].values[0]


        Prop_ref = properties_df.loc[properties_df['Id'] == Property_Id, 'UniqueReference'].values[0]
        Landlord_Id = properties_df.loc[properties_df['Id'] == Property_Id, 'LandlordId'].values[0]
        Org_Name = landlords_df.loc[landlords_df['Id'] ==Landlord_Id, 'OrganisationName'].values[0]
        
        
        print('Room Name:\t',Room_Name)
        print('Room Type:\t', Room_Type)
        print('Property ref:\t', Prop_ref)
        print('Property ID:\t', Property_Id)
        print('Org Name:\t', Org_Name)
        print('Landlord ID:\t', Landlord_Id)

    if id_type == 'prop':
        Prop_ref = properties_df.loc[properties_df['Id'] == id_code, 'UniqueReference'].values[0]
        Landlord_Id = properties_df.loc[properties_df['Id'] == id_code, 'LandlordId'].values[0]
        Address_Id =  properties_df.loc[properties_df['Id'] == id_code, 'AddressId'].values[0]
        
        Org_Name =  landlords_df.loc[landlords_df['Id'] ==Landlord_Id, 'OrganisationName'].values[0]
        
        #long = addresses_df.loc[addresses_df['Id'] == Address_Id, 'Longitude'].values
        
        Room_Id = rooms_df.loc[rooms_df['PropertyId'] == id_code, 'Id'].values
        Room_Name = rooms_df.loc[rooms_df['PropertyId'] == id_code, 'Name'].values
        Room_Type = rooms_df.loc[rooms_df['PropertyId'] == id_code, 'RoomType'].values
        
        #Address = addresses_df.loc[addresses_df['Id'] == Address_Id, ['AddressLine1', 'AddressLine2']].values[0]
        
        # Need to do list comprehension for looking up lists.
        #Sensor_Id = rooms_df.loc[rooms_df['Id'] == Room_Id, 'SensorId'].values
        #Device = sensors_df.loc[sensors_df['Id'] == Sensor_Id, 'DeviceEUI'].values
        
        
        # Output better to be tabulated, even return dataframe for use.
        print('Org Name:\t', Org_Name)
        print('Landlord ID:\t', Landlord_Id)
        print('Property ref:\t', Prop_ref)
        #print('Address', Address)
        print('Room ID:\t', Room_Id)
        print('Room Name:\t', Room_Name)
        print('Room Type:\t', Room_Type)
        


        

