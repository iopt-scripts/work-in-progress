import pandas as pd
import datetime as dt
from dateutil.relativedelta import relativedelta
import numpy as np

import psycopg2
import psycopg2.extras
from sqlalchemy import create_engine

from pythermalcomfort.models import pmv_ppd
from pythermalcomfort.utilities import v_relative
from pythermalcomfort.models import clo_tout

today = dt.datetime.today()
todaystr = today.strftime('%Y-%m-%d')
yesterday = (today + relativedelta(days=-1)).strftime('%Y-%m-%d')
daybeforeyesterday = (today + relativedelta(days=-2)).strftime('%Y-%m-%d')
one_week_ago_one_day = (today + relativedelta(weeks=-1, days=-1)).strftime('%Y-%m-%d')
one_week_ago = (today + relativedelta(weeks=-1)).strftime('%Y-%m-%d')
one_month_ago_one_day = (today + relativedelta(months=-1, days = -1)).strftime('%Y-%m-%d')
one_month_ago = (today + relativedelta(months=-1)).strftime('%Y-%m-%d')

print('Reading data')

engine = create_engine(
    'postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine, columns=[
    'Id', 'PropertyId', 'SensorId', 'Name', 'RoomType', 'RecordState'])
sensors_df = pd.read_sql_table(
    'Sensors', engine, columns=['Id', 'DeviceEUI'])
properties_df = pd.read_sql_table('Properties', engine, columns=[
                                  'Id', 'LandlordId', 'AddressId', 'UniqueReference', 'RecordState', 'PropertyType' ])


# Landlord could be passed into function
NL_data = pd.read_csv('North Lanarkshire Council_device_readings.csv', parse_dates=['time'])


NL_data['time'] = pd.to_datetime(NL_data['time'].round('min'))

#device_readings = device_readings.drop_duplicates(subset=['time', 'device'], keep='last')

#device_readings = device_readings.set_index(['time'])

#print(no_of_readings - len(device_readings),
#      'duplicates found and removed.')

NL_data = NL_data.reset_index()
NL_data = NL_data.drop_duplicates(subset=['time', 'device'], keep='last')
NL_data = NL_data.set_index(['time'])

def resample_add_grads(df):
    if len(df) < 5:
        print('Sensor', df['device'][0], 'in property', df['PropertyId'][0], 'has only', len(df), 'readings.')
    else:
        df = df.resample('1min').interpolate(method='time')
        df = df.resample('15min').interpolate(method='time')
        df['temp_grad'] = np.gradient(df['temperature'])
        df['co2_grad'] = np.gradient(df['co2'])
        df['hum_grad'] = np.gradient(df['humidity'])
        # Thermal comfort
        v_r = v_relative(v=0.1, met=1.2)
        results = pmv_ppd(tdb=df['temperature'], tr=df['temperature'], vr=v_r, rh=df['humidity'], met=1.2, clo=clo_tout(tout=df['temperature']), wme=0, standard="ISO")
        results = pd.DataFrame.from_dict(results)
        results.index = df.index 
        df = df.join(results)
        df = df.loc[(df.index > one_week_ago)]
        df = df.loc[(df.index < todaystr)]
        df = df.reset_index()
        if len(df) < 671:
            add_rows = 671 - len(df)
            for row in range(add_rows):
                df = pd.concat([df, df[-1:]])
                df.loc[-1:,'time'] = df.loc[-2:,'time'] + pd.Timedelta(minutes=15)
        df = df.set_index(['time'])
        return df

print('Grouping and resampling data...')

NL_data = NL_data.drop_duplicates()

NL_data = NL_data.groupby(
     'device').apply(resample_add_grads)

NL_data.drop(['device'], axis=1, inplace=True)
NL_data.reset_index(inplace=True)
NL_data.set_index('time', inplace=True)

NL_data['Presence_Prediction'] = np.where(((NL_data.co2 > 500)&(NL_data.co2_grad > 10) | (NL_data.co2_grad > 50)) ,1, 0)

NL_data.reset_index(inplace=True)

# Adding propertyid
sensors_df.rename(columns={'DeviceEUI': 'device'}, inplace=True)
NL_data = NL_data.merge(sensors_df[['Id', 'device']])


NL_data.rename(columns={'Id': 'SensorId'}, inplace=True)
NL_data = NL_data.merge(rooms_df[['SensorId', 'PropertyId']])


properties = NL_data['PropertyId'].unique().tolist()

NL_data_grp = NL_data.groupby('PropertyId')

report = pd.DataFrame()

for prop in properties:    
    property_grouped = NL_data_grp.get_group(prop)
    
    property_devices = property_grouped['device'].unique().tolist()
    
    property_pred = pd.DataFrame()

    property_pred['time'] =property_grouped[['time']].loc[(property_grouped['device'] == property_devices[0])]
    property_pred['preds'] =property_grouped[['Presence_Prediction']].loc[(property_grouped['device'] == property_devices[0])]
    property_pred['co2'] =property_grouped[['co2']].loc[(property_grouped['device'] == property_devices[0])] 
    
    if len(property_devices) > 1:
        for device in range(len(property_devices)-1):
            property_pred['new_co2'] = property_grouped['co2'].loc[(property_grouped['device'] == property_devices[device])].values
            property_pred['new_preds'] = property_grouped['Presence_Prediction'].loc[(property_grouped['device'] == property_devices[device])].values
            property_pred['preds'] = property_pred[['new_preds', 'preds']].max(axis=1)
            property_pred['co2'] = property_pred[['new_co2', 'co2']].max(axis=1)

    property_pred['co2_grad'] = np.gradient(property_pred['co2'])
    property_pred['Presence_Prediction'] = np.where(((property_pred.co2 > 500)&(property_pred.co2_grad > 10) | (property_pred.co2_grad > 50)) ,1, 0)
    property_pred.set_index('time', inplace=True)
    
    result = []
    for value in property_pred.index:
        if value.hour >= 12:
            result.append(value.date())
        else:
            result.append(value.date() - pd.DateOffset(days=1))
           
    property_pred["Night of"] = result
    
    nights = property_pred.groupby('Night of').sum()
    
    property_grouped = NL_data_grp.get_group(prop)

    property_devices = property_grouped['device'].unique().tolist()

    property_pred = pd.DataFrame()

    property_pred['time'] =property_grouped[['time']].loc[(property_grouped['device'] == property_devices[0])]
    property_pred['preds'] =property_grouped[['Presence_Prediction']].loc[(property_grouped['device'] == property_devices[0])]
    property_pred['co2'] =property_grouped[['co2']].loc[(property_grouped['device'] == property_devices[0])]

    if len(property_devices) > 1:
        for device in range(len(property_devices)-1):
            property_pred['new_co2'] = property_grouped['co2'].loc[(property_grouped['device'] == property_devices[device])].values
            property_pred['new_preds'] = property_grouped['Presence_Prediction'].loc[(property_grouped['device'] == property_devices[device])].values
            property_pred['preds'] = property_pred[['new_preds', 'preds']].max(axis=1)
            property_pred['co2'] = property_pred[['new_co2', 'co2']].max(axis=1)


    property_pred['co2_grad'] = np.gradient(property_pred['co2'])
    property_pred['Presence_Prediction'] = np.where(((property_pred.co2 > 500)&(property_pred.co2_grad > 10) | (property_pred.co2_grad > 50)) ,1, 0)
    property_pred.set_index('time', inplace=True)

    result = []
    for value in property_pred.index:
        if value.hour >= 12:
            result.append(value.date())
        else:
            result.append(value.date() - pd.DateOffset(days=1))

    property_pred["Night of"] = result

    nights = property_pred.groupby('Night of').sum()

    nights['Stay_overnight_fromaddedCO2'] = np.where((nights.Presence_Prediction > 13) ,1, 0)
    nights['Stay_overnight_fromaddedpreds'] = np.where((nights.preds > 13) ,1, 0)
    nights.to_csv('data/'+prop+'nights.csv')
    night= nights[['Stay_overnight_fromaddedCO2', 'Stay_overnight_fromaddedpreds']]    
    night= night.T
    night['propid'] = [prop, prop]
    uniqueref = properties_df.loc[properties_df['Id'] == prop, 'UniqueReference'].values[0]
    night['UniqueReference'] = [uniqueref, uniqueref]
    report = pd.concat([report, night], axis=0)

    

report = report.set_index(['UniqueReference'])
report.to_csv('temp_report.csv')

print('Report written to file temp_report.csv')