import pandas as pd
import re

df = pd.read_csv("Data/maryhill_data.csv", names=['NoID', 'Time', 'Address',
                 'Name', 'Temperature', 'Co2', 'Humidity', 'Battery', 'Dewpoint'], skiprows=1)
df = df.loc[:, ['Time', 'Address', 'Temperature']]

devices = df['Address'].unique()
properties = []
df.replace(to_replace='[nN]ew',
           value='^(\d{1,3}/\d{1,3}\s+?\d{0,5})', regex=True)
for device in devices:
    prop_name = re.match(r"^(\d{1,3}/\d{1,3}\s+?\d{0,5})", device)
    if prop_name and prop_name.group(0) not in properties:
        properties.append(prop_name.group(0))
    #df.loc[df['id']==device]['id'] = prop_name
    # if prop_name not in properties:

#prop_data = {}
#
# for device in devices:
#    X = df.loc[(df['id'] == device), ['time', 'temperature']]
#    prop_data[device] = X
