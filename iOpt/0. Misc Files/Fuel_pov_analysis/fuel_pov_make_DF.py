import pandas as pd
import os

list_of_csvs = os.listdir("Data/Fuel_pov_data")

list_of_fp = []
for data_frame in list_of_csvs:
    X = pd.read_csv(data_frame)
    if data_frame[:8] == "maryhill":
        X['label'] = 0
    else:
        X['label'] = 1
    list_of_fp.append(X)



df = pd.concat(list_of_fp)
#df = pd.DataFrame(list_of_corrs, columns=[i+1 for i in range(41)])
df = df.dropna()
df = df.set_index('Unnamed: 0')
df.index.names = ["Device"]
#print(df.head())
df.to_csv("Data/Fuel_pov_data/fuel_pov_df.csv")



