import pandas as pd
import numpy as np
import pickle
import os
import datetime as dt


if __name__ == "__main__":
    # Directory of csvs for analysis
    directory = "iOpt/Data/Fuel_pov_data/fuel_pov_pos_raw"
    csvs = os.listdir(directory)
    start_date = dt.datetime(year=2021, month=8, day=1)
    start_date = start_date.date()
    # Cap dates to autumnal/winter months (classifier trained on this date range so most effective here)
    end_date = dt.datetime(year=2022, month=1, day=1)
    end_date = end_date.date()
    temp_data = {}
    dfs = []
    address_by_device = {}

    for csv in csvs:    # Go through each csv
        X = pd.read_csv(directory + "/" + csv)
        add = X['Address'].unique()[0]
        devices = X['device'].unique()
        X['time'] = pd.to_datetime(X['time'])
        X['time'] = X['time'].dt.tz_localize(None)
        X = X.loc[(X['time'].dt.date > start_date)
                  & (X['time'].dt.date < end_date)]
        for device in devices:
            address_by_device[device] = add
            # Go through each device's data
            X_sub = X.loc[X['device'] == device]
            Y = X_sub.sort_values('time')
            Y.index = Y.time
            Z = Y.loc[:, ['temperature']]
            # Get daily tempearture averages
            Z = Z.resample('D').mean()
            Z = Z.dropna()
            if len(Z.index) > 40:
                Z['temperature'] = Z['temperature'].rolling(    # Find the rolling average to smooth data and find the trend (removes seasonality and more likely noise)
                    window=10, center=True).mean()
                temp_data[device] = Z.dropna()

    for room_data in temp_data.keys():
        try:
            temp_min = temp_data[room_data].loc[:, 'temperature'].min()
            temp_av = temp_data[room_data].loc[:, 'temperature'].mean()
            temp_std = temp_max = temp_data[room_data].loc[:, 'temperature'].std(
            )
            total_data = [temp_min, temp_av, temp_std]
            X = {room_data: total_data}
            column_names = ["min", "av", "std"]
            X = pd.DataFrame(X, column_names)
            X = X.transpose()
            dfs.append(X)
        except Exception as e:
            print(room_data + " gives following error: " + str(e))
            continue
    # Unpickle classifier, input features, record output

    svm = pickle.load(open("iOpt/Fuel_pov_analysis/svm_classifier.sav", 'rb'))
    dfs = pd.concat(dfs)
    dfs['fp_preds'] = svm.predict(dfs)
    potential_fp = dfs.loc[dfs['fp_preds'] == 1]

    fuel_pov = list(potential_fp.index)
    fuel_pov_adds = []
    for i in fuel_pov:
        # Make a list of addresses corresponding to devices with outlying temp decays
        fuel_pov_adds.append(address_by_device[i])

    # Remove duplicate addresses (forming the set removes repeating addresses)
    Y = list(set(fuel_pov_adds))
    Y = {'Addresses': Y}
    if fuel_pov:
        Y = pd.DataFrame(Y)
        # Choose where to store csv of problem addresses
        Y.to_csv("Exit_Path.csv")
