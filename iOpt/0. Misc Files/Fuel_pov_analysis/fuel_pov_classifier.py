import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import pickle

data_pos = pd.read_csv(
    "iOpt/Data/Fuel_pov_data/pos_fuel_pov_features.csv")   # Load Data
data_pos_eui = pd.read_csv(
    "iOpt/Data/Fuel_pov_data/pos_fuel_pov_features_eui.csv")
data_neg = pd.read_csv("iOpt/Data/Fuel_pov_data/neg_fuel_pov_features.csv")
#data_neg = data_neg.iloc[:10, :]
# Make Data Frame of Features and Labels

X = pd.concat([data_neg, data_pos, data_pos_eui])
X = X.dropna()
pos_features = X.loc[X['label'] == 1].loc[:, ['min', 'av', 'std']]
pos_labels = X.loc[X['label'] == 1].loc[:, ['label']]
neg_features = X.loc[X['label'] == 0].loc[:, ['min', 'av', 'std']]
neg_labels = X.loc[X['label'] == 0].loc[:, ['label']]
features = X.loc[:, ['min', 'av', 'std']]
labels = X['label']
X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.25,
                                                    random_state=7)  # Split Data into training and test sets
classifier = SVC()
classifier.fit(X_train, y_train)        # Train Classifier
y_pred = classifier.predict(X_test)  # Predict labels for test set
# Compare test set predictions to actual values
print("TEST: " + str(accuracy_score(y_test, y_pred)))
neg_pred = classifier.predict(neg_features)
# Evaluate performance on negative labels
print("NEG: " + str(accuracy_score(neg_pred, neg_labels)))
pos_pred = classifier.predict(pos_features)
# Evaluate performance on positive labels
print("POS: " + str(accuracy_score(pos_labels, pos_pred)))

model_path = "iOpt/Fuel_pov_analysis/svm_classifier.sav"
pickle.dump(classifier, open(model_path, 'wb'))
