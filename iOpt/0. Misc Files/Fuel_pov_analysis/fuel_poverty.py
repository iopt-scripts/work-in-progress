from statsmodels.tsa.stattools import acf
import pandas as pd
import datetime as dt
from matplotlib import pyplot as plt
import os
from statsmodels.tsa.seasonal import seasonal_decompose

if __name__ == "__main__":

    #X = pd.read_csv("../Data/maryhill_data.csv")
    #X["ID"] = X.Address.str[0:15] + X["Name"]
    #X["ID"] = X["ID"].astype(str)
    #X['ID'] = X['ID'].str.replace(" ", "_", regex=False)
    #X['ID'] = X["ID"].str.replace("/", "_", regex=False)
    # Use if to get a list of csvs, iterate through, getting a list of devices per csv and extracting features for each device
    directory = "iOpt/Data/Fuel_pov_data/fuel_pov_pos_eui"
    list_of_csvs = os.listdir(directory)
    devices = []
    start_date = dt.datetime(year=2021, month=8, day=1)
    start_date = start_date.date()
    end_date = dt.datetime(year=2022, month=1, day=1)
    end_date = end_date.date()
    temp_abs_data = {}
    temp_change_data = {}
    temp_data = {}
    dfs = []
    for i in list_of_csvs:
        X = pd.read_csv(directory + "/" + i)
        #X["ID"] = X.Address.str[0:15] + X["Name"]
        #X["ID"] = X["ID"].astype(str)
        #X['ID'] = X['ID'].str.replace(" ", "_", regex=False)
        #X['ID'] = X["ID"].str.replace("/", "_", regex=False)
        #device_names = X['ID'].unique()
        device_names = X['DeviceEUI'].unique()
        X['time'] = pd.to_datetime(X['time'])
        X['time'] = X['time'].dt.tz_localize(None)
        X = X.loc[(X['time'].dt.date > start_date)
                  & (X['time'].dt.date < end_date)]
        acf_data = []
        for device in device_names:
            Y = X.loc[(X['DeviceEUI'] == device)]
            Y = Y.sort_values('time')
            Y.index = Y.time
            Z = Y.loc[:, ['temperature']]
            Z = Z.resample('D').mean()
            Z = Z.dropna()
            if len(Z.index) > 40:
                Z['temperature'] = Z['temperature'].rolling(
                    window=10, center=True).mean()
                temp_data[device] = Z.dropna()

    for room_data in temp_data.keys():
        try:
            temp_min = temp_data[room_data].loc[:, 'temperature'].min()
            temp_av = temp_data[room_data].loc[:, 'temperature'].mean()
            temp_std = temp_max = temp_data[room_data].loc[:, 'temperature'].std(
            )
            total_data = [temp_min, temp_av, temp_std]
            X = {room_data: total_data}
            column_names = ["min", "av", "std"]
            X = pd.DataFrame(X, column_names)
            X = X.transpose()
            dfs.append(X)
            #csv_str = "Data/Fuel_pov_data/maryhill" + str(room_data) + '.csv'
            # X.to_csv(csv_str)
            # acf_data.append(acf(temp_abs_data[room_data]))
        except Exception as e:
            print(room_data + " gives following error: " + str(e))
            continue
    dfs = pd.concat(dfs)
    dfs['label'] = 1
    dfs.to_csv("pos_fuel_pov_features_eui")
   # for i in acf_data:
   #     plt.plot(i)
   #     plt.xlabel("Lags")
   #     plt.ylabel("ACF Value")
   #     plt.show()
#
   #     # Get local weather for property
