# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

# Not all properties have surveys so can't use this
'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# 1. Remove comments on LandlordId you require.

# Loreburn
LandlordId = '8fb0fac8-ea47-480d-95ba-c6e1c8be16cd'


Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, three_months_ago=three_months_ago)
#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_ren variable with output
device_all_loreburn = pd.read_csv('device_all.csv', parse_dates=['time'])

device_all_loreburn['device'].nunique()

loreburn = properties_df.loc[properties_df['LandlordId'] == '8fb0fac8-ea47-480d-95ba-c6e1c8be16cd']
loreburn_rooms = loreburn.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
loreburn_rooms_sensors = loreburn_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
loreburn_rooms_sensors_address = loreburn_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
loreburn_rooms_sensors_address['Address'] = loreburn_rooms_sensors_address['AddressLine1'].map(str) + ' ' + loreburn_rooms_sensors_address['AddressLine2'].map(str) + ' ' + loreburn_rooms_sensors_address['City'].map(str) + ' ' + loreburn_rooms_sensors_address['Code'].map(str)
loreburn_properties = loreburn_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

loreburn_properties_data = loreburn_properties.merge(device_all_loreburn, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
#loreburn_properties_data.to_csv('loreburn_env_sensors.csv')
sw_11 = loreburn_properties_data.loc[loreburn_properties_data['Address'].str.contains('11 Smith Way')]
sw_11 = sw_11.loc[sw_11['temperature'] < 100]
sw_11.to_csv('11_smith_way.csv')

loreburn_data_last_month = loreburn_properties_data.loc[loreburn_properties_data['time'] >= one_month_ago]
loreburn_data_last_month = loreburn_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
loreburn_data_last_month = loreburn_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
loreburn_data_last_three_months = loreburn_properties_data.loc[loreburn_properties_data['time'] >= three_months_ago]
loreburn_data_last_three_months = loreburn_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
loreburn_data_last_three_months = loreburn_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

loreburn_averages_1m = loreburn_data_last_month.groupby(['Address', 'Name', 'device']).mean()
loreburn_averages_3m = loreburn_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
loreburn_averages_1m = loreburn_averages_1m.reset_index()
loreburn_averages_3m = loreburn_averages_3m.reset_index()

loreburn_averages = loreburn_averages_1m.merge(loreburn_averages_3m, on = 'device', how = 'inner')
loreburn_averages = loreburn_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (loreburn_averages['co2_1m_avg'].ge(550) & loreburn_averages['temperature_1m_avg'].lt(14)) ,
    (loreburn_averages['humidity_1m_avg'].ge(70) & loreburn_averages['temperature_1m_avg'] - loreburn_averages['dewpoint_1m_avg'].lt(4) & loreburn_averages['co2_1m_avg'].ge(1500)),
    (loreburn_averages['humidity_1m_avg'].ge(65) & loreburn_averages['temperature_1m_avg'] - loreburn_averages['dewpoint_1m_avg'].lt(5)),
    (loreburn_averages['co2_1m_avg'].ge(1300)),
    (loreburn_averages['co2_1m_avg'].lt(500) & loreburn_averages['temperature_1m_avg'].lt(13)),
    (loreburn_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

loreburn_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (loreburn_averages['co2_1m_avg'].ge(2000) | (loreburn_averages['temperature_1m_avg'].lt(14) & loreburn_averages['co2_1m_avg'].ge(550)) | loreburn_averages['humidity_1m_avg'].ge(70)),
    (loreburn_averages['co2_1m_avg'].ge(1300) | (loreburn_averages['temperature_1m_avg'].lt(15) & loreburn_averages['co2_1m_avg'].ge(550)) | loreburn_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

loreburn_averages['severity'] = np.select(conditions, choices)

loreburn_averages['address_room'] = loreburn_averages['Address'] + ' ' + loreburn_averages['Name']
loreburn_averages.to_csv('loreburn_averages.csv')
loreburn_averages_1m['address_room'] = loreburn_averages_1m['Address'] + ' ' + loreburn_averages_1m['Name']
loreburn_averages_1m.to_csv('loreburn_1m_averages.csv')

loreburn_averages_critical = loreburn_averages.loc[loreburn_averages['severity'] == 'critical']

loreburn_averages_critical['issue'] = loreburn_averages['issue'].astype(pd.api.types.CategoricalDtype(categories = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']))
loreburn_averages_critical = loreburn_averages_critical.sort_values('issue')
loreburn_averages_critical_count = loreburn_averages_critical
loreburn_averages_critical_count = loreburn_averages_critical_count.drop_duplicates('Address', keep = 'first')
loreburn_averages_critical_count = loreburn_averages_critical_count['issue'].value_counts()


weather_df = weather_df.loc[(weather_df['code'] == 'Moffat') | (weather_df['code'] == 'Castle Douglas') | (weather_df['code'] == 'Stranraer')]
weather_df['time'] = pd.to_datetime(weather_df['time'])
weather_df['time'] = weather_df['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

# Merging env and weather
conditions = [
    (loreburn_properties_data['Address'].str.contains('Smith Way')),
    (loreburn_properties_data['Address'].str.contains('Castle Douglas')),
    (loreburn_properties_data['Address'].str.contains('Stranraer'))   
]
choices = ['Moffat', 'Castle Douglas', 'Stranraer']

loreburn_properties_data['area'] = np.select(conditions, choices)


loreburn_properties_data['time'] = loreburn_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
loreburn_properties_data['time'] = pd.to_datetime(loreburn_properties_data['time'])

weather_df['time'] = pd.to_datetime(weather_df['time'])
# Resampling Weather for
weather_3h = weather_df.set_index(
    'time').resample('3 hours').ffill()

# Set time delta
tol = pd.Timedelta('3 hours')

loreburn_env_weather = pd.merge_asof(loreburn_properties_data.sort_values('time'), weather_df.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)
loreburn_env_weather = loreburn_env_weather.loc[loreburn_env_weather['time'] > '2022-02-03 04:57:00']

loreburn_env_weather = loreburn_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
loreburn_env_weather = loreburn_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2']]
#loreburn_env_weather.to_csv('loreburn_weather_env.csv')

#forsyth_1 = loreburn_properties_data.loc[loreburn_properties_data['Address'].str.contains('1 Forsyth')]
#forsyth_1.to_csv('1_forsyth.csv')

'''
from scipy.stats import pearsonr
corr = pearsonr(loreburn_env_weather['indoor_temperature'], loreburn_env_weather['outdoor_temperature'])
print(corr)
'''

################## ENERGY ##################

### 3-Phase 

property_ids = Landlord_properties['Id']
property_id = tuple(property_ids)

conn_string = "host='iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com' dbname='iopt' user='iopt_readonly' password='b!erHalle22'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device_id, timestamp, property_id, channel_1_current, channel_2_current, channel_3_current, channel_1_energy, channel_2_energy, channel_3_energy FROM three_phase_consumption WHERE timestamp BETWEEN '{three_months_ago}' AND '{today}' AND property_id IN {property_id}".format(today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago, property_id=property_id)
cursor.execute(query, [tuple(property_id)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_power.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_energy_loreburn = pd.read_csv('device_all_power.csv', parse_dates=['timestamp'])

############

### Single Phase

property_ids = Landlord_properties['Id']
property_id = tuple(property_ids)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

#query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE timestamp BETWEEN '{one_month_ago}' AND '{today}' AND device_id IN ('00137A100000CB23', '00137A100000D217')".format(today=today, one_month_ago=one_month_ago, property_id=property_id)
query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE device_id IN ('00137A100000CB23', '00137A100000D217', '00137A100000D72E', '00137A100000CB1E')".format(today=today, one_month_ago=one_month_ago, property_id=property_id)

cursor.execute(query, [tuple(property_id)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_power.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_power_loreburn = pd.read_csv('device_all_power.csv', parse_dates=['timestamp'])
device_all_power_loreburn['reading'] = abs(device_all_power_loreburn['reading']) * 230 / 1000 / 1000
device_all_power_loreburn = device_all_power_loreburn.merge(properties_df, left_on = 'property_id', right_on = 'Id', how = 'left')
device_all_power_loreburn = device_all_power_loreburn.merge(addresses_df, left_on='AddressId', right_on='Id', how='left')
device_all_power_loreburn = device_all_power_loreburn[['AddressLine1', 'device_id', 'timestamp', 'device_type', 'reading']]

conditions = [
    (device_all_power_loreburn['device_id'] == '00137A100000CB23'),
    (device_all_power_loreburn['device_id'] == '00137A100000D217'),
    (device_all_power_loreburn['device_id'] == '00137A100000D72E'),
    (device_all_power_loreburn['device_id'] == '00137A100000CB1E')    
]
choices = ['14 Balliol Court', '17 Balliol Court', '1 Forsyth Street', '12 Balliol Court']

device_all_power_loreburn['Address'] = np.select(conditions, choices)
device_all_power_loreburn = device_all_power_loreburn.loc[(device_all_power_loreburn['timestamp'] >= '2022-04-01')]
device_all_power_loreburn = device_all_power_loreburn[['Address', 'device_id', 'timestamp', 'device_type', 'reading']]

#device_all_power_loreburn.to_csv('energy_loreburn.csv')

'''
env_sensors_for_power_comparison = loreburn_properties_data.loc[(loreburn_properties_data['device'] == '70B3D55F200010C6') | (loreburn_properties_data['device'] == '70B3D55F200010CE') | (loreburn_properties_data['device'] == '70B3D55F200010EA') | (loreburn_properties_data['device'] == '70B3D55F200010D0') | (loreburn_properties_data['device'] == '70B3D55F200010D4') | (loreburn_properties_data['device'] == '70B3D55F200010CA') | (loreburn_properties_data['device'] == '70B3D55F200010C5')]
env_sensors_for_power_comparison.to_csv('loreburn_power_env_comparison.csv')
'''


device_all_power_loreburn_month = device_all_power_loreburn.groupby(['Address', pd.Grouper(key='timestamp', freq='M')]).sum()
device_all_power_loreburn_week = device_all_power_loreburn.groupby(['Address', pd.Grouper(key='timestamp', freq='W')]).sum()
device_all_power_loreburn_day = device_all_power_loreburn.groupby(['Address', pd.Grouper(key='timestamp', freq='D')]).sum()

device_all_power_loreburn_month = device_all_power_loreburn_month.reset_index()
device_all_power_loreburn_month = device_all_power_loreburn_month.groupby('Address')['reading'].mean()

device_all_power_loreburn_week = device_all_power_loreburn_week.reset_index()
device_all_power_loreburn_week = device_all_power_loreburn_week.groupby('Address')['reading'].mean()

device_all_power_loreburn_day = device_all_power_loreburn_day.reset_index()
device_all_power_loreburn_day = device_all_power_loreburn_day.groupby('Address')['reading'].mean()

######

device_all_energy_loreburn_calculated = device_all_energy_loreburn

##### 11 Smith Way ######
device_all_energy_loreburn_calculated = device_all_energy_loreburn_calculated.merge(properties_df, left_on = 'property_id', right_on = 'Id', how = 'left')
device_all_energy_loreburn_calculated = device_all_energy_loreburn_calculated.merge(addresses_df, left_on='AddressId', right_on='Id', how='left')
#device_all_energy_loreburn_calculated = device_all_energy_loreburn_calculated.loc[device_all_energy_loreburn_calculated['AddressLine1'].str.contains('11 Smith Way')]
device_all_energy_loreburn_calculated = device_all_energy_loreburn_calculated.sort_values('timestamp')
device_all_energy_loreburn_calculated['AddressLine1'].value_counts()

device_all_energy_loreburn_calculated['channel_1_energy_kwh'] = device_all_energy_loreburn_calculated['channel_1_energy'].shift(-1) - device_all_energy_loreburn_calculated['channel_1_energy']
device_all_energy_loreburn_calculated['channel_1_energy_kwh'] = abs(device_all_energy_loreburn_calculated['channel_1_energy_kwh'] / 1000)
device_all_energy_loreburn_calculated['channel_2_energy_kwh'] = device_all_energy_loreburn_calculated['channel_2_energy'].shift(-1) - device_all_energy_loreburn_calculated['channel_2_energy']
device_all_energy_loreburn_calculated['channel_2_energy_kwh'] = abs(device_all_energy_loreburn_calculated['channel_2_energy_kwh'] / 1000)
device_all_energy_loreburn_calculated['channel_3_energy_kwh'] = device_all_energy_loreburn_calculated['channel_3_energy'].shift(-1) - device_all_energy_loreburn_calculated['channel_3_energy']
device_all_energy_loreburn_calculated['channel_3_energy_kwh'] = abs(device_all_energy_loreburn_calculated['channel_3_energy_kwh'] / 1000)
device_all_energy_loreburn_calculated['channel_1_current'] = device_all_energy_loreburn_calculated['channel_1_current']*230/1000
device_all_energy_loreburn_calculated['channel_2_current'] = device_all_energy_loreburn_calculated['channel_2_current']*230/1000
device_all_energy_loreburn_calculated['channel_3_current'] = device_all_energy_loreburn_calculated['channel_3_current']*230/1000

device_all_energy_loreburn_calculated = device_all_energy_loreburn_calculated[['AddressLine1', 'device_id', 'timestamp', 'channel_1_current', 'channel_2_current', 'channel_3_current', 'channel_1_energy_kwh', 'channel_2_energy_kwh', 'channel_3_energy_kwh']]
#device_all_energy_loreburn_calculated.to_csv('sw_energy.csv')

device_all_energy_loreburn_calculated_hour = device_all_energy_loreburn_calculated.groupby(['AddressLine1', pd.Grouper(key='timestamp', freq='H')]).mean()
device_all_energy_loreburn_calculated_hour = device_all_energy_loreburn_calculated_hour.reset_index()
#device_all_energy_loreburn_calculated_hour.to_csv('sw_energy_hour.csv')

device_all_energy_loreburn_calculated_month = device_all_energy_loreburn_calculated_hour.groupby(['AddressLine1', pd.Grouper(key='timestamp', freq='M')]).sum()
device_all_energy_loreburn_calculated_week = device_all_energy_loreburn_calculated_hour.groupby(['AddressLine1', pd.Grouper(key='timestamp', freq='W')]).sum()
device_all_energy_loreburn_calculated_day = device_all_energy_loreburn_calculated_hour.groupby(['AddressLine1', pd.Grouper(key='timestamp', freq='D')]).sum()
#device_all_energy_loreburn_calculated_day.to_csv('11_smith_way_daily_electricity_consumption.csv')
#device_all_energy_loreburn_calculated_week.to_csv('11_smith_way_weekly_electricity_consumption.csv')

device_all_energy_loreburn_calculated_day_mean = device_all_energy_loreburn_calculated_day.reset_index()
device_all_energy_loreburn_calculated_day_mean = device_all_energy_loreburn_calculated_day_mean.groupby('AddressLine1').mean() 

device_all_energy_loreburn_calculated_month_mean = device_all_energy_loreburn_calculated_month.reset_index()
device_all_energy_loreburn_calculated_month_mean = device_all_energy_loreburn_calculated_month_mean.groupby('AddressLine1').mean() 


