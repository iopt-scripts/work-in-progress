# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 08:16:46 2022

@author: tomda
"""

import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

# Not all properties have surveys so can't use this
'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# 1. Remove comments on LandlordId you require.

# macmic
LandlordId = 'e6e5cdd6-6a60-4d3f-b5f1-c7b1d7956c5d'


Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, three_months_ago=three_months_ago)
#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_ren variable with output
device_all_macmic = pd.read_csv('device_all.csv', parse_dates=['time'])

device_all_macmic['device'].nunique()

macmic = properties_df.loc[properties_df['LandlordId'] == 'e6e5cdd6-6a60-4d3f-b5f1-c7b1d7956c5d']
macmic_rooms = macmic.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
macmic_rooms_sensors = macmic_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
macmic_rooms_sensors_address = macmic_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
macmic_rooms_sensors_address['Address'] = macmic_rooms_sensors_address['AddressLine1'].map(str) + ' ' + macmic_rooms_sensors_address['AddressLine2'].map(str) + ' ' + macmic_rooms_sensors_address['City'].map(str) + ' ' + macmic_rooms_sensors_address['Code'].map(str)
macmic_properties = macmic_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

macmic_properties_data = macmic_properties.merge(device_all_macmic, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
#macmic_properties_data.to_csv('macmic_env_sensors.csv')

conditions = [
    (macmic_properties_data['device'] == '70B3D55F20000FBF') | (macmic_properties_data['device'] == '70B3D55F20000CC0')
    (macmic_properties_data['device'].ge(1300) | (macmic_properties_data['temperature_1m_avg'].lt(15) & macmic_averages['co2_1m_avg'].ge(550)) | macmic_averages['humidity_1m_avg'].ge(65))
]
choices = ['10 Musgrove Road  London SE14 5PW', 'medium']

macmic_properties_data['Address'] = np.select(conditions, choices)

macmic_data_last_month = macmic_properties_data.loc[macmic_properties_data['time'] >= one_month_ago]
macmic_data_last_month = macmic_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
macmic_data_last_month = macmic_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
macmic_data_last_three_months = macmic_properties_data.loc[macmic_properties_data['time'] >= three_months_ago]
macmic_data_last_three_months = macmic_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
macmic_data_last_three_months = macmic_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

macmic_averages_1m = macmic_data_last_month.groupby(['Address', 'Name', 'device']).mean()
macmic_averages_3m = macmic_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
macmic_averages_1m = macmic_averages_1m.reset_index()
macmic_averages_3m = macmic_averages_3m.reset_index()

macmic_averages = macmic_averages_1m.merge(macmic_averages_3m, on = 'device', how = 'inner')
macmic_averages = macmic_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (macmic_averages['co2_1m_avg'].ge(550) & macmic_averages['temperature_1m_avg'].lt(14)) ,
    (macmic_averages['humidity_1m_avg'].ge(70) & macmic_averages['temperature_1m_avg'] - macmic_averages['dewpoint_1m_avg'].lt(4) & macmic_averages['co2_1m_avg'].ge(1500)),
    (macmic_averages['humidity_1m_avg'].ge(65) & macmic_averages['temperature_1m_avg'] - macmic_averages['dewpoint_1m_avg'].lt(5)),
    (macmic_averages['co2_1m_avg'].ge(1300)),
    (macmic_averages['co2_1m_avg'].lt(500) & macmic_averages['temperature_1m_avg'].lt(13)),
    (macmic_averages['temperature_1m_avg'].ge(27))
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

macmic_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (macmic_averages['co2_1m_avg'].ge(2000) | (macmic_averages['temperature_1m_avg'].lt(14) & macmic_averages['co2_1m_avg'].ge(550)) | macmic_averages['humidity_1m_avg'].ge(70)),
    (macmic_averages['co2_1m_avg'].ge(1300) | (macmic_averages['temperature_1m_avg'].lt(15) & macmic_averages['co2_1m_avg'].ge(550)) | macmic_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

macmic_averages['severity'] = np.select(conditions, choices)

# Weather

weather_online_df = pd.read_csv('C:/Users/tomda/Downloads/dataexport_20220425T120213.csv')
weather_online_df['timestamp'] = pd.to_datetime(weather_online_df['timestamp'])
weather_online_df['area'] = 'London'

weather_df = weather_df.loc[(weather_df['code'] == 'London')]
weather_online_df['timestamp'] = pd.to_datetime(weather_online_df['timestamp'])
weather_online_df['timestamp'] = weather_online_df['timestamp'].dt.strftime('%Y-%m-%d %H:%M:%S')

macmic_properties_data['time'] = macmic_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
macmic_properties_data['time'] = pd.to_datetime(macmic_properties_data['time'])

weather_online_df['timestamp'] = pd.to_datetime(weather_online_df['timestamp'])
macmic_properties_data['area'] = 'London'

weather_3h = weather_online_df.set_index('timestamp').resample('3H').ffill()

weather_online_df['time'] = weather_online_df['timestamp']

tol = pd.Timedelta('3 hours')

macmic_env_weather = pd.merge_asof(macmic_properties_data.sort_values('time'), weather_online_df.sort_values('timestamp'), on='time', left_by='area', right_by='area', direction='nearest', tolerance=tol)
macmic_env_weather = macmic_env_weather.loc[macmic_env_weather['time'] > '2022-03-24 00:00:00']

macmic_env_weather = macmic_env_weather.rename(columns = {'temperature_y': 'outdoor_temperature', 'temperature_x': 'indoor_temperature'})
macmic_env_weather = macmic_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'outdoor_temperature', 'co2', 'area']]
#macmic_env_weather.to_csv('macmic_weather_env.csv')


macmic_weather_corr_humidity = macmic_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]
macmic_weather_corr_humidity = macmic_weather_corr_humidity.reset_index()
macmic_weather_corr_temperature = macmic_env_weather.groupby('device')[['outdoor_temperature','indoor_temperature']].corr().unstack().iloc[:,1]
macmic_weather_corr_temperature = macmic_weather_corr_temperature.reset_index()


macmic_humidity_corr_prop = macmic_properties.merge(macmic_weather_corr_humidity, left_on = 'DeviceEUI', right_on = 'device', how = 'left')
macmic_humidity_corr_prop.to_csv('macmic_humidity_corr.csv')

macmic_temperature_corr_prop = macmic_properties.merge(macmic_weather_corr_temperature, left_on = 'DeviceEUI', right_on = 'device', how = 'left')
macmic_temperature_corr_prop.to_csv('macmic_temperature_corr.csv')
