# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Pull tables in from database
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

engine = create_engine('postgresql://iopt_readonly:b!erHalle22@iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com/iopt')
rooms_df = pd.read_sql_table('Areas', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)


'''
#Total number of sensors 

edin = properties_df.loc[properties_df['OrganisationId'] == 'b934cedd-ba92-4422-972a-fdf12d00cfa7']
edin = edin.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'left')
edin = edin.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'left')
'''

# Mac Version
#schools_batch = pd.read_csv('/Users/tomdavie/Desktop/edin_school_audit.csv')
# Microsoft Version
schools_batch = pd.read_csv('C:/Users/tomda/Desktop/Misc/edin_school_audit.csv')



'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''


# edin_schools
OrganisationId = 'b934cedd-ba92-4422-972a-fdf12d00cfa7'

Landlord =  properties_df['OrganisationId'] == OrganisationId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

# Mac Version
#devices = pd.read_csv('/Users/tomdavie/Downloads/edin_devices.csv')
# Windows Version
devices = pd.read_csv('C:/Users/tomda/Downloads/edin_devices.csv')
device = tuple(devices['Device'])

today = datetime.datetime.today()
one_week_ago = date.today() + relativedelta(weeks=-1)
three_months_ago = date.today() + relativedelta(months=-3)
two_months_ago = date.today() + relativedelta(months=-2)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_week_ago = one_week_ago.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
two_months_ago = two_months_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "postgresql://iopt_readonly:b!erHalle22@iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com/iopt"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) 

# 2. Change time frame to desired window

#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint, areaid FROM readings WHERE time BETWEEN '{two_months_ago}' AND '{today}' AND device IN {device}".format(device=device, today=today, one_week_ago=one_week_ago, one_month_ago=one_month_ago, two_months_ago=two_months_ago, three_months_ago=three_months_ago)
query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint, areaid FROM readings WHERE time BETWEEN '2022-11-21 02:59:30' AND '2022-11-21 03:00:30' AND device IN {device}".format(device=device, today=today, one_week_ago=one_week_ago, one_month_ago=one_month_ago, two_months_ago=two_months_ago, three_months_ago=three_months_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_edin_schools.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

# Create device_all_df variable with output
device_all_edin_schools = pd.read_csv('device_all_edin_schools.csv', parse_dates=['time'])

'''
test = device_all_edin_schools.loc[device_all_edin_schools['temperature'] == -40].drop_duplicates('device')
test['device'].nunique()
#test = test.merge(edin_schools_rooms_sensors_batch, left_on = 'device', right_on = 'Device', how = 'left')
'''

device_list_from_readings = device_all_edin_schools['device'].drop_duplicates()
device_list_from_readings = device_list_from_readings.to_frame()
device_list_from_readings['Status'] = 'Online'


device_count = device_all_edin_schools[['device', 'time']].sort_values('time').drop_duplicates('device', keep = 'last')

device_count['time'] = pd.to_datetime(device_count['time']) - pd.to_timedelta(7, unit='d')
test = device_count.groupby([pd.Grouper(key='time', freq='D')])['device'].count()

edin_schools_sensors = device_all_edin_schools.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'left')
edin_schools_sensors = edin_schools_sensors[['DeviceEUI', 'time', 'temperature', 'co2', 'humidity', 'AreaId']]
edin_schools_sensors_rooms = edin_schools_sensors.merge(rooms_df[['Id', 'PropertyId', 'Name']], left_on = 'AreaId', right_on = 'Id', how = 'left')
edin_schools_sensors_rooms_properties = edin_schools_sensors_rooms.merge(properties_df[['Id', 'UniqueReference']], left_on = 'PropertyId', right_on = 'Id', how = 'left')
edin_schools_sensors_rooms_properties = edin_schools_sensors_rooms_properties[['UniqueReference', 'Name', 'DeviceEUI', 'time', 'temperature', 'co2', 'humidity']]

##########################################

# Audit around 3am Monday CO2

'''
edin_at_3am = edin_schools_sensors_rooms_properties.loc[(edin_schools_sensors_rooms_properties['co2'] <= 370) | (edin_schools_sensors_rooms_properties['co2'] >= 530)]
edin_schools_sensors_rooms_properties['DeviceEUI'].nunique()
edin_at_3am = edin_at_3am.sort_values('time').drop_duplicates('DeviceEUI', keep = 'last')
edin_at_3am = edin_at_3am[['UniqueReference', 'Name', 'DeviceEUI', 'co2']]
edin_at_3am['DeviceEUI'].nunique()

edin_at_3am = edin_at_3am.merge(schools_batch, left_on = 'DeviceEUI', right_on = 'Device', how = 'inner')
edin_at_3am.to_csv('edin_at_3am.csv')

below_co2 = edin_at_3am.loc[edin_at_3am['co2'] < 250]

old_audit = pd.read_excel("C:/Users/tomda/Downloads/Edinburgh School Sensor Audit 10-11-2022 (1).xlsx")

edin_at_3am_old_fault = edin_at_3am.merge(old_audit, left_on = 'Device', right_on = 'Device', how = 'left')
edin_at_3am_old_fault = edin_at_3am_old_fault[['UniqueReference', 'Name', 'DeviceEUI', 'co2', 'Batch_x', 'Status', 'Error Code']]
#edin_at_3am_old_fault.to_csv('edin_calibration_audit.csv')
'''
##########################################


edin_schools_rooms_sensors_batch = edin_schools_sensors_rooms_properties.merge(schools_batch, left_on = 'DeviceEUI', right_on = 'Device', how = 'inner')

conditions = [
    ((edin_schools_rooms_sensors_batch['temperature'] == -40) & (edin_schools_rooms_sensors_batch['humidity'] == 0) & (edin_schools_rooms_sensors_batch['temperature'].lt(0))),
     (edin_schools_rooms_sensors_batch['co2'].ge(5001)),
     ((edin_schools_rooms_sensors_batch['temperature'].lt(5)) | (edin_schools_rooms_sensors_batch['humidity'].lt(10)) | (edin_schools_rooms_sensors_batch['temperature'].ge(50)))
]

choices = ['1', '2', '3']

edin_schools_rooms_sensors_batch['error_code'] = np.select(conditions, choices, default='none')

edin_schools_rooms_sensors_batch_error = edin_schools_rooms_sensors_batch.sort_values('error_code', ascending=True).drop_duplicates(['Device'], keep = 'first') 
edin_schools_rooms_sensors_batch_error = edin_schools_rooms_sensors_batch_error[['UniqueReference', 'Name', 'DeviceEUI', 'Batch', 'error_code']].rename(columns = {'UniqueReference': 'School', 'Name': 'Room', 'DeviceEUI': 'Device', 'error_code': 'Error Code'})

final = edin_schools_rooms_sensors_batch_error.merge(edin_complete_batch, left_on = 'Device', right_on = 'Device', how = 'inner')

test = edin_schools_rooms_sensors_batch.groupby([pd.Grouper(key='time', freq='W')])['error_code'].value_counts()

# Schools Errors per Batch

schools_error_count = edin_schools_rooms_sensors_batch.groupby(['Batch', 'Device', 'error_code']).count()
schools_error_count = schools_error_count.reset_index()

schools_error_count = schools_error_count.sort_values('error_code', ascending=True).drop_duplicates(['Device'], keep = 'first') 
schools_error_count.groupby('Batch')['error_code'].value_counts()

'''
batch_4 = edin_schools_rooms_sensors_batch.loc[edin_schools_rooms_sensors_batch['Batch'] == 4]
batch_4_error40 = batch_4.loc[batch_4['temperature'] == -40]
batch_4_error40 = batch_4_error40[['UniqueReference', 'Name', 'Device', 'time', 'temperature', 'co2', 'humidity']]

error_1 = edin_schools_rooms_sensors_batch.loc[edin_schools_rooms_sensors_batch['error_code'] == '1']
error_1 = error_1.drop_duplicates('DeviceEUI')
'''

edin_schools_rooms_sensors_batch = edin_schools_rooms_sensors_batch.rename(columns = {'error_code': 'Error Code'})
edin_schools_rooms_sensors_batch = edin_schools_rooms_sensors_batch[['School', 'Room', 'Device', 'Batch', 'Error Code']]
edin_schools_rooms_sensors_batch = edin_schools_rooms_sensors_batch.drop_duplicates('Device')

edin_complete_batch = edin.merge(schools_batch, left_on = 'DeviceEUI', right_on = 'Device', how = 'right')
edin_complete_batch = edin_complete_batch.dropna()

##############################

# Pulling out the last time each edin sensor gave a reading

conn_string = "postgresql://iopt_readonly:b!erHalle22@iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com/iopt"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) 

# 2. Change time frame to desired window

query = "SELECT device, MAX(time) FROM readings WHERE device IN {device} GROUP BY device".format(device=device, today=today, one_week_ago=one_week_ago, one_month_ago=one_month_ago, three_months_ago=three_months_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_max_time_edin.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

# Create device_all_df variable with output
device_all_max_time_edin = pd.read_csv('device_all_max_time_edin.csv', parse_dates=['max'])

device_all_max_time_edin['max'] = pd.to_datetime(device_all_max_time_edin['max'])
device_all_max_time_edin['max'] = device_all_max_time_edin['max'].dt.strftime('%Y-%m-%d %H:%M:%S')
one_week_ago = pd.to_datetime(one_week_ago)

coverage_issue = device_all_max_time_edin.loc[device_all_max_time_edin['max'] <= '2022-11-01']
coverage_issue['Status'] = 'Coverage Issue'
online = device_all_max_time_edin.loc[device_all_max_time_edin['max'] > '2022-11-01']
online['Status'] = 'Online'
coverage_online = coverage_issue.merge(online, on = 'device', how = 'outer')
all_sensors_with_status = edin.merge(coverage_online, left_on = 'DeviceEUI', right_on = 'device', how = 'left')
all_sensors_with_status = all_sensors_with_status[['UniqueReference', 'Name', 'device', 'Status_x', 'Status_y']]
all_sensors_with_status.to_csv('all_sensors_with_status.csv')

# Current work around - in excel merge statues and add offline to remainder then delete 'service' rooms

all_sensors_with_status_fixed = pd.read_csv("C:/Users/tomda/Downloads/all_sensors_with_status.csv")
all_sensors_with_status_fixed['Status'].value_counts()


Final_edin_list = all_sensors_with_status_fixed.merge(edin_schools_rooms_sensors_batch_error, left_on = ['UniqueReference', 'Name'], right_on = ['School', 'Room'], how = 'left')
Final_edin_list = Final_edin_list[['UniqueReference', 'Name', 'device', 'Status', 'Batch', 'Error Code']]

Final_edin_list = Final_edin_list.merge(edin_complete_batch, left_on = ['UniqueReference', 'Name'], right_on = ['School', 'Room'], how = 'right')
Final_edin_list = Final_edin_list[['School', 'Room', 'Device', 'Batch_x', 'Batch_y', 'Status', 'Error Code']]
Final_edin_list.to_csv('final_edin_list.csv')


final_final_edin_list = final_final_edin_list.merge(device_list_from_readings, left_on = 'Device', right_on = 'device', how = 'left')
final_final_edin_list.to_csv('final_edin_list.csv')

final_final_edin_list = pd.read_csv("C:/Users/tomda/Downloads/final_edin_list.csv")

school_fault_count = final_final_edin_list.pivot_table(index=['School'], columns=['Error Code'], aggfunc='size', fill_value=0)
school_status_count = final_final_edin_list.pivot_table(index=['School'], columns=['Status'], aggfunc='size', fill_value=0)

school_overall_count = school_status_count.merge(school_fault_count, on = 'School', how = 'inner')
school_overall_count.to_csv('school_overall_count.csv')

#################################################

final_audit = edin_complete_batch.merge(edin_schools_rooms_sensors_batch, left_on = 'DeviceEUI', right_on = 'Device', how = 'left')
final_audit = final_audit.merge(schools_batch, left_on = 'DeviceEUI', right_on = 'Device', how = 'left')
final_audit['Status'] = final_audit['Status'].fillna('Offline')
final_audit['Error Code'] = final_audit['Error Code'].fillna('none')
final_audit = final_audit[['UniqueReference', 'Name', 'Device_x', 'Batch_x', 'Error Code', 'Status']].rename(columns = {'UniqueReference': 'School', 'Name': 'Room', 'Device_x': 'Device', 'Batch_x': 'Batch'})
school_fault_count = list_for_emma_updated.pivot_table(index=['School'], columns=['Error Code'], aggfunc='size', fill_value=0)
school_status_count = list_for_emma_updated.pivot_table(index=['School'], columns=['Status'], aggfunc='size', fill_value=0)

school_overall_count = school_status_count.merge(school_fault_count, on = 'School', how = 'inner')

#school_overall_count.to_csv('Edinburgh School Sensor Audit.csv')

original_faults = pd.read_csv("C:/Users/tomda/Downloads/original_faults.csv")
calibration = pd.read_csv('C:/Users/tomda/Downloads/calibration.csv')
new_faults = final_master[['device']]
new_faults['fault'] = '2'

list_for_emma = final_final_edin_list.merge(original_faults, left_on = 'Device', right_on = 'device', how = 'left')
list_for_emma = list_for_emma.merge(calibration, left_on = 'Device', right_on = 'device', how = 'left')
list_for_emma = list_for_emma.merge(new_faults, left_on = 'Device', right_on = 'device', how = 'left')

list_for_emma.to_csv('list_for_emma.csv')

list_for_emma_updated = pd.read_csv('list_for_emma.csv')

'''
# Categorising error by the highest count in that error type
schools_error_count = schools_error_count.sort_values('device', ascending=False).drop_duplicates(['device'])
schools_error_count = schools_error_count.sort_values(['device'])
schools_error_count = schools_error_count [['device', 'error_code']]
schools_error_count['error_code'].value_counts()


schools_with_readings = edin_schools_rooms_sensors.merge(schools, left_on = 'device', right_on = 'Device', how = 'left')
schools_na = schools_with_readings.loc[schools_with_readings['Device'].isna()]

'''
