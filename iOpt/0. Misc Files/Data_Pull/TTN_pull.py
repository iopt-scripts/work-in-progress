import subprocess
import json
import pathlib
import re
import pandas as pd

"""
Comment in or out app and key pair, select hours (with h)
""" 

hours = '2160h'

#app = 'iopt-invisible-systems-3phase-ct'
#key = 'NNSXS.RFC6WPH22AUA2FJDGM5U6WLFRXFOVMMPKG7KD4Y.5K5PONKDDCTGRBH6I5YQ7SFU32OVAGHVUCANKQBMXAMR7NT3TVQA'

#app = 'robeau-flowmeters'
#key = 'NNSXS.HTU4ISHPT74ESM5NN7QVDEAYN4NJ47YWQAWSMFY.P42NTG3A5HI6YWT4VIBFLCU6CSATE6MADO7DRTUPTLTH4NQOGOJQ'

#app = 'iopt-fludia-gas'
#key = 'NNSXS.J2YABGERQH4Q4IFAZHY4UACK2VATDEK7ZUGMMHI.3VLSOTBQY6G4BAXUTWRIIP32LBGCDKDWYRS5KXGBEQXZCWKXAEFQ'

#app = 'iopt-lht65-temperature-probe'
#key = 'NNSXS.C32TEHSQKBKSEAMWPQN4E3VZNP2B5MZOAEWUISA.D5DZ32EGRUBPTYCQVST6ZSPPZAGDQQKPD7MCM4E2EY2PGWUKLBIQ' 
    
#app = 'iopt-131c-general'
#key = 'NNSXS.YJOUGGQUC5WAYRQSQPI7YAARO7RXMOXMPUIEBQI.56LGMYOEORBANCH2HVXMHJVYFN4H2ECWDPO7QKAY5WQF2K52ABNQ'

app = 'iopt-131a-general'
key = 'NNSXS.EDVRHGFN2WTFYYBOXPG62CVFCHSCK6FSQWHRRPQ.7XW7TOGE6GTQM6CYICKG25VM6F26KCDUN6RLBFQZ5J5LJPSS3K2A'

#Novus
#app = 'iopt-131a-novus-warrington'
#key = 'NNSXS.63B2FD5KFLPTSIN7IXCU4CS4KERZ33IREULIHGA.KHMEM5UVPXNFPV6A4RDIRQGU2YVDPA6Y5OOIP7FRD2A7QUADCN2Q'

#Albyn
#app = 'iopt-invisible-systems-ct-albyn'
#key = 'NNSXS.FY72RWYF43ECIPBBC4U5YUSKDBKZRAFVYF44PZY.ZL7RDYIONSCFHURT7W3JQLP3J3KVB63Z2JGSF6ATTJXQWYUE7KNQ'

#app = 'iopt-131a-albyn-inverness'
#key = 'NNSXS.V5LV6XUZS6WOOHXUOQRMYLK5DAAJXQQAEPYYE7Q.Z5STAXIQL3LKION2QE4XBZSH3V3LXASF6PXDS4XJYSMZBXRWGPQQ'

#Inch

#app = 'iopt-inch-elsys-ers'
#key = 'NNSXS.Q5CTRSN2JUAT7X77F2RKDZ4PEEPHDEHNCSKQHTY.WTKC5YEBUC2HM2FI5GRXD4DNZ2YUFGKSNM4AY4JOF57WN4F5PN4Q'

#app = 'iopt-inch-netvox-ct'
#key = 'NNSXS.EEZAFCA3PT7KEME3D26ZWMFKYFO3XIBX3UGMYDI.CBIKCMIXRHMCE2RAQGBMBEPONICBPGYPAMKB3TAB7KX6BBUSBV2Q'

print('Downloading data...')

class Error(Exception):
	"""Base class for errors in this module"""
	pass

class FetchError(Error):
	"""Raised when sensor_pull_storage can't deal with input
	Atrributes:
	   expression -- input expression where error occurred
	   message -- explanation of the error.
	"""
	def __init__(self, expression, message):
		self.expression = expression
		self.message = message

def sensor_pull_storage(appname, accesskey, timestring, *,data_folder = None, ttn_version=3):
	"""
	Pull data from TTN via the TTN storage API.
	appname is the name of the TTN app
	accesskey is the full accesskey from ttn.  For TTN V3, this is is the
		secret that is output when a key is created. For TTN V2, this is
		the string from the console, starting with 'ttn-acount-v2.'
	timestring indicates amount of data needed, e.g. '100h'.
	ttn_version should be 2 or 3; 3 is default.
	If data_folder is supplied, it is a string or a Path; it is taken as a directory,
	and the name "sensors_lastperiod.json" is appended to form an output file name, and
	the data is written to the resulting file, replacing any previous contents.
	Otherwise, the data is returned as a Python array (for V3) or a string (for V2).
	We've not really tested V2 extensively.
	"""
	args = [ "curl" ]
	if ttn_version == 2:
		args += [
			"-X", "GET",
			"--header", "Accept: application/json",
			"--header", f"Authorization: key {accesskey}",
			f"https://{appname}.data.thethingsnetwork.org/api/v2/query?last={timestring}"
			]
	elif ttn_version == 3:
		args += [
			"-G", f"https://ioptassets.eu1.cloud.thethings.industries/api/v3/as/applications/{appname}/packages/storage/uplink_message", # Edited to iopt url
			"--header", f"Authorization: Bearer {accesskey}",
			"--header", "Accept: text/event-stream",
			"-d", f"last={timestring}",
			"-d", "field_mask=up.uplink_message.decoded_payload",
		]
	else:
		raise FetchError(f"Illegal ttn_version (not 2 or 3)")


	# if the user supplied a data_folder, than tack on the args.
	# list1 += list2 syntax means "append each element of list2 to list 1"
	# pathlib.Path allows 
	if data_folder != None:
		args += [ "-o", pathlib.Path(data_folder, "sensors_lastperiod.json") ]

	result = subprocess.run( 
		args, shell=False, check=True, capture_output=True
		)

	sresult = result.stdout
	if ttn_version == 3:
		return list(map(json.loads, re.sub(r'\n+', '\n', sresult.decode()).splitlines()))
	else:
		return sresult



results_720h =  sensor_pull_storage(app, key, hours)

print(len(results_720h), 'readings found.')

fulldf = pd.json_normalize(results_720h)

fulldf.to_csv('TTN_device_readings.csv')

print('CSV written to file: TTN_device_readings.csv')

