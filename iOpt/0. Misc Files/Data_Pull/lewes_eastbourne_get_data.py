# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Pull tables in from database
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
power_df = pd.read_sql_table('solar_readings', engine)
# milliamps x 240V then / 1000 / 1000 to get kW then / 2 to take average over an hour to make kWh
power_df['reading'] = abs(power_df['reading']) * 240 / 1000 / 1000 / 2
weather_df = pd.read_sql_table('weather', engine)


power_df_39B = power_df.loc[power_df['device_id'] == '00137A1000013297']



engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# Lewes | Eastbourne
LandlordId = '2d4afd59-9599-461d-9bca-687d82fb72f7'

Landlord = properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

# create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:, 'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:, 'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{one_month_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

# Create device_all_df variable with output
device_all_lew_east = pd.read_csv('device_all.csv', parse_dates=['time'])

property_ids = Landlord_properties['Id']
property_id = tuple(property_ids)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE timestamp BETWEEN '{one_month_ago}' AND '{today}' AND property_id IN {property_id}".format(today=today, one_month_ago=one_month_ago, property_id=property_id)

cursor.execute(query, [tuple(property_id)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_power.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_power = pd.read_csv('device_all_power.csv', parse_dates=['timestamp'])
device_all_power['reading'] = abs(device_all_power['reading']) * 240 / 1000 / 1000
device_all_power = device_all_power.merge(properties_df, left_on = 'property_id', right_on = 'Id', how = 'left')
device_all_power = device_all_power.merge(addresses_df, left_on='AddressId', right_on='Id', how='left')
device_all_power = device_all_power[['AddressLine1', 'device_id', 'timestamp', 'device_type', 'reading']]

conditions = [
    (device_all_power['device_id'].str.contains('00137A1000013297')),
    (device_all_power['device_id'].str.contains('00137A1000013296')),
    (device_all_power['device_id'].str.contains('00137A1000013298')),
    (device_all_power['device_id'].str.contains('00137A10000153AC')),
    (device_all_power['device_id'].str.contains('00137A10000153B5')),
    (device_all_power['device_id'].str.contains('00137A10000153B2')),
    (device_all_power['device_id'].str.contains('00137A10000153B3')),
    (device_all_power['device_id'].str.contains('00137A1000015387')),
    (device_all_power['device_id'].str.contains('00137A1000015384'))
]
choices = ['11B Ashington Gardens', '39B Ashington Gardens', '3 The Downs', '5 School Gardens', '9 The Downs', '8 Beddingham Gardens', '4 School Gardens', '39C Ashington Gardens', '4 The Downs']

device_all_power['AddressLine1'] = np.select(conditions, choices)

device_all_power_resample = device_all_power.set_index('timestamp')
device_all_power_resample = device_all_power_resample.groupby('device_id').resample('30min').ffill().fillna(0)
device_all_power_resample = device_all_power_resample.drop('device_id', 1)

device_all_power = device_all_power.sort_values(["device_id", "timestamp"])
device_all_power_resample = device_all_power_resample.reset_index()
device_all_power['deltas'] = device_all_power['timestamp'].diff()
deltas = device_all_power.loc[device_all_power['deltas'] > timedelta(minutes=31)]

device_all_power_resample.to_csv('lewes_power_data_02_03.csv')

device_all_power.to_csv('device_all_power_lewes.csv')

#devices.to_csv("ren_devices_29_11_2021.csv", index=False)
#lew_east_rooms_propertysurveys_id = lew_east_rooms.merge(propertysurveys_df, on = 'PropertyId', how = 'inner')

lew_east = properties_df.loc[properties_df['LandlordId'] == '2d4afd59-9599-461d-9bca-687d82fb72f7']
lew_east_rooms = lew_east.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
lew_east_rooms_sensors = lew_east_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
lew_east_rooms_sensors_address = lew_east_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
lew_east_rooms_sensors_address['Address'] = lew_east_rooms_sensors_address['AddressLine1'].map(str) + ' ' + lew_east_rooms_sensors_address['AddressLine2'].map(str) + ' ' + lew_east_rooms_sensors_address['City'].map(str) + ' ' + lew_east_rooms_sensors_address['Code'].map(str)
lew_east_properties = lew_east_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

lew_east_properties_data = lew_east_properties.merge(device_all_lew_east, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
lew_east_properties_data['time'] = lew_east_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
lew_east_properties_data['time'] = pd.to_datetime(lew_east_properties_data['time'])

lew_east_data_last_month = lew_east_properties_data.loc[lew_east_properties_data['time'] >= one_month_ago]
lew_east_data_last_month = lew_east_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
lew_east_data_last_month = lew_east_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
lew_east_data_last_three_months = lew_east_properties_data.loc[lew_east_properties_data['time'] >= three_months_ago]
lew_east_data_last_three_months = lew_east_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
lew_east_data_last_three_months = lew_east_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

lew_east_averages_1m = lew_east_data_last_month.groupby(['Address', 'Name', 'device']).mean()
lew_east_averages_3m = lew_east_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
lew_east_averages_1m = lew_east_averages_1m.reset_index()
lew_east_averages_3m = lew_east_averages_3m.reset_index()

lew_east_averages = lew_east_averages_1m.merge(lew_east_averages_3m, on = 'device', how = 'inner')
lew_east_averages = lew_east_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (lew_east_averages['co2_1m_avg'].ge(600) & lew_east_averages['temperature_1m_avg'].lt(14)) ,
    (lew_east_averages['humidity_1m_avg'].ge(70) & lew_east_averages['temperature_1m_avg'] - lew_east_averages['dewpoint_1m_avg'].lt(4) & lew_east_averages['co2_1m_avg'].ge(1500)),
    (lew_east_averages['humidity_1m_avg'].ge(65) & lew_east_averages['temperature_1m_avg'] - lew_east_averages['dewpoint_1m_avg'].lt(5)),
    (lew_east_averages['co2_1m_avg'].ge(1300)),
    (lew_east_averages['co2_1m_avg'].lt(500) & lew_east_averages['temperature_1m_avg'].lt(13)),
    (lew_east_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

lew_east_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (lew_east_averages['co2_1m_avg'].ge(1600) | (lew_east_averages['temperature_1m_avg'].lt(14) & lew_east_averages['co2_1m_avg'].ge(550)) | lew_east_averages['humidity_1m_avg'].ge(70)),
    (lew_east_averages['co2_1m_avg'].ge(1300) | (lew_east_averages['temperature_1m_avg'].lt(15) & lew_east_averages['co2_1m_avg'].ge(550)) | lew_east_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

lew_east_averages['severity'] = np.select(conditions, choices)






# Temp Probe properties
temp_probe_data = lew_east_properties_data.loc[(lew_east_properties_data['device'].str.startswith('A', na=False))]
env_data = lew_east_properties_data.loc[(lew_east_properties_data['Address'].str.startswith(
    '9 The Downs', na=False)) & (lew_east_properties_data['device'].str.startswith('7', na=False))]
env_data.to_csv('env_9_the_downs.csv')

temp_probe_data['time'] = temp_probe_data['time'].dt.strftime(
    '%Y-%m-%d %H:%M:%S')
temp_probe_data['time'] = pd.to_datetime(temp_probe_data['time'])
env_data['time'] = env_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
env_data['time'] = pd.to_datetime(env_data['time'])

# Resampling Power for
device_all_power_resample = device_all_power.set_index(
    'timestamp').resample('15min').ffill()
device_all_power_9_the_downs = device_all_power.loc[device_all_power['device_id'] == '00137A10000153B5']
device_all_power_9_the_downs.to_csv('power_9_the_downs.csv')

# Set time delta
tol = pd.Timedelta('15 minute')
lew_east_power_env = pd.merge_asof(env_data.sort_values('time'), device_all_power.sort_values('timestamp'), left_on='time', right_on='timestamp', direction='nearest', tolerance=tol)
lew_east_power_env = lew_east_power_env[['device_id', 'device', 'reading', 'Name', 'time', 'temperature', 'co2', 'humidity']]
lew_east_power_env.to_csv('lew_east_power_env.csv')

lew_east_power_env = pd.merge_asof(power_8_bed, env_data.sort_values('time'), left_on='timestamp',
                                   right_on='time', left_by='property_id', right_by='PropertyId', direction='nearest', tolerance=tol)
lew_east_power_env = lew_east_power_env[['device_id', 'device', 'reading', 'Name', 'time', 'temperature', 'co2', 'humidity']]
lew_east_power_env.to_csv('lew_east_power_env.csv')

# lew_east_power_temp.to_csv('lew_east_temp_probe_data.csv')

# Merging env and weather
conditions = [
    (lew_east_properties_data['Address'].str.contains('Seaford')),
    (lew_east_properties_data['Address'].str.contains('Eastbourne')),
    (lew_east_properties_data['Address'].str.contains('Newick')),
    (lew_east_properties_data['Address'].str.contains('Glynde')),
    (lew_east_properties_data['Address'].str.contains('Peacehaven')),
    (lew_east_properties_data['Address'].str.contains('Barcombe'))
]
choices = ['Seaford', 'Eastbourne', 'Newick', 'Newick', 'Peacehaven', 'Newick']

lew_east_properties_data['area'] = np.select(conditions, choices)

lew_east_properties_data['time'] = lew_east_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
lew_east_properties_data['time'] = pd.to_datetime(lew_east_properties_data['time'])


weather_df_lew_east = weather_df.loc[(weather_df['code'] == 'Seaford') | (weather_df['code'] == 'Eastbourne') | (weather_df['code'] == 'Newick') | (weather_df['code'] == 'Peacehaven')]
#weather_df.to_csv('lew_east_weather.csv')

weather_df_seaford = weather_df.loc[(weather_df['code'] == 'Seaford')]
weather_df_seaford['time'] = weather_df_seaford['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df_seaford.to_csv('seaford_weather.csv')

weather_df_lew_east['time'] = weather_df_lew_east['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df_lew_east['time'] = pd.to_datetime(weather_df_lew_east['time'])

# Resampling Weather for
weather_3h = weather_df_lew_east.set_index(
    'time').resample('15min').ffill()

# Set time delta
tol = pd.Timedelta('3h')

lewes_env_weather = pd.merge_asof(lew_east_properties_data.sort_values('time'), weather_df_lew_east.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)

lewes_env_weather = lewes_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
lewes_env_weather = lewes_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2']]
lewes_env_weather.to_csv('lewes_28_02.csv')

lewes_48_bed = lewes_env_weather.loc[lewes_env_weather['device'] == '70B3D55F2000085A'].to_csv('lewes_48_bed.csv')
lewes_48_lounge = lewes_env_weather.loc[lewes_env_weather['device'] == '70B3D55F20000961'].to_csv('lewes_48_lounge.csv')

lewes_weather_corr = lewes_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]



# lew_east_averages.to_csv('lew_east_averages_over_last_month.csv')
