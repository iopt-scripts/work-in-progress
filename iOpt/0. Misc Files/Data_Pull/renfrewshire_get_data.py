# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
from datetime import timedelta
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
import seaborn as sns
from datetime import date
from dateutil.relativedelta import relativedelta

# Pull tables in from database
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# 1. Remove comments on LandlordId you require.

# Renfrewshire
LandlordId = '26bf6bb2-9f69-4596-adb2-6c7b82137cd1'


Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)
#
today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{one_month_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago)
query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN ('70B3D55F2000052F', '70B3D55F20000511', '70B3D55F2000069A', '70B3D55F2000084B') AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_ren variable with output
device_all_ren = pd.read_csv('device_all.csv', parse_dates=['time'])
device_all_ren['device'].nunique()


renfrewshire_prop = properties_df.loc[properties_df['LandlordId'] == '26bf6bb2-9f69-4596-adb2-6c7b82137cd1']
renfrewshire_prop_rooms = renfrewshire_prop.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
renfrewshire_prop_rooms_sensors = renfrewshire_prop_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
renfrewshire_prop_rooms_sensors_address = renfrewshire_prop_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
renfrewshire_prop_rooms_sensors_address['Address'] = renfrewshire_prop_rooms_sensors_address['AddressLine1'].map(str) + ' ' + renfrewshire_prop_rooms_sensors_address['AddressLine2'].map(str) + ' ' + renfrewshire_prop_rooms_sensors_address['City'].map(str) + ' ' + renfrewshire_prop_rooms_sensors_address['Code'].map(str)
renfrewshire_properties = renfrewshire_prop_rooms_sensors_address[['UniqueReference', 'Address', 'Name', 'DeviceEUI']]
#renfrewshire_properties.to_csv('ren_prop_with_devices.csv')

renfrewshire_prop_survey = renfrewshire_prop.merge(propertysurveys_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
#renfrewshire_prop_survey.to_csv('ren_prop_with_survery.csv')
renfrewshire_prop_survey['PropertyId'].nunique()

renfrewshire_properties_data = renfrewshire_properties.merge(device_all_ren, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')

conditions = [
    (renfrewshire_properties_data['Name'].str.contains('1')),
    (renfrewshire_properties_data['Name'].str.contains('2')),
    (renfrewshire_properties_data['Name'].str.contains('living'))
]
choices = ['Bedroom 1', 'Bedroom 2', 'Living Room']

renfrewshire_properties_data['Room'] = np.select(conditions, choices)

'''
renfrewshire_properties_data = renfrewshire_properties_data[['time', 'Room', 'temperature', 'co2', 'humidity']]
renfrewshire_properties_data = renfrewshire_properties_data.loc[renfrewshire_properties_data['time'] > '2021-03-01']
renfrewshire_properties_data.to_csv('ladyburn.csv')
'''

renfrewshire_data_last_month = renfrewshire_properties_data.loc[renfrewshire_properties_data['time'] >= one_month_ago]
renfrewshire_data_last_month = renfrewshire_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
renfrewshire_data_last_month = renfrewshire_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
renfrewshire_data_last_three_months = renfrewshire_properties_data.loc[renfrewshire_properties_data['time'] >= three_months_ago]
renfrewshire_data_last_three_months = renfrewshire_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
renfrewshire_data_last_three_months = renfrewshire_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

renfrewshire_averages_1m = renfrewshire_data_last_month.groupby(['Address', 'Name', 'device']).mean()
renfrewshire_averages_3m = renfrewshire_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
renfrewshire_averages_1m = renfrewshire_averages_1m.reset_index()
renfrewshire_averages_3m = renfrewshire_averages_3m.reset_index()

renfrewshire_averages = renfrewshire_averages_1m.merge(renfrewshire_averages_3m, on = 'device', how = 'inner')
renfrewshire_averages = renfrewshire_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (renfrewshire_averages['co2_1m_avg'].ge(600) & renfrewshire_averages['temperature_1m_avg'].lt(14)) ,
    (renfrewshire_averages['humidity_1m_avg'].ge(70) & renfrewshire_averages['temperature_1m_avg'] - renfrewshire_averages['dewpoint_1m_avg'].lt(4) & renfrewshire_averages['co2_1m_avg'].ge(1500)),
    (renfrewshire_averages['humidity_1m_avg'].ge(65) & renfrewshire_averages['temperature_1m_avg'] - renfrewshire_averages['dewpoint_1m_avg'].lt(5)),
    (renfrewshire_averages['co2_1m_avg'].ge(1300)),
    (renfrewshire_averages['co2_1m_avg'].lt(500) & renfrewshire_averages['temperature_1m_avg'].lt(13)),
    (renfrewshire_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

renfrewshire_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (renfrewshire_averages['co2_1m_avg'].ge(1600) | (renfrewshire_averages['temperature_1m_avg'].lt(14) & renfrewshire_averages['co2_1m_avg'].ge(550)) | renfrewshire_averages['humidity_1m_avg'].ge(70)),
    (renfrewshire_averages['co2_1m_avg'].ge(1300) | (renfrewshire_averages['temperature_1m_avg'].lt(15) & renfrewshire_averages['co2_1m_avg'].ge(550)) | renfrewshire_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

renfrewshire_averages['severity'] = np.select(conditions, choices)

renfrewshire_averages = renfrewshire_averages.loc[renfrewshire_averages['issue'] != '0']
renfrewshire_averages = renfrewshire_averages.loc[renfrewshire_averages['severity'] == 'critical']




john_july = renfrewshire_averages.merge(renfrewshire_prop_rooms_sensors_address, on = 'Address', how = 'left')
john_july = john_july[['Address', 'UniqueReference']].drop_duplicates()





#renfrewshire_issues.to_csv('renfrewshire_issues_01_22.csv')

fuel_pov = renfrewshire_issues.loc[renfrewshire_issues['issue'] == 'fuel poverty / heating issue']
fuel_pov = fuel_pov.reset_index()
fuel_pov = fuel_pov['Address']
fuel_pov = fuel_pov.drop_duplicates()
fuel_pov.to_csv('possible_renfrewshire_fuel_pov.csv')

Landlord_properties['AddressId']

renfrewshire_properties_data = renfrewshire_properties.merge(device_all_renfrewshire, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
renfrewshire_properties_data['time'] = renfrewshire_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
renfrewshire_properties_data['time'] = pd.to_datetime(renfrewshire_properties_data['time'])

'''
cuilmuir_11 = renfrewshire_properties_data.loc[(renfrewshire_properties_data['DeviceEUI'] == '70B3D55F200006D3') | (renfrewshire_properties_data['DeviceEUI'] == '70B3D55F200006C9') | (renfrewshire_properties_data['DeviceEUI'] == '70B3D55F200006DD')]
cuilmuir_11.to_csv('cuilmuir_11.csv')
'''

renfrewshire_properties_data['area'] = 'JOHNSTONE'

weather_df_renfrewshire = weather_df.loc[weather_df['code'] == 'JOHNSTONE']
weather_df_renfrewshire['time'] = weather_df_renfrewshire['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df_renfrewshire['time'] = pd.to_datetime(weather_df_renfrewshire['time'])

# Resampling Weather for
weather_3h = weather_df_renfrewshire.set_index(
    'time').resample('15min').ffill()

# Set time delta
tol = pd.Timedelta('3h')

renfrewshire_env_weather = pd.merge_asof(renfrewshire_properties_data.sort_values('time'), weather_df_renfrewshire.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)

renfrewshire_env_weather = renfrewshire_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
renfrewshire_env_weather = renfrewshire_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2']]

renfrewshire_weather_corr = renfrewshire_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]

renfrewshire_23a_bedroom = renfrewshire_env_weather.loc[renfrewshire_env_weather['device'] == '70B3D55F2000069E'].to_csv('renfrewshire_23_bed.csv')
renfrewshire_23a_lounge = renfrewshire_env_weather.loc[renfrewshire_env_weather['device'] == '70B3D55F200006A7'].to_csv('renfrewshire_23_lounge.csv')

renfrewshire_averages['Address'].nunique()
        
'''
left_joined = renfrewshire_properties.merge(
  renfrewshire_properties_data, how='left', 
  on='DeviceEUI', indicator=True)
left_joined.sample(5)
left_joined['_merge'].value_counts()
left_only = left_joined.loc[left_joined['_merge'] == 'left_only']

left_joined = renfrewshire_properties.merge(
  renfrewshire_last_month, how='left', 
  on='DeviceEUI', indicator=True)
left_joined.sample(5)
left_joined['_merge'].value_counts()
left_only = left_joined.loc[left_joined['_merge'] == 'left_only']
left_joined['DeviceEUI'].nunique()

left_only['Address_x'].nunique()
renfrewshire_properties_data['day'] = pd.DatetimeIndex(renfrewshire_properties_data['time']).day
renfrewshire_properties_data['week'] = pd.DatetimeIndex(renfrewshire_properties_data['time']).week
renfrewshire_properties_data['year'] = pd.DatetimeIndex(renfrewshire_properties_data['time']).year
renfrewshire_properties_data['month'] = pd.DatetimeIndex(renfrewshire_properties_data['time']).month
renfrewshire_properties_data['date'] = pd.DatetimeIndex(renfrewshire_properties_data['time']).date
renfrewshire_properties_data_grouped = renfrewshire_properties_data.groupby(['date', 'year', 'month', 'day', 'device']).agg({'temperature': 'mean', 'co2': 'mean', 'humidity': 'mean', 'dewpoint': 'mean', 'battery': 'mean'})
renfrewshire_properties_data_grouped = renfrewshire_properties_data_grouped.reset_index(level='device', drop = False)
grouped = renfrewshire_properties_data_grouped.groupby(['date', 'year', 'month', 'day'])
grouped_plot = grouped['device'].nunique()
grouped_plot = grouped_plot.reset_index()
g = sns.lineplot(data=grouped_plot, x="date", y="device", ci=None).legend_.remove()
'''

'''
# Looking at offline sensors

device_all_ren['device'].nunique()

# Installer Issues List
installer_issue_ren = list(set(device) - set(device_all_ren['device']))
installer_issue_ren = pd.DataFrame (installer_issue_ren, columns = ['device'])
installer_issue_ren['status'] = 'Offlien - Installation Issue'
installer_issue_ren = installer_issue_ren.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'inner')
installer_issue_ren = installer_issue_ren.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'left')
installer_issue_ren = installer_issue_ren.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
installer_issue_ren = installer_issue_ren.merge(propertysurveys_df, left_on = 'PropertyId', right_on = 'PropertyId', how = 'left')
installer_issue_ren = installer_issue_ren.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
installer_issue_ren['Address'] = installer_issue_ren['AddressLine1'].map(str) + ' ' + installer_issue_ren['AddressLine2'].map(str) + ' ' + installer_issue_ren['City'].map(str) + ' ' + installer_issue_ren['Code'].map(str)
installer_issue_ren = installer_issue_ren[['Address', 'Code', 'device', 'Name', 'status']]
installer_issue_ren = installer_issue_ren.drop_duplicates(subset=['device'])

# Gateway Issues List
gateway_issues = device_all_ren.sort_values(["device", "time"])
gateway_issues['time'] = pd.to_datetime(gateway_issues['time'])
gateway_issues['time'] = gateway_issues['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
gateway_issues['time'] = pd.to_datetime(gateway_issues['time'])
gateway_issues['deltas'] = gateway_issues['time'].diff()
gateway_issues = gateway_issues.loc[(gateway_issues['deltas'] >= timedelta(days=3)) & (gateway_issues['deltas'] <= timedelta(days=300))]
gateway_issues_last_3 = gateway_issues.loc[gateway_issues['time'] >= '2021-11-01 00:00:00'] 
gateway_issues_last_3 = gateway_issues_last_3[['device']]
gateway_issues_last_3['status'] = 'Gateway Issue'
gateway_issues_last_3 = gateway_issues_last_3.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'inner')
gateway_issues_last_3 = gateway_issues_last_3.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'left')
gateway_issues_last_3 = gateway_issues_last_3.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
gateway_issues_last_3 = gateway_issues_last_3.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
gateway_issues_last_3['Address'] = gateway_issues_last_3['AddressLine1'].map(str) + ' ' + gateway_issues_last_3['AddressLine2'].map(str) + ' ' + gateway_issues_last_3['City'].map(str) + ' ' + gateway_issues_last_3['Code'].map(str)
gateway_issues_last_3 = gateway_issues_last_3.drop_duplicates(subset=['device'])
gateway_issues_last_3 = gateway_issues_last_3[['Address', 'Code', 'device', 'Name', 'status']]

# Stopped working
stopped_working = device_all_ren.sort_values(["device", "time"])
stopped_working['time'] = pd.to_datetime(stopped_working['time'])
stopped_working['time'] = stopped_working['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
stopped_working['time'] = pd.to_datetime(stopped_working['time'])
stopped_working['deltas'] = stopped_working['time'].diff()
stopped_working = stopped_working.loc[(stopped_working['deltas'] > timedelta(days=300)) | (stopped_working['time'] <= '2021-11-01 00:00:00' & \
                                                                                         stopped_working['time'] > '2021-11-01 00:00:00')]
stopped_working = stopped_working[['device']]
stopped_working['status'] = 'Offline - Since Install'
stopped_working = stopped_working.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'inner')
stopped_working = stopped_working.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'left')
stopped_working = stopped_working.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
stopped_working = stopped_working.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
stopped_working['Address'] = stopped_working['AddressLine1'].map(str) + ' ' + stopped_working['AddressLine2'].map(str) + ' ' + stopped_working['City'].map(str) + ' ' + stopped_working['Code'].map(str)
stopped_working = stopped_working.drop_duplicates(subset=['device'])
stopped_working = stopped_working[['Address', 'Code', 'device', 'Name', 'status']]

#online_sensor = []
#online_sensor = pd.DataFrame(online_sensor)
#online_sensor['device'] = list(set(device) - set(installer_issue_ren['device']) - set(gateway_issues_last_3['device']))
online_sensor = device_all_ren.sort_values(["device", "time"])
online_sensor['time'] = online_sensor['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
online_sensor['time'] = pd.to_datetime(online_sensor['time'])
online_sensor['deltas'] = online_sensor['time'].diff()
online_sensor = online_sensor.loc[online_sensor['time'] >= '2021-11-01 00:00:00'] 
online_sensor = online_sensor.loc[online_sensor['deltas'] < timedelta(days=3)]
online_sensor = online_sensor[['device']]
online_sensor['status'] = 'Online'
online_sensor = online_sensor.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'left')
online_sensor = online_sensor.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'left')
online_sensor = online_sensor.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
online_sensor = online_sensor.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
online_sensor['Address'] = online_sensor['AddressLine1'].map(str) + ' ' + online_sensor['AddressLine2'].map(str) + ' ' + online_sensor['City'].map(str) + ' ' + online_sensor['Code'].map(str)
online_sensor = online_sensor[['Address', 'Code', 'device', 'Name', 'status']]
online_sensor = online_sensor.drop_duplicates(subset=['device'])

gateways = pd.read_csv('C:/Users/tomda/Documents/GitHub/iopt/raw_data/gateways.csv')
gateways['status'] = 'Gateway'
gateways['device'] = 'NA'
gateways['Name'] = 'NA'
gateways = gateways[['Address', 'Code', 'device', 'Name', 'status']]

ren_sensor_map = pd.concat([installer_issue_ren, gateway_issues_last_3, online_sensor, gateways])
ren_sensor_map.to_csv("ren_sensor_map.csv")

'''


engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
power_df = pd.read_sql_table('solar_readings', engine)

power_ren = power_df[(power_df['device_id'] == '00137A100000D211') | (power_df['device_id'] == '00137A100000D218') | (power_df['device_id'] == '00137A100000CB1B') | (power_df['device_id'] == '00137A100000D208')]



conditions = [
    (device_all_ren['device'] == '70B3D55F20000B24'),
    (device_all_ren['device'] == '70B3D55F20000B12'),
    (device_all_ren['device'] == '70B3D55F20000B0E'),
    (device_all_ren['device'] == '70B3D55F20000AFE'),
    (device_all_ren['device'] == '70B3D55F20000B00'),
    (device_all_ren['device'] == '70B3D55F20000B0F')
]
choices = ['Bedroom 1', 'Bedroom 2', 'Lounge', 'Bedroom 1', 'Bedroom 2', 'Lounge']

device_all_ren['Room'] = np.select(conditions, choices)


conditions = [
    (device_all_ren['device'] == '70B3D55F20000B24'),
    (device_all_ren['device'] == '70B3D55F20000B12'),
    (device_all_ren['device'] == '70B3D55F20000B0E'),
    (device_all_ren['device'] == '70B3D55F20000AFE'),
    (device_all_ren['device'] == '70B3D55F20000B00'),
    (device_all_ren['device'] == '70B3D55F20000B0F')
]
choices = ['56 Blackstoun', '56 Blackstoun', '56 Blackstoun', '121 Blackstoun', '121 Blackstoun', '121 Blackstoun']

device_all_ren['Address'] = np.select(conditions, choices)

conditions = [
    (power_ren['device_id'] == '00137A100000D211'),
    (power_ren['device_id'] == '00137A100000D218'),
    (power_ren['device_id'] == '00137A100000CB1B'),
    (power_ren['device_id'] == '00137A100000D208')
]
choices = ['56 Blackstoun', '56 Blackstoun', '121 Blackstoun', '121 Blackstoun']

power_ren['Address'] = np.select(conditions, choices)

device_all_ren.to_csv('ren_3m_environmental.csv')
power_ren.to_csv('ren_power.csv')
'''

address_count = renfrewshire_prop_survey.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'inner')
address_count = address_count[['UniqueReference', 'AddressLine1', 'AddressLine2', 'City', 'Code']]
address_count = address_count[['Address']]
address_count['Address'].nunique()
address_count[address_count.duplicated(keep=False)]
z = address_count.duplicated(subset = 'Address', keep=False)
dropped = address_count.drop_duplicates(['AddressLine1', 'AddressLine2', 'City', 'Code'])
dropped.to_csv('renfrewshire_installed_address_list.csv')

blackstoun = renfrewshire_properties_data.loc[renfrewshire_properties_data['Address'].str.contains('Blackstoun', case=False)]
blackstoun['Address'].value_counts()
blackstoun.to_csv('blackstoun_env_data.csv')




renfrewshire_properties_data['time'] = renfrewshire_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
renfrewshire_properties_data['time'] = pd.to_datetime(renfrewshire_properties_data['time'])

renfrewshire_properties_data['area'] = 'PAISLEY'

weather_df_renfrewshire = weather_df.loc[(weather_df['code'] == 'PAISLEY')]
#weather_df.to_csv('renfrewshire_weather.csv')


weather_df_renfrewshire['time'] = weather_df_renfrewshire['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df_renfrewshire['time'] = pd.to_datetime(weather_df_renfrewshire['time'])

# Resampling Weather for
weather_3h = weather_df_renfrewshire.set_index('time').resample('15min').ffill()

# Set time delta
tol = pd.Timedelta('3h')

renfrewshire_env_weather = pd.merge_asof(renfrewshire_properties_data.sort_values('time'), weather_df_renfrewshire.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)

renfrewshire_env_weather = renfrewshire_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
renfrewshire_env_weather = renfrewshire_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2']]
#renfrewshire_env_weather.to_csv('renfrewshire_28_02.csv')

renfrewshire_15_chapelhill = renfrewshire_env_weather.loc[(renfrewshire_env_weather['device'] == '70B3D55F20000829') | (renfrewshire_env_weather['device'] == '70B3D55F200007FE') | (renfrewshire_env_weather['device'] == '70B3D55F200007F1') | (renfrewshire_env_weather['device'] == '70B3D55F20000814')].to_csv('renfrewshire_15_chapelhill.csv')

renfrewshire_weather_corr = renfrewshire_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]

'''