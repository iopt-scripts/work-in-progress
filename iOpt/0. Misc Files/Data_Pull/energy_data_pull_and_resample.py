# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 12:16:20 2022

Resampling Power Data to fill in missing timestamps

@author: tomda
"""


# Edit code with <<< comment >>> above

import psycopg2
import psycopg2.extras
import datetime
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Import data from applications database
engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')

# <<< 1. Choose the Landlord you would like to use >>>
# Renfrewshire
#LandlordId = '26bf6bb2-9f69-4596-adb2-6c7b82137cd1'
# Cornwall
#LandlordId = 'b10f0221-ed28-4dc1-bcef-9f0853bd75e7'
# Maryhill
#LandlordId = '01e96558-1e52-480b-9a4e-4bc955f83489'
# Great Places
#LandlordId = '4337e818-dcf4-473b-8580-c1fb4ae17b4b'
# Loreburn
#LandlordId = '8fb0fac8-ea47-480d-95ba-c6e1c8be16cd'
# Lewes | Eastbourne
LandlordId = '2d4afd59-9599-461d-9bca-687d82fb72f7'
# NLC
#LandlordId = 'db8ad941-a28c-4a71-958d-a1dcba23f568'

# <<< 2. Remove comments on code chunk A for Renfrewshire and B for all other landlords >>>

''' A 
Landlord = out['LandlordId'] == LandlordId
Landlord_properties = out[Landlord]
'''

''' B
Landlord = properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]
'''

# Create list of enviromental devices from your landlord
Landlord_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @Landlord_props_id')
sensors = out_room.loc[:, 'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:, 'DeviceEUI']
device = tuple(devices)

# Define time ranges to input into SQL query of Sensor database
today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

# Create tuple of landlord property ids for querying solar readings db
property_ids = Landlord_properties['Id']
property_id = tuple(property_ids)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# <<< 3. Change time frame to desired window >>>

query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE timestamp BETWEEN '{one_month_ago}' AND '{today}' AND property_id IN {property_id}".format(today=today, one_month_ago=one_month_ago, property_id=property_id)

cursor.execute(query, [tuple(property_id)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_power.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_power = pd.read_csv('device_all_power.csv', parse_dates=['timestamp'])
# Convert to kW and wrangle
device_all_power['reading'] = abs(device_all_power['reading']) * 230 / 1000 / 1000
device_all_power = device_all_power.merge(properties_df, left_on = 'property_id', right_on = 'Id', how = 'left')
device_all_power = device_all_power.merge(addresses_df, left_on='AddressId', right_on='Id', how='left')
device_all_power = device_all_power[['AddressLine1', 'device_id', 'timestamp', 'device_type', 'reading']]

device_all_power_resample = device_all_power.set_index('timestamp')
device_all_power_resample = device_all_power_resample.groupby('device_id').resample('30min').ffill().fillna(0)
device_all_power_resample = device_all_power_resample.drop('device_id', 1)

device_all_power = device_all_power.sort_values(["device_id", "timestamp"])
device_all_power_resample = device_all_power_resample.reset_index()
device_all_power['deltas'] = device_all_power['timestamp'].diff()
deltas = device_all_power.loc[device_all_power['deltas'] > timedelta(minutes=31)]

# device_all_power_resample.to_csv('power_data_resample.csv')

