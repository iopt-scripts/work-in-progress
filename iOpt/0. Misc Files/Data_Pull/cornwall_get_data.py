# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')


# cornwall
LandlordId = 'b10f0221-ed28-4dc1-bcef-9f0853bd75e7'

Landlord =  out['LandlordId'] == LandlordId
Landlord_properties = out[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '02/01/2021 00:00:00' and '02/14/2022 00:00:00' AND device IN {device} AND NOT (co2 = 65535)".format(device=device)
query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_df variable with output
device_all_df = pd.read_csv('device_all.csv', parse_dates=['time'])


cornwall = properties_df.loc[properties_df['LandlordId'] == 'b10f0221-ed28-4dc1-bcef-9f0853bd75e7']
cornwall_rooms = cornwall.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
cornwall_rooms_sensors = cornwall_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
cornwall_rooms_sensors_address = cornwall_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
cornwall_rooms_sensors_address['Address'] = cornwall_rooms_sensors_address['AddressLine1'].map(str) + ' ' + cornwall_rooms_sensors_address['AddressLine2'].map(str) + ' ' + cornwall_rooms_sensors_address['City'].map(str) + ' ' + cornwall_rooms_sensors_address['Code'].map(str)
cornwall_properties = cornwall_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

cornwall_properties_data = cornwall_properties.merge(device_all_df, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')


cornwall_averages = nlc_properties_data.loc[nlc_properties_data['time'] > '12/25/2021 00:00:00']
cornwall_averages = cornwall_averages.groupby(['Address', 'Name']).mean()

conditions = [
    (cornwall_averages['co2'].ge(600) & cornwall_averages['temperature'].lt(14)) ,
    (cornwall_averages['humidity'].ge(70) & cornwall_averages['temperature'] - cornwall_averages['dewpoint'].lt(4) & cornwall_averages['co2'].ge(1500)),
    (cornwall_averages['humidity'].ge(65) & cornwall_averages['temperature'] - cornwall_averages['dewpoint'].lt(5)),
    (cornwall_averages['co2'].ge(1300)),
    (cornwall_averages['co2'].lt(500) & cornwall_averages['temperature'].lt(13)),
    (cornwall_averages['temperature'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

cornwall_averages['issue'] = np.select(conditions, choices)


'''
weather_df = weather_df.loc[weather_df['code'] == 'Kilsyth']
weather_df['time'] = weather_df['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df.to_csv('nlc_weather.csv')
'''
