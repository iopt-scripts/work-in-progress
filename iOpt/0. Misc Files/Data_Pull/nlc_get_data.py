# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Pull tables in from database
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# NLC
LandlordId = 'db8ad941-a28c-4a71-958d-a1dcba23f568'

Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_nlc.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

# Create device_all_df variable with output
device_all_nlc = pd.read_csv('device_all_nlc.csv', parse_dates=['time'])
'''
# Looing at Tinto 54
tinto_54 = device_all_nlc

conditions = [
    (tinto_54['device'] == '70B3D55F200006D0'),
    (tinto_54['device'] == '70B3D55F200006CE'),
     (tinto_54['device'] == '70B3D55F200006D4')   
]
choices = ['Bedroom 2', 'Lounge', 'Main Bedroom']

tinto_54['Room'] = np.select(conditions, choices)

tinto_54['time'] = tinto_54['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

tinto_54.to_csv('54_tinto_last_month.csv')
'''

nlc = properties_df.loc[properties_df['LandlordId'] == 'db8ad941-a28c-4a71-958d-a1dcba23f568']
nlc_rooms = nlc.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
nlc_rooms_sensors = nlc_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
nlc_rooms_sensors_address = nlc_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
nlc_rooms_sensors_address['Address'] = nlc_rooms_sensors_address['AddressLine1'].map(str) + ' ' + nlc_rooms_sensors_address['AddressLine2'].map(str) + ' ' + nlc_rooms_sensors_address['City'].map(str) + ' ' + nlc_rooms_sensors_address['Code'].map(str)
nlc_properties = nlc_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

nlc_properties_data = nlc_properties.merge(device_all_nlc, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
nlc_properties_data['time'] = nlc_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
nlc_properties_data['time'] = pd.to_datetime(nlc_properties_data['time'])

nlc_data_last_month = nlc_properties_data.loc[nlc_properties_data['time'] >= one_month_ago]
nlc_data_last_month = nlc_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
nlc_data_last_month = nlc_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
nlc_data_last_three_months = nlc_properties_data.loc[nlc_properties_data['time'] >= three_months_ago]
nlc_data_last_three_months = nlc_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
nlc_data_last_three_months = nlc_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

nlc_averages_1m = nlc_data_last_month.groupby(['Address', 'Name', 'device']).mean()
nlc_averages_3m = nlc_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
nlc_averages_1m = nlc_averages_1m.reset_index()
nlc_averages_3m = nlc_averages_3m.reset_index()

nlc_averages = nlc_averages_1m.merge(nlc_averages_3m, on = 'device', how = 'inner')
nlc_averages = nlc_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (nlc_averages['co2_1m_avg'].ge(600) & nlc_averages['temperature_1m_avg'].lt(14)) ,
    (nlc_averages['humidity_1m_avg'].ge(70) & nlc_averages['temperature_1m_avg'] - nlc_averages['dewpoint_1m_avg'].lt(4) & nlc_averages['co2_1m_avg'].ge(1500)),
    (nlc_averages['humidity_1m_avg'].ge(65) & nlc_averages['temperature_1m_avg'] - nlc_averages['dewpoint_1m_avg'].lt(5)),
    (nlc_averages['co2_1m_avg'].ge(1300)),
    (nlc_averages['co2_1m_avg'].lt(500) & nlc_averages['temperature_1m_avg'].lt(13)),
    (nlc_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

nlc_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (nlc_averages['co2_1m_avg'].ge(1600) | (nlc_averages['temperature_1m_avg'].lt(14) & nlc_averages['co2_1m_avg'].ge(550)) | nlc_averages['humidity_1m_avg'].ge(70)),
    (nlc_averages['co2_1m_avg'].ge(1300) | (nlc_averages['temperature_1m_avg'].lt(15) & nlc_averages['co2_1m_avg'].ge(550)) | nlc_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

nlc_averages['severity'] = np.select(conditions, choices)

nlc_averages_critical = nlc_averages.loc[nlc_averages['severity'] == 'critical']
nlc_averages_critical['issue'].value_counts()
nlc_averages_critical['Address'].nunique()
'''
cuilmuir_11 = nlc_properties_data.loc[(nlc_properties_data['DeviceEUI'] == '70B3D55F200006D3') | (nlc_properties_data['DeviceEUI'] == '70B3D55F200006C9') | (nlc_properties_data['DeviceEUI'] == '70B3D55F200006DD')]
cuilmuir_11.to_csv('cuilmuir_11.csv')
'''

nlc_properties_data['area'] = 'Kilsyth'

weather_df_nlc = weather_df.loc[weather_df['code'] == 'Kilsyth']
weather_df_nlc['time'] = weather_df_nlc['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df_nlc['time'] = pd.to_datetime(weather_df_nlc['time'])

# Resampling Weather for
weather_3h = weather_df_nlc.set_index(
    'time').resample('15min').ffill()

# Set time delta
tol = pd.Timedelta('3h')

nlc_env_weather = pd.merge_asof(nlc_properties_data.sort_values('time'), weather_df_nlc.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)

nlc_env_weather = nlc_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
nlc_env_weather = nlc_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2']]

nlc_weather_corr = nlc_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]

nlc_11_bedroom = nlc_env_weather.loc[nlc_env_weather['device'] == '70B3D55F200006D3'].to_csv('nlc_11_bed.csv')
nlc_11_lounge = nlc_env_weather.loc[nlc_env_weather['device'] == '70B3D55F200006D3'].to_csv('nlc_11_lounge.csv')
'''
hazel_rd_193 = device_all_df
hazel_rd_193_bedroom = hazel_rd_193.loc[hazel_rd_193['device'] == '70B3D55F200006C3']
hazel_rd_193_bedroom['temperature'].mean()
hazel_rd_193_bedroom['time'] = hazel_rd_193_bedroom['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
hazel_rd_193_first_5 = hazel_rd_193_bedroom.loc[hazel_rd_193_bedroom['time'] <= '2022-02-01 04:00:00']
hazel_rd_193_first_5['temperature'].mean()
hazel_rd_193.to_csv('193_hazel_road_last_10_days.csv')
'''
