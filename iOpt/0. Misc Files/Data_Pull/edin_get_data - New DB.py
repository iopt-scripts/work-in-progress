# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Pull tables in from database
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

engine = create_engine('postgresql://iopt_readonly:b!erHalle22@fat-uat-application.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com/iopt_uat')
rooms_df = pd.read_sql_table('Areas', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# edin_schools
OrganisationId = 'b934cedd-ba92-4422-972a-fdf12d00cfa7'

Landlord =  properties_df['OrganisationId'] == OrganisationId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
one_week_ago = date.today() + relativedelta(weeks=-1)
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "postgresql://iopt_readonly:b!erHalle22@fat-uat-application.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com/iopt_uat"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{one_week_ago}' AND '{today}' AND device IN {device}".format(device=device, today=today, one_week_ago=one_week_ago, one_month_ago=one_month_ago, three_months_ago=three_months_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_edin_schools.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

# Create device_all_df variable with output
device_all_edin_schools = pd.read_csv('device_all_edin_schools.csv', parse_dates=['time'])

edin_schools = properties_df.loc[properties_df['OrganisationId'] == 'b934cedd-ba92-4422-972a-fdf12d00cfa7']
edin_schools_rooms = edin_schools.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
edin_schools_rooms_sensors = edin_schools_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
edin_schools_rooms_sensors_address = edin_schools_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
edin_schools_rooms_sensors_address['Address'] = edin_schools_rooms_sensors_address['AddressLine1'].map(str) + ' ' + edin_schools_rooms_sensors_address['AddressLine2'].map(str) + ' ' + edin_schools_rooms_sensors_address['City'].map(str) + ' ' + edin_schools_rooms_sensors_address['Code'].map(str)
edin_schools_properties = edin_schools_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

edin_schools_properties_data = edin_schools_properties.merge(device_all_edin_schools, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
edin_schools_properties_data['time'] = edin_schools_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
edin_schools_properties_data['time'] = pd.to_datetime(edin_schools_properties_data['time'])

'''
edin_schools_data_last_month = edin_schools_properties_data.loc[edin_schools_properties_data['time'] >= one_month_ago]
edin_schools_data_last_month = edin_schools_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
edin_schools_data_last_month = edin_schools_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
edin_schools_data_last_three_months = edin_schools_properties_data.loc[edin_schools_properties_data['time'] >= three_months_ago]
edin_schools_data_last_three_months = edin_schools_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
edin_schools_data_last_three_months = edin_schools_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

edin_schools_averages_1m = edin_schools_data_last_month.groupby(['Address', 'Name', 'device']).mean()
edin_schools_averages_3m = edin_schools_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
edin_schools_averages_1m = edin_schools_averages_1m.reset_index()
edin_schools_averages_3m = edin_schools_averages_3m.reset_index()

edin_schools_averages = edin_schools_averages_1m.merge(edin_schools_averages_3m, on = 'device', how = 'inner')
edin_schools_averages = edin_schools_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (edin_schools_averages['co2_1m_avg'].ge(600) & edin_schools_averages['temperature_1m_avg'].lt(14)) ,
    (edin_schools_averages['humidity_1m_avg'].ge(70) & edin_schools_averages['temperature_1m_avg'] - edin_schools_averages['dewpoint_1m_avg'].lt(4) & edin_schools_averages['co2_1m_avg'].ge(1500)),
    (edin_schools_averages['humidity_1m_avg'].ge(65) & edin_schools_averages['temperature_1m_avg'] - edin_schools_averages['dewpoint_1m_avg'].lt(5)),
    (edin_schools_averages['co2_1m_avg'].ge(1300)),
    (edin_schools_averages['co2_1m_avg'].lt(500) & edin_schools_averages['temperature_1m_avg'].lt(13)),
    (edin_schools_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

edin_schools_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (edin_schools_averages['co2_1m_avg'].ge(1600) | (edin_schools_averages['temperature_1m_avg'].lt(14) & edin_schools_averages['co2_1m_avg'].ge(550)) | edin_schools_averages['humidity_1m_avg'].ge(70)),
    (edin_schools_averages['co2_1m_avg'].ge(1300) | (edin_schools_averages['temperature_1m_avg'].lt(15) & edin_schools_averages['co2_1m_avg'].ge(550)) | edin_schools_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

edin_schools_averages['severity'] = np.select(conditions, choices)

edin_schools_averages_critical = edin_schools_averages.loc[edin_schools_averages['severity'] == 'critical']
edin_schools_averages_critical['issue'].value_counts()
edin_schools_averages_critical['Address'].nunique()
'''

device_all_edin_schools['device'].nunique()

all_devs['device'].nunique()
