# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 14:47:07 2022

@author: tomda
"""

edin_schools = device_all_df.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'left')
edin_schools = edin_schools.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'inner')
edin_schools = edin_schools.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'inner')
edin_schools = edin_schools.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
edin_schools['Address'] = edin_schools['AddressLine1'].map(str) + ' ' + edin_schools['AddressLine2'].map(str) + ' ' + edin_schools['City'].map(str) + ' ' + edin_schools['Code'].map(str)
edin_schools = edin_schools[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

conditions = [
    (edin_schools['co2'].ge(1400) & edin_schools['type'] == 'multiuse',
     edin_schools['co2'].ge(1100) & edin_schools['co2'].lt(1400) & edin_schools['type'] == 'multiuse',
     edin_schools['co2'].lt(1100) & edin_schools['type'] == 'multiuse',
     edin_schools['co2'].ge(800) & edin_schools['type'] == 'classroom',
     edin_schools['co2'].ge(700) & edin_schools['co2'].lt(800) & edin_schools['type'] == 'classroom',
     edin_schools['co2'].lt(700) & edin_schools['type'] == 'classroom')   
]
choices = ['red', 'amber', 'green', 'red', 'amber', 'green']

edin_schools['threshold'] = np.select(conditions, choices)

edin_schools.group_by(room_id, )

edin_schools.to_csv('edin_schools_env_sensors.csv')