# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 10:56:48 2022

@author: tomda
"""


import psycopg2
import psycopg2.extras
import datetime
from datetime import timedelta
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
import seaborn as sns
from datetime import date
from dateutil.relativedelta import relativedelta

engine = create_engine('postgresql://iopt_readonly:b!erHalle22@iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com:5432/iopt')
area_df = pd.read_sql_table('Areas', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)
organisation_df = pd.read_sql_table('Organisations', engine)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

# Create list of all sensors 
all_sensors = sensors_df.loc[sensors_df['RecordState'] == 0]
devices = all_sensors.loc[:,'DeviceEUI']
device = tuple(devices)

# Pull all readings from all devices in database

conn_string = "host='iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com' dbname='iopt' user='iopt_readonly' password='b!erHalle22'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, areaid FROM readings WHERE time BETWEEN '2022-12-10 00:00:00' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('all_readings.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_ren variable with output
all_readings = pd.read_csv('all_readings.csv', parse_dates=['time'])

all_props = all_readings.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'inner')
all_prop_rooms_sensors = all_props.merge(area_df, left_on = 'AreaId', right_on = 'Id', how = 'inner')
all_prop_rooms_sensors_address = all_prop_rooms_sensors.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
all_prop_rooms_sensors_address_org = all_prop_rooms_sensors_address.merge(organisation_df, left_on = 'OrganisationId', right_on = 'Id', how = 'left')
#all_prop_rooms_sensors_address = all_prop_rooms_sensors.merge(address_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
#all_prop_rooms_sensors_address['Address'] = all_prop_rooms_sensors_address['AddressLine1'].map(str) + ' ' + all_prop_rooms_sensors_address['AddressLine2'].map(str) + ' ' + all_prop_rooms_sensors_address['City'].map(str) + ' ' + all_prop_rooms_sensors_address['Code'].map(str)
#all_properties = all_prop_rooms_sensors_address[['UniqueReference', 'Address', 'Name', 'DeviceEUI']]

fuel_pov = all_prop_rooms_sensors_address_org[['OrganisationName', 'UniqueReference', 'Name', 'DeviceEUI', 'temperature', 'co2']]
fuel_pov = fuel_pov.loc[fuel_pov['co2'] > 700]
fuel_pov_mean = fuel_pov.groupby(['OrganisationName', 'UniqueReference', 'Name'])['temperature'].mean()
fuel_pov_mean = fuel_pov_mean.reset_index()
fuel_pov_mean = fuel_pov_mean.loc[fuel_pov_mean['temperature'] < 15]

#formatting
fuel_pov_mean = fuel_pov_mean[['OrganisationName', 'UniqueReference', 'Name', 'temperature']].rename(columns = {'UniqueReference': 'Property', 'Name': 'Room', 'temperature': 'Average Temperature (Last 48h)'})

for i, g in fuel_pov_mean.groupby(['OrganisationName']):
    g.to_csv('{}.csv'.format(i), header=True, index_label=True)

