# -*- coding: utf-8 -*-
"""
Created on Thu May 26 10:10:25 2022

@author: tomda
"""

import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

# Not all properties have surveys so can't use this
'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# 1. Remove comments on LandlordId you require.

# Balgreen Primary School
LandlordId = 'c1c56b0c-88b3-41b9-be16-44980887b346'


Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
one_week_ago = date.today() + relativedelta(weeks=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_week_ago = one_week_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{one_week_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_week_ago=one_week_ago)
#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_ren variable with output
device_all_balgreen_primary = pd.read_csv('device_all.csv', parse_dates=['time'])

balgreen = properties_df.loc[properties_df['LandlordId'] == 'c1c56b0c-88b3-41b9-be16-44980887b346']
balgreen_rooms = balgreen.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
balgreen_rooms_sensors = balgreen_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
balgreen_rooms_sensors_address = balgreen_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
balgreen_rooms_sensors_address['Address'] = balgreen_rooms_sensors_address['AddressLine1'].map(str) + ' ' + balgreen_rooms_sensors_address['AddressLine2'].map(str) + ' ' + balgreen_rooms_sensors_address['City'].map(str) + ' ' + balgreen_rooms_sensors_address['Code'].map(str)
balgreen_properties = balgreen_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

balgreen_properties_data = balgreen_properties.merge(device_all_balgreen_primary, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
balgreen_properties_data = balgreen_properties_data[['UniqueReference', 'Name', 'time', 'temperature', 'co2', 'humidity', 'dewpoint']]
balgreen_properties_data['time'] = balgreen_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
balgreen_properties_data['time'] = pd.to_datetime(balgreen_properties_data['time'])
'''
balgreen_properties_data = balgreen_properties_data.set_index('time')
balgreen_properties_data.index = pd.to_datetime(balgreen_properties_data.index)
balgreen_properties_data_resample = balgreen_properties_data.groupby(['UniqueReference', 'Name']).resample('15min').ffill()
balgreen_properties_data_resample = balgreen_properties_data_resample[['temperature', 'co2', 'humidity', 'dewpoint']]
balgreen_properties_data_resample = balgreen_properties_data_resample.reset_index(drop=False)

balgreen_properties_data_resample.to_excel('balgreen.xlsx')

pivot_table = pd.pivot_table(balgreen_properties_data_resample, values = 'co2', index=['UniqueReference','Name'], columns = 'time').reset_index()
pivot_table.to_excel('balgreen_pivot_table.xlsx')
'''

'''
balgreen_properties_data['day'] = balgreen_properties_data['time'].dt.day_name()
balgreen_properties_data = balgreen_properties_data.groupby(['Name', 'day']).agg(['min', 'mean', 'max'])

balgreen_properties_data = balgreen_properties_data.set_index('time')
balgreen_properties_data.index = pd.to_datetime(balgreen_properties_data.index)
balgreen_properties_data['mean'] = balgreen_properties_data.groupby(['UniqueReference', 'Name']).resample('D', on = 'time').mean()
'''

balgreen_properties_data_agg = balgreen_properties_data.groupby(['Name', balgreen_properties_data['time'].dt.date]).agg(['min', 'mean', 'max'])

with pd.ExcelWriter('balgreen_primary.xlsx') as writer:  
    balgreen_properties_data_agg.to_excel(writer, sheet_name='Summary Data')
    balgreen_properties_data.to_excel(writer, sheet_name='Raw Data')
    
balgreen_properties_data.to_csv('balgreen_properties_data.csv')

'''
balgreen_properties_data_resample_day = balgreen_properties_data.groupby(['UniqueReference', 'Name']).resample('1D').ffill()
balgreen_properties_data_resample_day = balgreen_properties_data_resample[['temperature', 'co2', 'humidity', 'dewpoint']]
balgreen_properties_data_resample_day = balgreen_properties_data_resample.reset_index(drop=False)
'''



import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

emailfrom = "tom.davie@ioptassets.com"
emailto = "tomdavie5@hotmail.com"
fileToSend = "balgreen_properties_data.csv"
username = "tom.davie@ioptassets.com"
password = "Maximus25!"

msg = MIMEMultipart()
msg["From"] = emailfrom
msg["To"] = emailto
msg["Subject"] = "Weekly Environmental Sensor Data"
msg.preamble = "Weekly Environmental Sensor Data"

ctype, encoding = mimetypes.guess_type(fileToSend)
if ctype is None or encoding is not None:
    ctype = "application/octet-stream"

maintype, subtype = ctype.split("/", 1)

if maintype == "text":
    fp = open(fileToSend)
    # Note: we should handle calculating the charset
    attachment = MIMEText(fp.read(), _subtype=subtype)
    fp.close()
elif maintype == "image":
    fp = open(fileToSend, "rb")
    attachment = MIMEImage(fp.read(), _subtype=subtype)
    fp.close()
elif maintype == "audio":
    fp = open(fileToSend, "rb")
    attachment = MIMEAudio(fp.read(), _subtype=subtype)
    fp.close()
else:
    fp = open(fileToSend, "rb")
    attachment = MIMEBase(maintype, subtype)
    attachment.set_payload(fp.read())
    fp.close()
    encoders.encode_base64(attachment)
attachment.add_header("Content-Disposition", "attachment", filename=fileToSend)
msg.attach(attachment)

server = smtplib.SMTP("smtp.gmail.com:587")
server.starttls()
server.login(username,password)
server.sendmail(emailfrom, emailto, msg.as_string())
server.quit()


