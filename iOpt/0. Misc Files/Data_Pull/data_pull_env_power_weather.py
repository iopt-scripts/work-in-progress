# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 11:56:05 2022

Title: Pull for Enviromental, Power and Weather Data for a Landlord

@author: tomda
"""

# Edit code with <<< comment >>> above

import psycopg2
import psycopg2.extras
import datetime
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Import data from applications database
engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')

# <<< 1. Choose the Landlord you would like to use >>>
# Renfrewshire
#LandlordId = '26bf6bb2-9f69-4596-adb2-6c7b82137cd1'
# Cornwall
#LandlordId = 'b10f0221-ed28-4dc1-bcef-9f0853bd75e7'
# Maryhill
#LandlordId = '01e96558-1e52-480b-9a4e-4bc955f83489'
# Great Places
#LandlordId = '4337e818-dcf4-473b-8580-c1fb4ae17b4b'
# Loreburn
#LandlordId = '8fb0fac8-ea47-480d-95ba-c6e1c8be16cd'
# Lewes | Eastbourne
#LandlordId = '2d4afd59-9599-461d-9bca-687d82fb72f7'
# NLC
#LandlordId = 'db8ad941-a28c-4a71-958d-a1dcba23f568'
#LiveWest
LandlordId = 'f7e5e481-e96e-4844-9603-f0b5e6444cd3'

# <<< 2. For Renfrewshire uncomment A and delete B below >>>

''' 
# A 
Landlord = out['LandlordId'] == LandlordId
Landlord_properties = out[Landlord]
'''

# B
Landlord = properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]


# Create list of enviromental devices from your landlord
Landlord_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @Landlord_props_id')
sensors = out_room.loc[:, 'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:, 'DeviceEUI']
device = tuple(devices)

# Define time ranges to input into SQL query of Sensor database
today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

# Query sensor database for environmental sensors
conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# <<< 3. Change time frame to desired window >>>

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

# Create device_all_df variable with output
device_all = pd.read_csv('device_all.csv', parse_dates=['time'])
device_all = device_all.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'left')
device_all = device_all.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'left')
device_all = device_all.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
device_all = device_all.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
device_all['Address'] = device_all['AddressLine1'].map(str) + ' ' + device_all['AddressLine2'].map(str) + ' ' + device_all['City'].map(str) + ' ' + device_all['Code'].map(str)
device_all_env = device_all[['Address', 'Name', 'time', 'temperature', 'co2', 'humidity', 'battery', 'dewpoint', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

device_all_env['time'] = device_all_env['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
device_all_env['time'] = pd.to_datetime(device_all_env['time'])

data_last_month = device_all_env.loc[device_all_env['time'] >= one_month_ago]
data_last_month = data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
data_last_month = data_last_month[['Address', 'Name', 'DeviceEUI', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
data_last_three_months = device_all_env.loc[device_all_env['time'] >= three_months_ago]
data_last_three_months = data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
data_last_three_months = data_last_three_months[['Address', 'Name', 'DeviceEUI', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

averages_1m = data_last_month.groupby(['Address', 'Name', 'DeviceEUI']).mean()
averages_3m = data_last_three_months.groupby(['Address', 'Name', 'DeviceEUI']).mean()
averages_1m = averages_1m.reset_index()
averages_3m = averages_3m.reset_index()

averages = averages_1m.merge(averages_3m, on = 'DeviceEUI', how = 'inner')
averages = averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (averages['co2_1m_avg'].ge(600) & averages['temperature_1m_avg'].lt(14)) ,
    (averages['humidity_1m_avg'].ge(70) & averages['temperature_1m_avg'] - averages['dewpoint_1m_avg'].lt(4) & averages['co2_1m_avg'].ge(1500)),
    (averages['humidity_1m_avg'].ge(65) & averages['temperature_1m_avg'] - averages['dewpoint_1m_avg'].lt(5)),
    (averages['co2_1m_avg'].ge(1300)),
    (averages['co2_1m_avg'].lt(500) & averages['temperature_1m_avg'].lt(13)),
    (averages['temperature_1m_avg'].ge(27))
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (averages['co2_1m_avg'].ge(1600) | (averages['temperature_1m_avg'].lt(14) & averages['co2_1m_avg'].ge(550)) | averages['humidity_1m_avg'].ge(70)),
    (averages['co2_1m_avg'].ge(1300) | (averages['temperature_1m_avg'].lt(15) & averages['co2_1m_avg'].ge(550)) | averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

averages['severity'] = np.select(conditions, choices)

# Create tuple of landlord property ids for querying solar readings db
property_ids = Landlord_properties['Id']
property_id = tuple(property_ids)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# <<< 4. Change time frame to desired window >>>

query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE timestamp BETWEEN '{three_months_ago}' AND '{today}' AND property_id IN {property_id}".format(today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago, property_id=property_id)

cursor.execute(query, [tuple(property_id)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_power.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_power = pd.read_csv('device_all_power.csv', parse_dates=['timestamp'])
# Convert to kW and wrangle
device_all_power['reading'] = abs(device_all_power['reading']) * 230 / 1000 / 1000
device_all_power = device_all_power.merge(properties_df, left_on = 'property_id', right_on = 'Id', how = 'left')
device_all_power = device_all_power.merge(addresses_df, left_on='AddressId', right_on='Id', how='left')
device_all_power = device_all_power[['AddressLine1', 'device_id', 'timestamp', 'device_type', 'reading']]

# Pull in weather table from database 
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)           
# Create tuple of citysfor querying weather db
city = device_all['City']
city = tuple(city)
           
# Query sensor database for environmental sensors
conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# <<< 5. Change time frame to desired window >>>

query = "SELECT code, time, temp, humidity FROM weather WHERE time BETWEEN '{one_month_ago}' AND '{today}' AND code IN {city}".format(today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago, city=city)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_weather.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_weather = pd.read_csv('device_all_weather.csv', parse_dates=['time'])

