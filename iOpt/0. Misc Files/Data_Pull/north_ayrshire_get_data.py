# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Pull tables in from database
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)



'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''
# 1. Remove comments on LandlordId you require.

LandlordId = '04c7bb8d-88a2-42ca-8353-b0f345b67616'

Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)
#
today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
two_months_ago = date.today() + relativedelta(months=-2)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
two_months_ago = two_months_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')


conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, two_months_ago=two_months_ago, three_months_ago=three_months_ago)
#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all_north_ayrshire.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_north_ayrshire variable with output
device_all_north_ayrshire = pd.read_csv('device_all_north_ayrshire.csv', parse_dates=['time'])

device_all_north_ayrshire['time'] = pd.to_datetime(device_all_north_ayrshire['time'])
device_all_north_ayrshire['time'] = device_all_north_ayrshire['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

north_ayrshire_prop = properties_df.loc[properties_df['LandlordId'] == '04c7bb8d-88a2-42ca-8353-b0f345b67616']
north_ayrshire_prop_rooms = north_ayrshire_prop.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
north_ayrshire_prop_rooms_sensors = north_ayrshire_prop_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
north_ayrshire_prop_rooms_sensors_address = north_ayrshire_prop_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
north_ayrshire_prop_rooms_sensors_address['Address'] = north_ayrshire_prop_rooms_sensors_address['AddressLine1'].map(str) + ' ' + north_ayrshire_prop_rooms_sensors_address['AddressLine2'].map(str) + ' ' + north_ayrshire_prop_rooms_sensors_address['City'].map(str) + ' ' + north_ayrshire_prop_rooms_sensors_address['Code'].map(str)
north_ayrshire_properties = north_ayrshire_prop_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference']]

north_ayrshire_properties_data = north_ayrshire_properties.merge(device_all_north_ayrshire, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
#north_ayrshire_properties_data.to_csv('north_ayrshire_properties.csv')

north_ayrshire_properties_data['time'] = pd.to_datetime(north_ayrshire_properties_data['time'])
north_ayrshire_properties_data['time'] = north_ayrshire_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

north_ayrshire_data_last_month = north_ayrshire_properties_data.loc[(north_ayrshire_properties_data['time'] >= two_months_ago) & (north_ayrshire_properties_data['time'] >= three_months_ago)]
north_ayrshire_data_last_month = north_ayrshire_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
north_ayrshire_data_last_month = north_ayrshire_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
north_ayrshire_data_last_three_months = north_ayrshire_properties_data.loc[north_ayrshire_properties_data['time'] >= three_months_ago]
north_ayrshire_data_last_three_months = north_ayrshire_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
north_ayrshire_data_last_three_months = north_ayrshire_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

north_ayrshire_averages_1m = north_ayrshire_data_last_month.groupby(['Address', 'Name', 'device']).mean()
north_ayrshire_averages_3m = north_ayrshire_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
north_ayrshire_averages_1m = north_ayrshire_averages_1m.reset_index()
north_ayrshire_averages_3m = north_ayrshire_averages_3m.reset_index()

north_ayrshire_averages = north_ayrshire_averages_1m.merge(north_ayrshire_averages_3m, on = 'device', how = 'inner')
north_ayrshire_averages = north_ayrshire_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (north_ayrshire_averages['co2_1m_avg'].ge(600) & north_ayrshire_averages['temperature_1m_avg'].lt(14)) ,
    (north_ayrshire_averages['humidity_1m_avg'].ge(70) & north_ayrshire_averages['temperature_1m_avg'] - north_ayrshire_averages['dewpoint_1m_avg'].lt(4) & north_ayrshire_averages['co2_1m_avg'].ge(1500)),
    (north_ayrshire_averages['humidity_1m_avg'].ge(65) & north_ayrshire_averages['temperature_1m_avg'] - north_ayrshire_averages['dewpoint_1m_avg'].lt(5)),
    (north_ayrshire_averages['co2_1m_avg'].ge(1300)),
    (north_ayrshire_averages['co2_1m_avg'].lt(500) & north_ayrshire_averages['temperature_1m_avg'].lt(13)),
    (north_ayrshire_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

north_ayrshire_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (north_ayrshire_averages['co2_1m_avg'].ge(1500) | (north_ayrshire_averages['temperature_1m_avg'].lt(14) & north_ayrshire_averages['co2_1m_avg'].ge(550)) | north_ayrshire_averages['humidity_1m_avg'].ge(70)),
    (north_ayrshire_averages['co2_1m_avg'].ge(1300) | (north_ayrshire_averages['temperature_1m_avg'].lt(15) & north_ayrshire_averages['co2_1m_avg'].ge(550)) | north_ayrshire_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

north_ayrshire_averages['severity'] = np.select(conditions, choices)
north_ayrshire_averages_critical = north_ayrshire_averages[north_ayrshire_averages['severity'] == 'critical']

north_ayrshire_power_df['timestamp'] = device_all_power['timestamp'].dt.strftime('%Y-%m-%d %H:%M:%S')
north_ayrshire_power_df['timestamp'] = pd.to_datetime(north_ayrshire_power_df['timestamp']).sort_values()

north_ayrshire_power_props = pd.merge_asof(north_ayrshire_properties_data, north_ayrshire_power_df, left_on = 'device', right_on = 'device_id', left_by = 'time', right_by = 'timestamp')

north_ayrshire_weather_df = weather_df.loc[(weather_df['code'] == 'Knutsford') | (weather_df['code'] == 'Fleetwood')]
north_ayrshire_weather_df['time'] = pd.to_datetime(north_ayrshire_weather_df['time'])
north_ayrshire_weather_df['time'] = north_ayrshire_weather_df['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

# Merging env and weather
conditions = [
    (north_ayrshire_properties_data['Address'].str.contains('Longridge')),
    (north_ayrshire_properties_data['Address'].str.contains('Shaw')),
    (north_ayrshire_properties_data['Address'].str.contains('Southfields')),
    (north_ayrshire_properties_data['Address'].str.contains('Overfields')),
    (north_ayrshire_properties_data['Address'].str.contains('Edgerton'))
    
]
choices = ['Knutsford', 'Knutsford', 'Knutsford', 'Knutsford', 'Knutsford']

north_ayrshire_properties_data['area'] = np.select(conditions, choices, default='Fleetwood')
north_ayrshire_averages['Address'].nunique()

north_ayrshire_properties_data.to_csv('north_ayrshire_env_data_0903.csv')

north_ayrshire_properties_data['time'] = pd.to_datetime(north_ayrshire_properties_data['time'])
north_ayrshire_properties_data['time'] = north_ayrshire_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

# Resampling Weather for
weather_3h = north_ayrshire_weather_df.set_index('time').resample('15min').ffill()

# Set time delta
tol = pd.Timedelta('3h')

north_ayrshire_env_weather = pd.merge_asof(north_ayrshire_properties_data.sort_values('time'), north_ayrshire_weather_df.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)

north_ayrshire_env_weather = north_ayrshire_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
north_ayrshire_env_weather = north_ayrshire_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2']]
north_ayrshire_env_weather.to_csv('north_ayrshire_09_03.csv')

north_ayrshire_18wc_lounge = north_ayrshire_env_weather.loc[north_ayrshire_env_weather['device'] == '70B3D55F2000087F'].to_csv('north_ayrshire_18wc_lounge.csv')

north_ayrshire_weather_corr = north_ayrshire_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]
    

device_all_north_ayrshire['device'].nunique()
