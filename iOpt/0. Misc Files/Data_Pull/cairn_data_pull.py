# -*- coding: utf-8 -*-
"""
Created on Mon May 23 11:22:03 2022

@author: tomda
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

# Not all properties have surveys so can't use this
'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# 1. Remove comments on LandlordId you require.

# Cairn
LandlordId = 'a7d7d467-c6f1-488e-8fac-532a9c63b772'


Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, three_months_ago=three_months_ago)
#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_ren variable with output
device_all_cairn = pd.read_csv('device_all.csv', parse_dates=['time'])

device_all_cairn['device'].nunique()

cairn = properties_df.loc[properties_df['LandlordId'] == 'a7d7d467-c6f1-488e-8fac-532a9c63b772']
cairn_rooms = cairn.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
cairn_rooms_sensors = cairn_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
cairn_rooms_sensors_address = cairn_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
cairn_rooms_sensors_address['Address'] = cairn_rooms_sensors_address['AddressLine1'].map(str) + ' ' + cairn_rooms_sensors_address['AddressLine2'].map(str) + ' ' + cairn_rooms_sensors_address['City'].map(str) + ' ' + cairn_rooms_sensors_address['Code'].map(str)
cairn_properties = cairn_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

cairn_properties_data = cairn_properties.merge(device_all_cairn, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
cairn_properties_data.to_csv('cairn_env_sensors.csv')

cairn_data_last_month = cairn_properties_data.loc[cairn_properties_data['time'] >= one_month_ago]
cairn_data_last_month = cairn_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
cairn_data_last_month = cairn_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
cairn_data_last_three_months = cairn_properties_data.loc[cairn_properties_data['time'] >= three_months_ago]
cairn_data_last_three_months = cairn_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
cairn_data_last_three_months = cairn_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

# cairn Visit

cairn_data_pre_visit = cairn_properties_data.loc[(cairn_properties_data['time'] < '2022-04-13') & (cairn_properties_data['time'] > '2022-03-30')] 
cairn_data_pre_visit = cairn_data_pre_visit.rename(columns = {'temperature': 'temperature_pre_avg', 'humidity': 'humidity_pre_avg', 'co2': 'co2_pre_avg', 'battery': 'battery_pre_avg', 'dewpoint': 'dewpoint_pre_avg'})
cairn_data_pre_visit = cairn_data_pre_visit[['Address', 'Name', 'device', 'temperature_pre_avg', 'humidity_pre_avg', 'co2_pre_avg', 'battery_pre_avg', 'dewpoint_pre_avg']]
cairn_data_post_visit = cairn_properties_data.loc[(cairn_properties_data['time'] > '2022-04-13') & (cairn_properties_data['time'] < '2022-04-27')] 
cairn_data_post_visit = cairn_data_post_visit.rename(columns = {'temperature': 'temperature_post_avg', 'humidity': 'humidity_post_avg', 'co2': 'co2_post_avg', 'battery': 'battery_post_avg', 'dewpoint': 'dewpoint_post_avg'})
cairn_data_post_visit = cairn_data_post_visit[['Address', 'Name', 'device', 'temperature_post_avg', 'humidity_post_avg', 'co2_post_avg', 'battery_post_avg', 'dewpoint_post_avg']]
cairn_data_post_4_visit = cairn_properties_data.loc[(cairn_properties_data['time'] > '2022-04-27') & (cairn_properties_data['time'] < '2022-05-10')] 
cairn_data_post_4_visit = cairn_data_post_4_visit.rename(columns = {'temperature': 'temperature_post_4_avg', 'humidity': 'humidity_post_4_avg', 'co2': 'co2_post_4_avg', 'battery': 'battery_post_4_avg', 'dewpoint': 'dewpoint_post_4_avg'})
cairn_data_post_4_visit = cairn_data_post_4_visit[['Address', 'Name', 'device', 'temperature_post_4_avg', 'humidity_post_4_avg', 'co2_post_4_avg', 'battery_post_4_avg', 'dewpoint_post_4_avg']]

cairn_averages_pre = cairn_data_pre_visit.groupby(['Address', 'Name', 'device']).mean()
cairn_averages_post = cairn_data_post_visit.groupby(['Address', 'Name', 'device']).mean()
cairn_averages_post_4 = cairn_data_post_4_visit.groupby(['Address', 'Name', 'device']).mean()
cairn_averages_pre = cairn_averages_pre.reset_index()
cairn_averages_post = cairn_averages_post.reset_index()
cairn_averages_post_4 = cairn_averages_post_4.reset_index()
cairn_averages_pre_post = cairn_averages_pre.merge(cairn_averages_post, on = 'device', how = 'inner')
cairn_averages_pre_post_4 = cairn_averages_pre_post.merge(cairn_averages_post_4, on = 'device', how = 'inner')
cairn_averages_pre_post_4 = cairn_averages_pre_post_4[['Address_x', 'Name_x', 'temperature_pre_avg', 'temperature_post_avg', 'temperature_post_4_avg', 'humidity_pre_avg', 'humidity_post_avg', 'humidity_post_4_avg', 'co2_pre_avg', 'co2_post_avg', 'co2_post_4_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
cairn_averages_pre_post_4 = cairn_averages_pre_post_4.loc[(cairn_averages_pre_post_4['Address'].str.contains('1B')) | (cairn_averages_pre_post_4['Address'].str.contains('4D')) | (cairn_averages_pre_post_4['Address'].str.contains('2A'))]
cairn_averages_pre_post_4.to_csv('cairn_visit.csv')

# cairn Visit - End

cairn_averages_1m = cairn_data_last_month.groupby(['Address', 'Name', 'device']).mean()
cairn_averages_3m = cairn_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
cairn_averages_1m = cairn_averages_1m.reset_index()
cairn_averages_3m = cairn_averages_3m.reset_index()

cairn_averages = cairn_averages_1m.merge(cairn_averages_3m, on = 'device', how = 'inner')
cairn_averages = cairn_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (cairn_averages['co2_1m_avg'].ge(550) & cairn_averages['temperature_1m_avg'].lt(14)) ,
    (cairn_averages['humidity_1m_avg'].ge(70) & cairn_averages['temperature_1m_avg'] - cairn_averages['dewpoint_1m_avg'].lt(4) & cairn_averages['co2_1m_avg'].ge(1500)),
    (cairn_averages['humidity_1m_avg'].ge(65) & cairn_averages['temperature_1m_avg'] - cairn_averages['dewpoint_1m_avg'].lt(5)),
    (cairn_averages['co2_1m_avg'].ge(1300)),
    (cairn_averages['co2_1m_avg'].lt(500) & cairn_averages['temperature_1m_avg'].lt(13)),
    (cairn_averages['temperature_1m_avg'].ge(27))
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

cairn_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (cairn_averages['co2_1m_avg'].ge(2000) | (cairn_averages['temperature_1m_avg'].lt(14) & cairn_averages['co2_1m_avg'].ge(550)) | cairn_averages['humidity_1m_avg'].ge(70)),
    (cairn_averages['co2_1m_avg'].ge(1300) | (cairn_averages['temperature_1m_avg'].lt(15) & cairn_averages['co2_1m_avg'].ge(550)) | cairn_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

cairn_averages['severity'] = np.select(conditions, choices)


weather_df = weather_df.loc[(weather_df['code'] == 'Moffat') | (weather_df['code'] == 'Castle Douglas') | (weather_df['code'] == 'Stranraer')]
weather_df['time'] = pd.to_datetime(weather_df['time'])
weather_df['time'] = weather_df['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

# Merging env and weather
conditions = [
    (cairn_properties_data['Address'].str.contains('Smith Way')),
    (cairn_properties_data['Address'].str.contains('Castle Douglas')),
    (cairn_properties_data['Address'].str.contains('Stranraer'))   
]
choices = ['Moffat', 'Castle Douglas', 'Stranraer']

cairn_properties_data['area'] = np.select(conditions, choices)

conditions = [
    (cairn_properties_data['Address'].str.contains('Smith Way')),
    (cairn_properties_data['Address'].str.contains('Bengairn View') | cairn_properties_data['Address'].str.contains('1B McCormack') | cairn_properties_data['Address'].str.contains('4D McCormack Gardens')),
    (cairn_properties_data['Address'].str.contains('McCormack'))
]
choices = ['Wet Electric', 'Storage Heaters', 'Gas']

cairn_properties_data['heating_system'] = np.select(conditions, choices) 


cairn_properties_data['time'] = cairn_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
cairn_properties_data['time'] = pd.to_datetime(cairn_properties_data['time'])

weather_df['time'] = pd.to_datetime(weather_df['time'])
# Resampling Weather for
weather_3h = weather_df.set_index('time').resample('3H').ffill()

# Set time delta
tol = pd.Timedelta('3 hours')

cairn_env_weather = pd.merge_asof(cairn_properties_data.sort_values('time'), weather_df.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)
cairn_env_weather = cairn_env_weather.loc[cairn_env_weather['time'] > '2022-02-03 04:57:00']

cairn_env_weather = cairn_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
cairn_env_weather = cairn_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2', 'area']]
#cairn_env_weather.to_csv('cairn_weather_env.csv')


cairn_weather_corr_humidity = cairn_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]
cairn_weather_corr_humidity = cairn_weather_corr_humidity.reset_index()
cairn_weather_corr_temperature = cairn_env_weather.groupby('device')[['outdoor_temperature','indoor_temperature']].corr().unstack().iloc[:,1]
cairn_weather_corr_temperature = cairn_weather_corr_temperature.reset_index()


cairn_humidity_corr_prop = cairn_properties.merge(cairn_weather_corr_humidity, left_on = 'DeviceEUI', right_on = 'device', how = 'left')
cairn_humidity_corr_prop.to_csv('cairn_humidity_corr.csv')

cairn_temperature_corr_prop = cairn_properties.merge(cairn_weather_corr_temperature, left_on = 'DeviceEUI', right_on = 'device', how = 'left')
cairn_temperature_corr_prop.to_csv('cairn_temperature_corr.csv')

'''
from scipy.stats import pearsonr
corr = pearsonr(cairn_env_weather['indoor_temperature'], cairn_env_weather['outdoor_temperature'])
print(corr)
'''

# Temp Decay
'''
decay_mean = pd.Series(decay_means, name='decay_mean')
decay_mean = decay_mean.reset_index()

heat_decay = decay_mean.merge(cairn_properties, left_on = 'index', right_on = 'DeviceEUI', how = 'left')
heat_decay.to_csv('cairn_heat_decay_23032022.csv')

cairn_properties_data.to_csv('cairn_properties_data_by_area.csv')
'''


'''
mccormack = cairn_properties_data.loc[cairn_properties_data['UniqueReference'].str.contains('McCormack')]
mccormack_march = mccormack.loc[(mccormack['time'] >= '2022-03-01 00:00:00+00:00') & (mccormack['time'] < '2022-04-01 00:00:00+00:00')]
mccormack_march.to_csv('mccormack_march_22.csv')
'''

# Energy

property_ids = Landlord_properties['Id']
property_id = tuple(property_ids)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

#query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE timestamp BETWEEN '{one_month_ago}' AND '{today}' AND device_id IN ('00137A100000CB23', '00137A100000D217')".format(today=today, one_month_ago=one_month_ago, property_id=property_id)
query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE device_id IN ('00137A100000CB23', '00137A100000D217')".format(today=today, one_month_ago=one_month_ago, property_id=property_id)

cursor.execute(query, [tuple(property_id)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_power.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_power_cairn = pd.read_csv('device_all_power.csv', parse_dates=['timestamp'])
device_all_power_cairn['reading'] = abs(device_all_power_cairn['reading']) * 230 / 1000 / 1000 / 2
device_all_power_cairn = device_all_power_cairn.merge(properties_df, left_on = 'property_id', right_on = 'Id', how = 'left')
device_all_power_cairn = device_all_power_cairn.merge(addresses_df, left_on='AddressId', right_on='Id', how='left')
device_all_power_cairn = device_all_power_cairn[['AddressLine1', 'device_id', 'timestamp', 'device_type', 'reading']]

conditions = [
    (device_all_power_cairn['device_id'] == '00137A100000CB23'),
    (device_all_power_cairn['device_id'] == '00137A100000D217')
]
choices = ['14 Balliol Court', '17 Balliol Court']

device_all_power_cairn['Address'] = np.select(conditions, choices)

device_all_power_cairn= device_all_power_cairn.loc[device_all_power_cairn['timestamp'] > '2022-03-28']
device_all_power_cairn.to_csv('energy_average_cairn.csv')

env_sensors_for_power_comparison = cairn_properties_data.loc[(cairn_properties_data['device'] == '70B3D55F200010C6') | (cairn_properties_data['device'] == '70B3D55F200010CE') | (cairn_properties_data['device'] == '70B3D55F200010EA') | (cairn_properties_data['device'] == '70B3D55F200010D0') | (cairn_properties_data['device'] == '70B3D55F200010D4') | (cairn_properties_data['device'] == '70B3D55F200010CA') | (cairn_properties_data['device'] == '70B3D55F200010C5')]
env_sensors_for_power_comparison.to_csv('cairn_power_env_comparison.csv')


------------

#Power

cairn_power = pd.read_csv('C:/Users/tomda/Downloads/Devlin Court - Power Consumption Monitoring (Watt Hours)-1653292922207.csv')
cairn_power['Date and Time'] = pd.to_datetime(cairn_power['Date and Time'])
cairn_power = cairn_power.set_index('Date and Time').resample('30min').mean()
cairn_power['Apartment 9'] = cairn_power['Apartment 9'] / 1000 / 2
cairn_power['Apartment 5'] = cairn_power['Apartment 5'] / 1000 / 2
cairn_power['Apartment 20'] = cairn_power['Apartment 20'] / 1000 / 2
cairn_power['Apartment 37'] = cairn_power['Apartment 37'] / 1000 / 2

cairn_power.to_csv('cairn_power.csv')




