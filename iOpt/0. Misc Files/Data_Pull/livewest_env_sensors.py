# -*- coding: utf-8 -*-
"""
Created on Wed May 18 12:57:09 2022

@author: tomda
"""


import psycopg2
import psycopg2.extras
import datetime
from datetime import timedelta
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
import seaborn as sns
from datetime import date
from dateutil.relativedelta import relativedelta


engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# Live West
LandlordId = 'f7e5e481-e96e-4844-9603-f0b5e6444cd3'

Landlord = properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

# create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:, 'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:, 'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

# Create device_all_df variable with output
device_all_live_west = pd.read_csv('device_all.csv', parse_dates=['time'])

engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)


live_west = properties_df.loc[properties_df['LandlordId'] == 'f7e5e481-e96e-4844-9603-f0b5e6444cd3']
live_west_rooms = live_west.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
live_west_rooms_sensors = live_west_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
live_west_rooms_sensors_address = live_west_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
live_west_rooms_sensors_address['Address'] = live_west_rooms_sensors_address['AddressLine1'].map(str) + ' ' + live_west_rooms_sensors_address['AddressLine2'].map(str) + ' ' + live_west_rooms_sensors_address['City'].map(str) + ' ' + live_west_rooms_sensors_address['Code'].map(str)
live_west_properties = live_west_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

live_west_properties_data = live_west_properties.merge(device_all_live_west, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
live_west_properties_data['time'] = live_west_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
live_west_properties_data['time'] = pd.to_datetime(live_west_properties_data['time'])

#live_west_properties_data.to_csv('live_west.csv')

live_west_data_last_month = live_west_properties_data.loc[live_west_properties_data['time'] >= one_month_ago]
live_west_data_last_month = live_west_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
live_west_data_last_month = live_west_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
live_west_data_last_three_months = live_west_properties_data.loc[live_west_properties_data['time'] >= three_months_ago]
live_west_data_last_three_months = live_west_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
live_west_data_last_three_months = live_west_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

live_west_averages_1m = live_west_data_last_month.groupby(['Address', 'Name', 'device']).mean()
live_west_averages_3m = live_west_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
live_west_averages_1m = live_west_averages_1m.reset_index()
live_west_averages_3m = live_west_averages_3m.reset_index()

live_west_averages = live_west_averages_1m.merge(live_west_averages_3m, on = 'device', how = 'inner')
live_west_averages = live_west_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (live_west_averages['co2_1m_avg'].ge(600) & live_west_averages['temperature_1m_avg'].lt(14)) ,
    (live_west_averages['humidity_1m_avg'].ge(70) & live_west_averages['temperature_1m_avg'] - live_west_averages['dewpoint_1m_avg'].lt(4) & live_west_averages['co2_1m_avg'].ge(1500)),
    (live_west_averages['humidity_1m_avg'].ge(65) & live_west_averages['temperature_1m_avg'] - live_west_averages['dewpoint_1m_avg'].lt(5)),
    (live_west_averages['co2_1m_avg'].ge(1300)),
    (live_west_averages['co2_1m_avg'].lt(500) & live_west_averages['temperature_1m_avg'].lt(13)),
    (live_west_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

live_west_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (live_west_averages['co2_1m_avg'].ge(1600) | (live_west_averages['temperature_1m_avg'].lt(14) & live_west_averages['co2_1m_avg'].ge(550)) | live_west_averages['humidity_1m_avg'].ge(70)),
    (live_west_averages['co2_1m_avg'].ge(1300) | (live_west_averages['temperature_1m_avg'].lt(15) & live_west_averages['co2_1m_avg'].ge(550)) | live_west_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

live_west_averages['severity'] = np.select(conditions, choices)

live_west_averages_critical = live_west_averages.loc[live_west_averages['severity'] == 'critical']
live_west_averages_critical['address_room'] = live_west_averages_critical['Address'] + ' ' + live_west_averages_critical['Name']
live_west_averages_critical.to_csv('live_west_critical.csv')

live_west_averages_critical['issue'] = live_west_averages['issue'].astype(pd.api.types.CategoricalDtype(categories = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']))
live_west_averages_critical = live_west_averages_critical.sort_values('issue')
live_west_averages_critical_count = live_west_averages_critical
live_west_averages_critical_count = live_west_averages_critical_count.drop_duplicates('Address', keep = 'first')
live_west_averages_critical_count = live_west_averages_critical_count['issue'].value_counts()

livewest = livewest.loc[~livewest['Site'].str.contains('ADHOC')]
livewest = livewest.rename(columns = {'7 Swallowfields Totones' : '7 Swallowfields, Totnes TQ9 5LA, UK '})

conditions = [
    (livewest['Site'] == '7 Swallowfields Totones'),
    (livewest['Site'] == ' 39 Halveor Court, Perranporth, Cornwall, TR6 0LR'),
    (livewest['Site'] == '26 Treveneth Crescent'),
    (livewest['Site'] == '30 Mythyon Court')
]

choices = ['7 Swallowfields, Totnes TQ9 5LA, UK', '39 Halveor Court, Perranporth, Cornwall, TR6 0LR', '26 Treveneth Cres, Newlyn, Penzance TR18 5NG, UK', '13 Mythyon Ct, Heamoor, Penzance TR18 3QU, UK']

livewest['Site'] = np.select(conditions, choices, default = livewest['Site'])

livewest_averages = livewest_averages.drop_duplicates(subset = ['Address', 'Location_x'], keep = 'last')


conditions = [
    (livewest['Site'].str.contains('Halveor')),
    (livewest['Site'].str.contains('Kernick')),
    (livewest['Site'].str.contains('Roscadghill')),
    (livewest['Site'].str.contains('Parc')),
    (livewest['Site'].str.contains('Mythyon')),
    (livewest['Site'].str.contains('Lansdowne')),
    (livewest['Site'].str.contains('Chywoone')),
    (livewest['Site'].str.contains('Treveneth')),
    (livewest['Site'].str.contains('House')),
    (livewest['Site'].str.contains('Yealm')),
    (livewest['Site'].str.contains('Afflington')),
    (livewest['Site'].str.contains('Main')),
    (livewest['Site'].str.contains('Victory')),
    (livewest['Site'].str.contains('Butterlake')),
    (livewest['Site'].str.contains('Swallowfields')),
    (livewest['Site'].str.contains('Pathfields'))
    
]
choices = ['Penzance', 'Penzance', 'Penzance', 'Penzance', 'Penzance', 'Penzance', 'Penzance', 'Penzance', 'Penzance', 'Plymouth', 'Plymouth', 'Dartmouth', 'Dartmouth', 'Dartmouth', 'Dartmouth', 'Dartmouth']

livewest['area'] = np.select(conditions, choices, default = livewest['Site'])

weather_df['time'] = weather_df['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df['time'] = pd.to_datetime(weather_df['time'])
livewest['Time'] = livewest['Time'].dt.strftime('%Y-%m-%d %H:%M:%S')
livewest['Time'] = pd.to_datetime(livewest['Time'])

# Resampling Weather for
weather_3h = weather_df.set_index('Time').resample('15min').ffill() 

# Set Time delta
tol = pd.Timedelta('3h')

livewest_env_weather = pd.merge_asof(livewest.sort_values('Time'), weather_df.sort_values('time'), left_on='Time', right_on = 'time', left_by='area', right_by='code', direction='nearest', tolerance=tol)

livewest_env_weather = livewest_env_weather.rename(columns = {'humidity_x': 'indoor_humidity', 'humidity_y': 'outdoor_humidity'})
livewest_env_weather = livewest_env_weather[['Site', 'Location', 'Device ID', 'Time', 'indoor_humidity', 'outdoor_humidity']]
livewest_env_weather_corr = livewest_env_weather.groupby('Device ID')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]

livewest_env_weather_corr = livewest_env_weather_corr.reset_index()
livewest_env_weather_corr = livewest_env_weather_corr.merge(livewest, on = 'Device ID', how = 'left')
livewest_env_weather_corr = livewest_env_weather_corr.drop_duplicates(['Site', 'Location'])
livewest_env_weather_corr.to_csv('livewest_weather_corr.csv')

'''
roscadghill_51 = livewest.loc[livewest['Site'] == '51 Roscadghill Parc, Penzance, TR18 3QY']
roscadghill_51.to_csv('51_roscadghill.csv')

livewest.to_csv('livewest_env.csv')

livewest_all = pd.read_csv('C:/Users/tomda/Downloads/b36d3bdb-fec2-47e1-94c3-42b4a36a5940.csv')
livewest_all['Time'] = pd.to_dateTime(livewest_all['Time'], unit = 'ms', origin='unix')
livewest_all.to_csv('livewest_all.csv')

engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)




roscadghill_51['Time'] = roscadghill_51['Time'].dt.strfTime('%Y-%m-%d %H:%M:%S')
roscadghill_51['Time'] = pd.to_dateTime(roscadghill_51['Time'])
roscadghill_51_weather = roscadghill_51[['Time', 'Site', 'Location', 'Device ID', 'humidity']]
roscadghill_51_weather['Time'] = pd.to_dateTime(roscadghill_51_weather['Time'])

'''
livewest_weather_penz = weather_df.loc[weather_df['code'] == 'Penzance']
livewest_weather_penz.to_csv('livewest_weather_penz.csv')
livewest_env_weather.to_csv('livewest_env_weather.csv')
