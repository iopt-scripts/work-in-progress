# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta

# Pull tables in from database
engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)



'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''
# 1. Remove comments on LandlordId you require.

LandlordId = '4337e818-dcf4-473b-8580-c1fb4ae17b4b'

Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)
#
today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
two_months_ago = date.today() + relativedelta(months=-2)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
two_months_ago = two_months_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')


conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, two_months_ago=two_months_ago, three_months_ago=three_months_ago)
#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all_gp.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_gp variable with output
device_all_gp = pd.read_csv('device_all_gp.csv', parse_dates=['time'])

device_all_gp['time'] = pd.to_datetime(device_all_gp['time'])
device_all_gp['time'] = device_all_gp['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

property_ids = Landlord_properties['Id']
property_id = tuple(property_ids)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE timestamp BETWEEN '2022-10-01' AND '2022-10-31' AND property_id IN {property_id}".format(today=today, one_month_ago=one_month_ago, property_id=property_id)
#query = "SELECT device_id, timestamp, device_type, property_id, reading FROM solar_readings WHERE property_id IN {property_id}".format(today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago, property_id=property_id)

cursor.execute(query, [tuple(property_id)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('device_all_power.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_power_gp = pd.read_csv('device_all_power.csv', parse_dates=['timestamp'])
device_all_power_gp['reading'] = abs(device_all_power_gp['reading']) * 230 / 1000 / 1000 / 2
device_all_power_gp = device_all_power_gp.merge(properties_df, left_on = 'property_id', right_on = 'Id', how = 'left')
device_all_power_gp = device_all_power_gp.merge(addresses_df, left_on='AddressId', right_on='Id', how='left')
device_all_power_gp = device_all_power_gp[['AddressLine1', 'device_id', 'timestamp', 'device_type', 'reading']]
#device_all_power_gp.to_csv('power_gp.csv')

device_all_power_gp = device_all_power_gp.drop(columns = 'device_id')
device_all_power_gp = device_all_power_gp.reset_index()
device_all_power_gp_month = device_all_power_gp.groupby(['AddressLine1', pd.Grouper(key='timestamp', freq='M')]).sum()
device_all_power_gp_week = device_all_power_gp.groupby(['AddressLine1', pd.Grouper(key='timestamp', freq='W')]).sum()
device_all_power_gp_day = device_all_power_gp.groupby(['AddressLine1', pd.Grouper(key='timestamp', freq='D')]).sum()

device_all_power_gp_month = device_all_power_gp_month.reset_index()
device_all_power_gp_month = device_all_power_gp_month.groupby('AddressLine1')['reading'].mean()

device_all_power_gp_week = device_all_power_gp_week.reset_index()
device_all_power_gp_week = device_all_power_gp_week.groupby('AddressLine1')['reading'].mean()

device_all_power_gp_day = device_all_power_gp_day.reset_index()
device_all_power_gp_day = device_all_power_gp_day.groupby('AddressLine1')['reading'].mean()


#device_all_power['device_id'].value_counts()
#power_gp = device_all_power_gp.groupby('AddressLine1')['reading'].agg('mean')
#power_gp = power_gp.loc[power_gp < 0.01]

gp_prop = properties_df.loc[properties_df['LandlordId'] == '4337e818-dcf4-473b-8580-c1fb4ae17b4b']
gp_prop_rooms = gp_prop.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
gp_prop_rooms_sensors = gp_prop_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
gp_prop_rooms_sensors_address = gp_prop_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
gp_prop_rooms_sensors_address['Address'] = gp_prop_rooms_sensors_address['AddressLine1'].map(str) + ' ' + gp_prop_rooms_sensors_address['AddressLine2'].map(str) + ' ' + gp_prop_rooms_sensors_address['City'].map(str) + ' ' + gp_prop_rooms_sensors_address['Code'].map(str)
gp_properties = gp_prop_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference']]

gp_properties_data = gp_properties.merge(device_all_gp, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
#gp_properties_data.to_csv('gp_properties.csv')

gp_properties_data['time'] = gp_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
gp_properties_data['time'] = pd.to_datetime(gp_properties_data['time'])

gp_data_last_month = gp_properties_data.loc[(gp_properties_data['time'] >= two_months_ago) & (gp_properties_data['time'] >= three_months_ago)]
gp_data_last_month = gp_data_last_month.rename(columns = {'temperature': 'temperature_1m_avg', 'humidity': 'humidity_1m_avg', 'co2': 'co2_1m_avg', 'battery': 'battery_1m_avg', 'dewpoint': 'dewpoint_1m_avg'})
gp_data_last_month = gp_data_last_month[['Address', 'Name', 'device', 'temperature_1m_avg', 'humidity_1m_avg', 'co2_1m_avg', 'battery_1m_avg', 'dewpoint_1m_avg']]
gp_data_last_three_months = gp_properties_data.loc[gp_properties_data['time'] >= three_months_ago]
gp_data_last_three_months = gp_data_last_three_months.rename(columns = {'temperature': 'temperature_3m_avg', 'humidity': 'humidity_3m_avg', 'co2': 'co2_3m_avg', 'battery': 'battery_3m_avg', 'dewpoint': 'dewpoint_3m_avg'})
gp_data_last_three_months = gp_data_last_three_months[['Address', 'Name', 'device', 'temperature_3m_avg', 'humidity_3m_avg', 'co2_3m_avg', 'battery_3m_avg', 'dewpoint_3m_avg']]

gp_averages_1m = gp_data_last_month.groupby(['Address', 'Name', 'device']).mean()
gp_averages_3m = gp_data_last_three_months.groupby(['Address', 'Name', 'device']).mean()
gp_averages_1m = gp_averages_1m.reset_index()
gp_averages_3m = gp_averages_3m.reset_index()

gp_averages = gp_averages_1m.merge(gp_averages_3m, on = 'device', how = 'inner')
gp_averages = gp_averages[['Address_x', 'Name_x', 'temperature_3m_avg', 'temperature_1m_avg', 'humidity_3m_avg', 'humidity_1m_avg', 'co2_3m_avg', 'co2_1m_avg', 'dewpoint_3m_avg', 'dewpoint_1m_avg', 'battery_3m_avg', 'battery_1m_avg']].rename(columns = {'Address_x': 'Address', 'Name_x': 'Name'})
# Creating parameteres for 'Issues' column
conditions = [
    (gp_averages['co2_1m_avg'].ge(600) & gp_averages['temperature_1m_avg'].lt(14)) ,
    (gp_averages['humidity_1m_avg'].ge(70) & gp_averages['temperature_1m_avg'] - gp_averages['dewpoint_1m_avg'].lt(4) & gp_averages['co2_1m_avg'].ge(1500)),
    (gp_averages['humidity_1m_avg'].ge(65) & gp_averages['temperature_1m_avg'] - gp_averages['dewpoint_1m_avg'].lt(5)),
    (gp_averages['co2_1m_avg'].ge(1300)),
    (gp_averages['co2_1m_avg'].lt(500) & gp_averages['temperature_1m_avg'].lt(13)),
    (gp_averages['temperature_1m_avg'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

gp_averages['issue'] = np.select(conditions, choices)

# Creating parameteres for 'Severity' column
conditions = [
    (gp_averages['co2_1m_avg'].ge(1500) | (gp_averages['temperature_1m_avg'].lt(14) & gp_averages['co2_1m_avg'].ge(550)) | gp_averages['humidity_1m_avg'].ge(70)),
    (gp_averages['co2_1m_avg'].ge(1300) | (gp_averages['temperature_1m_avg'].lt(15) & gp_averages['co2_1m_avg'].ge(550)) | gp_averages['humidity_1m_avg'].ge(65))
]
choices = ['critical', 'medium']

gp_averages['severity'] = np.select(conditions, choices)
gp_averages_critical = gp_averages[gp_averages['severity'] == 'critical']

gp_power_df['timestamp'] = device_all_power['timestamp'].dt.strftime('%Y-%m-%d %H:%M:%S')
gp_power_df['timestamp'] = pd.to_datetime(gp_power_df['timestamp']).sort_values()

gp_power_props = pd.merge_asof(gp_properties_data, gp_power_df, left_on = 'device', right_on = 'device_id', left_by = 'time', right_by = 'timestamp')

gp_weather_df = weather_df.loc[(weather_df['code'] == 'Knutsford') | (weather_df['code'] == 'Fleetwood')]
gp_weather_df['time'] = pd.to_datetime(gp_weather_df['time'])
gp_weather_df['time'] = gp_weather_df['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

# Merging env and weather
conditions = [
    (gp_properties_data['Address'].str.contains('Longridge')),
    (gp_properties_data['Address'].str.contains('Shaw')),
    (gp_properties_data['Address'].str.contains('Southfields')),
    (gp_properties_data['Address'].str.contains('Overfields')),
    (gp_properties_data['Address'].str.contains('Edgerton'))
    
]
choices = ['Knutsford', 'Knutsford', 'Knutsford', 'Knutsford', 'Knutsford']

gp_properties_data['area'] = np.select(conditions, choices, default='Fleetwood')
gp_averages['Address'].nunique()

gp_properties_data.to_csv('gp_env_data_0903.csv')

gp_properties_data['time'] = pd.to_datetime(gp_properties_data['time'])
gp_properties_data['time'] = gp_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')

# Resampling Weather for
weather_3h = gp_weather_df.set_index('time').resample('15min').ffill()

# Set time delta
tol = pd.Timedelta('3h')

gp_env_weather = pd.merge_asof(gp_properties_data.sort_values('time'), gp_weather_df.sort_values('time'), on='time', left_by='area', right_by='code', direction='nearest', tolerance=tol)

gp_env_weather = gp_env_weather.rename(columns = {'temp': 'outdoor_temperature', 'humidity_x': 'indoor_humidity', 'temperature': 'indoor_temperature', 'humidity_y': 'outdoor_humidity'})
gp_env_weather = gp_env_weather[['Address', 'Name', 'device', 'time', 'indoor_temperature', 'indoor_humidity', 'outdoor_temperature', 'outdoor_humidity', 'co2']]
gp_env_weather.to_csv('gp_09_03.csv')

gp_18wc_lounge = gp_env_weather.loc[gp_env_weather['device'] == '70B3D55F2000087F'].to_csv('gp_18wc_lounge.csv')

gp_weather_corr = gp_env_weather.groupby('device')[['outdoor_humidity','indoor_humidity']].corr().unstack().iloc[:,1]
    

device_all_gp['device'].nunique()

insights(Great Places)
