# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 13:31:29 2022

@author: tomda
"""

import psycopg2
import psycopg2.extras
import datetime as dt
import datetime
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from dateutil.relativedelta import relativedelta

sensor_batch = pd.read_csv('C:/Users/tomda/Desktop/Misc/edin_sensor_batches.csv')

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine, columns = ['Id', 'PropertyId', 'Name', 'RoomType', 'SensorId', 'RecordState'])
sensors_df = pd.read_sql_table('Sensors', engine, columns = ['Id', 'DeviceEUI', 'RecordState'])
properties_df = pd.read_sql_table('Properties', engine, columns = ['Id', 'LandlordId', 'UniqueReference', 'AddressId', ])
addresses_df = pd.read_sql_table('Addresses', engine, columns = ['Id', 'AddressLine1', 'AddressLine2', 'Code', 'City'])

total_sensors_edin = sensor_batch.merge(sensors_df, left_on = 'Device', right_on = 'DeviceEUI', how = 'left')
total_sensors_edin = total_sensors_edin.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'left')
total_sensors_edin = total_sensors_edin.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
total_sensors_edin = total_sensors_edin[['UniqueReference', 'Name', 'Device', 'Batch', 'RecordState_y']]
total_sensors_edin = total_sensors_edin.rename(columns={'RecordState_y': 'Status'})

conditions = [
    (total_sensors_edin['Status'] == 0),
    (total_sensors_edin['Status'] == 1)
]
choices = ['Installed', 'Removed']

total_sensors_edin['Status'] = np.select(conditions, choices, default = 'nan')

#################

device = tuple(sensor_batch['Device'])

today = datetime.datetime.today()
one_week_ago = (today + relativedelta(weeks=-1)).strftime('%Y-%m-%d')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{one_week_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_week_ago=one_week_ago)
query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '2022-12-26 00:00:00' AND '2023-01-05 00:00:00' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_week_ago=one_week_ago)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_ren variable with output
device_all_edin = pd.read_csv('device_all.csv', parse_dates=['time'])
#device_all_edin['device'].nunique()

#device_all_edin = device_all_edin.drop_duplicates('device')
#device_all_edin = device_all_edin[['device']]

'''
#Delete after using
three_four_am = device_all_edin.merge(total_sensors_edin, left_on = 'device', right_on = 'Device', how = 'inner')
three_four_am = three_four_am[['UniqueReference', 'Name', 'Device', 'time', 'co2', 'temperature', 'humidity']]
three_four_am.to_csv('Readings 26th Dec 2022 3am - 4am.csv')
'''

'''
abc = device_all_edin.loc[(device_all_edin['co2'] > 530) | (device_all_edin['co2'] < 370)]
abc['device'].nunique()
device_all_edin['device'].nunique()
'''

'''
test = device_all_edin.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI', how = 'left')
test = test.merge(rooms_df, left_on = 'Id', right_on = 'SensorId', how = 'left')
test = test.merge(properties_df, left_on = 'PropertyId', right_on = 'Id', how = 'left')
test = test[['UniqueReference', 'Name', 'device', 'time', 'temperature', 'co2']]
test['UniqueReference'] = test['UniqueReference'].astype(str)
test = test.loc[(test['UniqueReference'].str.contains('Leith Walk')) | (test['UniqueReference'].str.contains('Balgreen'))]
#test = test.drop_duplicates('device', keep = 'last')
two_to_three = test.loc[(test['time'] > '2022-11-21 02:58:00') & (test['time'] < '2022-11-21 03:30:00')]
two_to_three.to_csv('leith_balgreen_co2.csv')
test.to_csv('all_readings_12th_nov_2.45_3.15.csv')
'''

#test = total_sensors_edin.merge(device_all_edin, left_on = 'Device', right_on = 'device', how = 'inner')
#test.to_csv('1203_sensors.csv')

device_all_edin_edit = device_all_edin[['device', 'time', 'temperature', 'co2']]

conditions = [
    (device_all_edin_edit['temperature'] == -40),
    ((device_all_edin_edit['co2'] > 530) | (device_all_edin_edit['co2'] < 370)),
]
choices = ['Original', 'Calibration']

device_all_edin_edit['Fault_New'] = np.select(conditions, choices, default = 'None')

device_all_edin_edit['total_count'] = device_all_edin_edit.groupby('device')['device'].transform('count')
faults = device_all_edin_edit.groupby(['device', 'Fault_New']).count().reset_index()
faults = faults[['device', 'total_count']].rename(columns = {'total_count': 'fault_count'})
device_all_edin_edit_faults = device_all_edin_edit.merge(faults, on = 'device')
device_all_edin_edit_faults['percentage'] = device_all_edin_edit_faults['fault_count'] / device_all_edin_edit_faults['total_count'] *100

issue_sensors = device_all_edin_edit_faults[device_all_edin_edit_faults['percentage'].between(5, 100)]
issue_sensors = issue_sensors.loc[(issue_sensors['Fault_New'] == 'Original') | (issue_sensors['Fault_New'] == 'Calibration')] 
issue_sensors = issue_sensors.drop_duplicates(['device', 'Fault_New'], keep = 'first')
issue_sensors = issue_sensors[['device', 'Fault_New']]
issue_sensors.groupby('Fault_New')['device'].count()

day_9 = issue_sensors.merge(device_all_edin, on = 'device', how = 'left')
day_9.to_csv('Sensors under 370ppm - 9 Day View.csv')

old_faults = pd.read_excel("C:/Users/tomda/Downloads/Edinburgh School Sensor Audit 10-11-2022 (2).xlsx")

old_new_faults = old_faults.merge(issue_sensors, left_on = 'Device', right_on = 'device', how = 'right')

final = total_sensors_edin.merge(old_new_faults, left_on = 'Device', right_on = 'device', how = 'left')
final = final[['UniqueReference', 'Name', 'Device_x', 'Batch_x', 'Status_x', 'Error Code', 'Fault_New']]
test = final.groupby('Batch_x')['Status_x'].value_counts().unstack()
