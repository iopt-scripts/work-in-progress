# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 13:41:03 2021

@author: JSLATER
"""

#convert csv tables to python pandas df


import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

properties_df.loc[properties_df['LandlordId'] == '0f1ca92f-e5e9-419b-bb05-a64921f9f044']

propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')

# Anchor Hanover
LandlordId = '0f1ca92f-e5e9-419b-bb05-a64921f9f044'

Landlord =  out['LandlordId'] == LandlordId
Landlord_properties = out[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '01/08/2022 00:00:00' and '02/08/2022 0:00:00' AND device IN ('70B3D55F200008D8', '70B3D55F200008EC', '70B3D55F200008EB', '70B3D55F200008F4', '70B3D55F200008E2', '70B3D55F200008DE', '70B3D55F200008F0', '70B3D55F200008F5', '70B3D55F200008E7', '70B3D55F200008D7', '70B3D55F200008E5', '70B3D55F200008DA') AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_df variable with output
device_all_df = pd.read_csv('device_all.csv', parse_dates=['time'])

#devices.to_csv("ren_devices_29_11_2021.csv", index=False)
#ah_rooms_propertysurveys_id = ah_rooms.merge(propertysurveys_df, on = 'PropertyId', how = 'inner')

ah = properties_df.loc[properties_df['LandlordId'] == '0f1ca92f-e5e9-419b-bb05-a64921f9f044']
ah_rooms = ah.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
ah_rooms_sensors = ah_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
ah_rooms_sensors_address = ah_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
ah_rooms_sensors_address['Address'] = ah_rooms_sensors_address['AddressLine1'].map(str) + ' ' + ah_rooms_sensors_address['AddressLine2'].map(str) + ' ' + ah_rooms_sensors_address['City'].map(str) + ' ' + ah_rooms_sensors_address['Code'].map(str)
ah_properties = ah_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

ah_properties_data = ah_properties.merge(device_all_df, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')

ah_averages = ah_properties_data.loc[ah_properties_data['time'] > '01/08/2022 00:00:00']
ah_averages = ah_averages.groupby(['Address', 'Name']).mean()

conditions = [
    (ah_averages['co2'].ge(600) & ah_averages['temperature'].lt(14)),
    (ah_averages['humidity'].ge(70) & ah_averages['temperature'] - ah_averages['dewpoint'].lt(4) & ah_averages['co2'].ge(1500)),
    (ah_averages['humidity'].ge(65) & ah_averages['temperature'] - ah_averages['dewpoint'].lt(5)),
    (ah_averages['co2'].ge(1300)),
    (ah_averages['co2'].lt(500) & ah_averages['temperature'].lt(13)),
    (ah_averages['temperature'].ge(27))   
]
choices = ['fuel poverty / heating issue', 'mould', 'damp', 'poor vent', 'low temp', 'high temp']

ah_averages['issue'] = np.select(conditions, choices)

# Temp Probe properties
ah_env_data = ah_properties_data
ah_env_data['time'] = ah_env_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
ah_env_data['time'] = pd.to_datetime(ah_env_data['time'])

weather_df = weather_df.loc[weather_df['code'] == 'London']
weather_df['time'] = weather_df['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
weather_df['time'] = pd.to_datetime(weather_df['time'])
# Resample from 3h to 15mins to match env sensors
weather_df = weather_df.set_index('time').resample('15min').ffill()

# Set time delta
tol = pd.Timedelta('15 minute')
ah_weather_env = pd.merge_asof(ah_env_data.sort_values('time'), weather_df, on = 'time', direction = 'nearest', tolerance = tol)
ah_weather_env = ah_weather_env[['Address', 'Name', 'device', 'time', 'humidity_x', 'humidity_y']].rename(columns={"humidity_x": "internal_humidity", "humidity_y": "external humidity"})
ah_weather_env.to_csv('ah_weather_env.csv')

ah_weather_env.groupby('device').corr(method = 'pearson')


wl = properties_df.loc[properties_df['LandlordId'] == '0f1ca92f-e5e9-419b-bb05-a64921f9f044']
wl_rooms = wl.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
wl_rooms_sensors = wl_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
wl_rooms_sensors_address = wl_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
wl_rooms_sensors_address['Address'] = wl_rooms_sensors_address['AddressLine1'].map(str) + ' ' + wl_rooms_sensors_address['AddressLine2'].map(str) + ' ' + wl_rooms_sensors_address['City'].map(str) + ' ' + wl_rooms_sensors_address['Code'].map(str)
wl_properties = wl_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

wl_properties_data = wl_properties.merge(device_all_df, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
wl_properties_data = wl_properties_data.loc[(wl_properties_data['time'] >= '08/01/2022 00:00:00') & (wl_properties_data['time'] >= '08/01/2022 00:00:00')]
wl_properties_data['time'] = wl_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
wl_properties_data['time'] = pd.to_datetime(wl_properties_data['time'])

'''
import pandas_profiling

profile = wl_properties_data.profile_report(title = "wl_properties_data")
profile.to_file(output_file = "wl_properties_data.html")
'''























