
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 31 09:13:35 2022

@author: tomda
"""

# Import Data


import psycopg2
import psycopg2.extras
import datetime as dt
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from dateutil.relativedelta import relativedelta

# File to be read in. List of school names.
# Check this matches UniqueReference in Properties table on db.
# Could be changed to getting properties from landlord.


#Unique_Refs = pd.read_csv('batch_1_and_2_school_names.csv')

schools = pd.read_csv("C:/Users/tomda/Desktop/Misc/full_schools_list.csv")

Unique_Refs_values = schools['School'].values

today = dt.datetime.today()
one_week_ago = (today + relativedelta(weeks=-1)).strftime('%Y-%m-%d')

# Pull lookup tables
# Only pulling required coloumns

engine = create_engine('postgresql://iopt_readonly:b!erHalle22@iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com:5432/iopt')
areas_df = pd.read_sql_table('Areas', engine, columns = ['Id', 'PropertyId', 'Name', 'SensorId'])
sensors_df = pd.read_sql_table('Sensors', engine, columns = ['Id', 'DeviceEUI'])
properties_df = pd.read_sql_table('Properties', engine, columns = ['Id', 'OrganisationId', 'UniqueReference', 'AddressId', ])
addresses_df = pd.read_sql_table('Addresses', engine, columns = ['Id', 'AddressLine1', 'AddressLine2', 'Code', 'City'])

# Get property Ids

print('Finding properties...')

props = pd.DataFrame()

for name in Unique_Refs_values:
    prop_add = properties_df.loc[properties_df['UniqueReference'] == name, ['Id', 'UniqueReference']]
    props = pd.concat([props, prop_add])

props

print('Found', len(props), 'properties.')

# Get list of Devices and readings from property list
print('Collecting sensor data...')
for prop_id in props['Id']: 
    

    out_room = areas_df.query('PropertyId in @prop_id')
    sensors = out_room.loc[:,'SensorId']
    out_sensor = sensors_df.query('Id in @sensors')
    out_sensor['DeviceEUI'].value_counts()
    devices = out_sensor.loc[:,'DeviceEUI']
    device = tuple(devices)
    #print(device)
    
    conn_string = "host='iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com' dbname='iopt' user='iopt_readonly' password='b!erHalle22'"
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)                                                                                                                                    

    if len(device) == 1:
        query = "SELECT device, time, co2, humidity, temperature FROM readings WHERE time BETWEEN '{one_week_ago}' AND '{today}' AND device = '{device[0]}'".format(device=device, one_week_ago=one_week_ago, today=today)
    elif len(device) > 1:
        query = "SELECT device, time, co2, humidity, temperature FROM readings WHERE time BETWEEN '{one_week_ago}' AND '{today}' AND device IN {device}".format(device=device, one_week_ago=one_week_ago, today=today)
                                                                                                         
    cursor.execute(query, [tuple(device)])
    outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
    with open(prop_id + '.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
    conn.close()



# Combine all devices into one dataframe

all_devs = pd.DataFrame()
for prop in props['Id']:
    prop_devs = pd.read_csv(prop +'.csv')
    all_devs = pd.concat([all_devs, prop_devs])

    
print('Collected', len(all_devs), 'readings from', all_devs['device'].nunique(), 'devices.')  


engine = create_engine('postgresql://iopt_readonly:b!erHalle22@iopt-production.cgdnc7xomhkr.eu-west-2.rds.amazonaws.com:5432/iopt')
areas_df = pd.read_sql_table('Areas', engine, columns = ['Id', 'PropertyId', 'Name', 'SensorId'])
sensors_df = pd.read_sql_table('Sensors', engine, columns = ['Id', 'DeviceEUI'])
properties_df = pd.read_sql_table('Properties', engine, columns = ['Id', 'OrganisationId', 'UniqueReference', 'AddressId', ])
addresses_df = pd.read_sql_table('Addresses', engine, columns = ['Id', 'AddressLine1', 'AddressLine2', 'Code', 'City'])
organisations_df =  pd.read_sql_table('Organisations', engine, columns = ['Id', 'OrganisationName'])

# Labelled Data Pull

labelled_schools_data = all_devs.merge(sensors_df, left_on = 'device', right_on = 'DeviceEUI')
labelled_schools_data = labelled_schools_data.merge(areas_df, left_on = 'Id', right_on = 'SensorId')
labelled_schools_data = labelled_schools_data.merge(properties_df, left_on = 'PropertyId', right_on = 'Id')
labelled_schools_data = labelled_schools_data[['UniqueReference', 'Name', 'time', 'temperature']]
labelled_schools_data = labelled_schools_data.loc[labelled_schools_data['temperature'] != -40]
labelled_schools_data.to_csv('weeks_temperature_data_to_27oct.csv', index = False)

#Diagnostics 

schools = pd.read_csv('C:/Users/tomda/Desktop/Misc/edin_school_audit.csv')

schools_merge = schools.merge(properties_df, left_on = 'School', right_on = 'UniqueReference', how = 'left')
schools_merge = schools_merge.merge(areas_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
schools_merge = schools_merge.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
schools_final = schools_merge[['UniqueReference', 'Name', 'DeviceEUI', 'Batch']]
#schools_final.to_csv('edin_schools_master.csv')

schools_null = schools_final.loc[schools_final['Name'].isnull()]

schooldevice = schools_final[['DeviceEUI']].dropna()
schooldevice = schooldevice.loc[:,'DeviceEUI']
schooldevice = tuple(schooldevice)

import_school_audit = pd.read_excel('C:/Users/tomda/Downloads/edin_school_audit.xlsx')
schools_offline_status = pd.read_csv("C:/Users/tomda/Downloads/schools_offline_status.csv", usecols = ['status','device'])

import_audit_status = import_school_audit.merge(schools_offline_status, left_on = 'Device', right_on = 'device', how = 'left')
online_devs = all_devs.loc[all_devs['time'] > '2022-09-06 09:00:00+00']
online_devs = online_devs['device'].drop_duplicates()
online_devs = online_devs.to_frame()
online_devs['status'] = 'online'
import_audit_status_on_off = import_audit_status.merge(online_devs, left_on = 'Device', right_on = 'device', how = 'left')
import_audit_status_on_off.to_csv('online_offline_edin_schools.csv')

online_offline_edin_schools = pd.read_csv("C:/Users/tomda/Downloads/online_offline_edin_schools.csv")
online_offline_edin_schools['Coverage Status'].value_counts()

#### Need to update this each time #####
#sensor_status_check = pd.read_csv('sensor_status_check.csv')
#online_offline_edin_schools = online_offline_edin_schools.merge(sensor_status_check, left_on = 'Device', right_on = 'device', how = 'left')

fault_status = pd.read_excel('C:/Users/tomda/Downloads/faulty_sensors.xlsx')
online_offline_edin_schools = online_offline_edin_schools.merge(fault_status, left_on = 'Device', right_on = 'device', how = 'left')
online_offline_edin_schools['fault '] = online_offline_edin_schools['fault '].replace(np.nan, "no")
#online_offline_edin_schools.to_csv('edin_schools_audit_09_09_22.csv')
online_offline_edin_schools = online_offline_edin_schools[['School', 'Room', 'Device', 'Batch', 'Coverage Status', 'fault ']]
school_status_count = online_offline_edin_schools.pivot_table(index=['School'], columns=['fault '], aggfunc='size', fill_value=0)
school_status_count.to_csv('schools_status_count.csv')

import_school_audit = import_school_audit.merge(all_devs, left_on = 'Device', right_on = 'device', how = 'left')

schools_final_w_data_and_conditions = schools_final_w_data

conditions = [
    ((schools_final_w_data_and_conditions['temperature'] == -40) | (schools_final_w_data_and_conditions['humidity'] == 0) | (schools_final_w_data_and_conditions['temperature'].lt(0)) | (schools_final_w_data_and_conditions['temperature'].ge(40)) | (schools_final_w_data_and_conditions['humidity'].lt(15)) | (schools_final_w_data_and_conditions['co2'].ge(5001)))
]

choices = ['yes']

schools_final_w_data_and_conditions['error'] = np.select(conditions, choices, default='no')

schools_error_count = schools_final_w_data_and_conditions.groupby(['UniqueReference', 'Name', 'Batch', 'device', 'error']).count()
schools_error_count = schools_error_count.reset_index()

schools_error_count = schools_error_count.sort_values('DeviceEUI', ascending=False).drop_duplicates(['UniqueReference', 'Name'])
schools_error_count = schools_error_count.sort_values(['UniqueReference', 'Name'])
schools_error_count = schools_error_count [['UniqueReference', 'Batch', 'Name', 'device', 'error']]
schools_error_count.rename(columns={'UniqueReference': 'School', 'Name': 'Room', 'error': 'Error'}, inplace=True)
schools_error_count['Error'].value_counts()  

#school_count = schools_final.groupby(['UniqueReference']).count()
#school_count.to_csv('school_count.csv')

#schools_final.to_csv('schools_export.csv')

