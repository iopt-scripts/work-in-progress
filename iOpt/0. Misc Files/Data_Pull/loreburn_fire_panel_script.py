# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 09:32:10 2022

@author: tomda
"""

today = datetime.datetime.today()
one_week_ago = date.today() + relativedelta(weeks=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_week_ago = one_week_ago.strftime('%Y-%m-%d %H:%M:%S')

# Fire Door Data

device_alerts = pd.read_csv('C:/Users/tomda/Downloads/iOptAlerts.csv')
open_close_count = pd.read_csv('C:/Users/tomda/Downloads/iOptHeartbeats.csv')
fire_panel_niths = pd.read_csv('C:/Users/tomda/Downloads/Nithsdale Mills.csv')
fire_panel_gifhorn = pd.read_csv('C:/Users/tomda/Downloads/Gifhorn House.csv')


conditions = [
    (device_alerts['State'] == 1),
    (device_alerts['State'] == 2)
]
choices = ['Ajar', 'Wedge Open']

device_alerts.to_csv('device_alerts.csv')

device_alerts['State_Word'] = np.select(conditions, choices)

conditions = [
    (device_alerts['PartitionKey'] == '70B3D51C200000F0'),
    (device_alerts['PartitionKey'] == '70B3D51C200000F2'),
    (device_alerts['PartitionKey'] == '70B3D51C200000F3'),
    (device_alerts['PartitionKey'] == '70B3D51C2000003F'),
    (device_alerts['PartitionKey'] == '70B3D51C20000058'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005B'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005E'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005F')
]
choices = ['Nithsdale Mills Ground Floor West External Fire Door (Car Park facing)', 'Nithsdale Mills Ground Floor South External Fire Door', 'Nithsdale Mills Ground Floor South External Fire Door', 'Nithsdale Mills Ground Floor South External Fire Door', 'Gifhorn House Door 2-4 Rear External Fire Door Basement', 'Gifhorn House Door 2-4 Rear External Fire Door Basement', 'Gifhorn House Door 1-9 Rear External Fire Door Basement', 'Gifhorn House Office Inner Door']

device_alerts['Name'] = np.select(conditions, choices)

conditions = [
    (device_alerts['PartitionKey'] == '70B3D51C200000F0'),
    (device_alerts['PartitionKey'] == '70B3D51C200000F2'),
    (device_alerts['PartitionKey'] == '70B3D51C200000F3'),
    (device_alerts['PartitionKey'] == '70B3D51C2000003F'),
    (device_alerts['PartitionKey'] == '70B3D51C20000058'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005B'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005E'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005F')
]
choices = ['Nithsdale Mills', 'Nithsdale Mills', 'Nithsdale Mills', 'Gifhorn House', 'Gifhorn House', 'Gifhorn House', 'Gifhorn House', 'Gifhorn House']
    
device_alerts['Building'] = np.select(conditions, choices)

conditions = [
    (device_alerts['PartitionKey'] == '70B3D51C200000F0'),
    (device_alerts['PartitionKey'] == '70B3D51C200000F2'),
    (device_alerts['PartitionKey'] == '70B3D51C200000F3'),
    (device_alerts['PartitionKey'] == '70B3D51C2000003F'),
    (device_alerts['PartitionKey'] == '70B3D51C20000058'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005B'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005E'),
    (device_alerts['PartitionKey'] == '70B3D51C2000005F')
]
choices = ['West External Fire Door (Car Park facing)', 'South External Fire Door', 'Shell Garage Internal Fire Door', 'Cleaners Cupboard', 'Rear External Fire Door Basement', 'Office Outer Door', 'Door 1-9 Rear External Fire Door', 'Office Inner Door']
    
device_alerts['Door'] = np.select(conditions, choices)


device_alerts.to_csv('device_alerts.csv')



open_close_count = open_close_count.sort_values(['PartitionKey', 'Timestamp'])
open_close_count = open_close_count.loc[open_close_count['Timestamp'] >= one_week_ago]
open_close_count['Count'] = open_close_count['RawCount'].diff()
open_close_count = open_close_count.loc[(open_close_count['Count'] >= 0) & (open_close_count['Count'] < 100)]

open_close_count['State_Word'] = np.select(conditions, choices)

conditions = [
    (open_close_count['PartitionKey'] == '70B3D51C200000F0'),
    (open_close_count['PartitionKey'] == '70B3D51C200000F2'),
    (open_close_count['PartitionKey'] == '70B3D51C200000F3'),
    (open_close_count['PartitionKey'] == '70B3D51C2000003F'),
    (open_close_count['PartitionKey'] == '70B3D51C20000058'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005B'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005E'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005F')
]
choices = ['Nithsdale Mills Ground Floor West External Fire Door (Car Park facing)', 'Nithsdale Mills Ground Floor South External Fire Door', 'Nithsdale Mills Ground Floor South External Fire Door', 'Nithsdale Mills Ground Floor South External Fire Door', 'Gifhorn House Door 2-4 Rear External Fire Door Basement', 'Gifhorn House Door 2-4 Rear External Fire Door Basement', 'Gifhorn House Door 1-9 Rear External Fire Door Basement', 'Gifhorn House Office Inner Door']

open_close_count['Name'] = np.select(conditions, choices)

conditions = [
    (open_close_count['PartitionKey'] == '70B3D51C200000F0'),
    (open_close_count['PartitionKey'] == '70B3D51C200000F2'),
    (open_close_count['PartitionKey'] == '70B3D51C200000F3'),
    (open_close_count['PartitionKey'] == '70B3D51C2000003F'),
    (open_close_count['PartitionKey'] == '70B3D51C20000058'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005B'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005E'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005F')
]
choices = ['Nithsdale Mills', 'Nithsdale Mills', 'Nithsdale Mills', 'Gifhorn House', 'Gifhorn House', 'Gifhorn House', 'Gifhorn House', 'Gifhorn House']
    
open_close_count['Building'] = np.select(conditions, choices)

conditions = [
    (open_close_count['PartitionKey'] == '70B3D51C200000F0'),
    (open_close_count['PartitionKey'] == '70B3D51C200000F2'),
    (open_close_count['PartitionKey'] == '70B3D51C200000F3'),
    (open_close_count['PartitionKey'] == '70B3D51C2000003F'),
    (open_close_count['PartitionKey'] == '70B3D51C20000058'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005B'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005E'),
    (open_close_count['PartitionKey'] == '70B3D51C2000005F')
]
choices = ['West External Fire Door (Car Park facing)', 'South External Fire Door', 'Shell Garage Internal Fire Door', 'Cleaners Cupboard', 'Rear External Fire Door Basement', 'Office Outer Door', 'Door 1-9 Rear External Fire Door', 'Office Inner Door']
    
open_close_count['Door'] = np.select(conditions, choices)


open_close_count.to_csv('open_close_count.csv')



fire_panel_niths['Date and Time'] = pd.to_datetime(fire_panel_niths['Date and Time'])
fire_panel_gifhorn['Date and Time'] = pd.to_datetime(fire_panel_gifhorn['Date and Time'])
fire_panel_niths = fire_panel_niths.loc[fire_panel_niths['Date and Time'] >= one_week_ago]
fire_panel_gifhorn = fire_panel_gifhorn.loc[fire_panel_gifhorn['Date and Time'] >= one_week_ago]
fire_panel_niths.to_csv('fire_panel_niths.csv')
fire_panel_gifhorn.to_csv('fire_panel_gifhorn.csv')


 