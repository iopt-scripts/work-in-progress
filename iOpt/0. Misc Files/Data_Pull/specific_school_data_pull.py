# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 17:21:57 2022

@author: tomda
"""

import psycopg2
import psycopg2.extras
import datetime
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
from sklearn.cluster import DBSCAN
from collections import Counter
from datetime import date
from dateutil.relativedelta import relativedelta

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

engine = create_engine('postgresql://jack:col)Jam86@35.177.25.126/iopt')
weather_df = pd.read_sql_table('weather', engine)

# Not all properties have surveys so can't use this
'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# 1. Remove comments on LandlordId you require.

# brunstane
LandlordId = '89b35faf-1af0-4705-bba3-b3e3b36444d5'


Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:%S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{three_months_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, three_months_ago=three_months_ago)
query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()


device_all_brunstane = pd.read_csv('device_all.csv', parse_dates=['time'])



brunstane = properties_df.loc[properties_df['LandlordId'] == '89b35faf-1af0-4705-bba3-b3e3b36444d5']
brunstane_rooms = brunstane.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
brunstane_rooms_sensors = brunstane_rooms.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
brunstane_rooms_sensors_address = brunstane_rooms_sensors.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
brunstane_rooms_sensors_address['Address'] = brunstane_rooms_sensors_address['AddressLine1'].map(str) + ' ' + brunstane_rooms_sensors_address['AddressLine2'].map(str) + ' ' + brunstane_rooms_sensors_address['City'].map(str) + ' ' + brunstane_rooms_sensors_address['Code'].map(str)
brunstane_properties = brunstane_rooms_sensors_address[['Address', 'Name', 'DeviceEUI', 'UniqueReference', 'PropertyId']]

brunstane_properties_data = brunstane_properties.merge(device_all_brunstane, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')

brunstane_properties_data['time'] = brunstane_properties_data['time'].dt.strftime('%Y-%m-%d %H:%M:%S')
brunstane_properties_data['time'] = pd.to_datetime(brunstane_properties_data['time'])

brunstane_properties_data = brunstane_properties_data[['UniqueReference', 'Name', 'time', 'temperature', 'co2', 'humidity', 'battery', 'dewpoint']]

brunstane_properties_data.to_csv('brunstane_data.csv')
    
