# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 11:08:32 2022

@author: tomda

This script allows you to pull sensor data against a specific landlord
"""
from sqlalchemy import create_engine
import psycopg2
import psycopg2.extras
import pandas as pd


engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')


# 1. Remove comments on LandlordId you require.

# Renfrewshire
LandlordId = '26bf6bb2-9f69-4596-adb2-6c7b82137cd1'
# Cornwall
#LandlordId = 'b10f0221-ed28-4dc1-bcef-9f0853bd75e7'
# Maryhill
#LandlordId = '01e96558-1e52-480b-9a4e-4bc955f83489'
# Great Places
#LandlordId = '4337e818-dcf4-473b-8580-c1fb4ae17b4b'
# Loreburn
#LandlordId = '8fb0fac8-ea47-480d-95ba-c6e1c8be16cd'
# Lewes | Eastbourne
#LandlordId = '2d4afd59-9599-461d-9bca-687d82fb72f7'
# NLC
#LandlordId = 'db8ad941-a28c-4a71-958d-a1dcba23f568'

Landlord = out['LandlordId'] == LandlordId
Landlord_properties = out[Landlord]

# create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:, 'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:, 'DeviceEUI']
device = tuple(devices)
renfrewshire_prop = properties_df.loc[properties_df['LandlordId']
                                      == '26bf6bb2-9f69-4596-adb2-6c7b82137cd1']
renfrewshire_prop_rooms = renfrewshire_prop.merge(
    rooms_df, left_on='Id', right_on='PropertyId', how='inner')
renfrewshire_prop_rooms_sensors = renfrewshire_prop_rooms.merge(
    sensors_df, left_on='SensorId', right_on='Id', how='inner')
renfrewshire_prop_rooms_sensors_address = renfrewshire_prop_rooms_sensors.merge(
    addresses_df, left_on='AddressId', right_on='Id', how='left')
renfrewshire_prop_rooms_sensors_address['Address'] = renfrewshire_prop_rooms_sensors_address['AddressLine1'].map(str) + ' ' + renfrewshire_prop_rooms_sensors_address['AddressLine2'].map(
    str) + ' ' + renfrewshire_prop_rooms_sensors_address['City'].map(str) + ' ' + renfrewshire_prop_rooms_sensors_address['Code'].map(str)
renfrewshire_properties = renfrewshire_prop_rooms_sensors_address[[
    'Address', 'Name', 'DeviceEUI']]
conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '01/08/2021 00:00:00' and '01/01/2022 00:00:00' AND device IN {device} AND NOT (co2 = 65535)".format(
    device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print(cursor)
with open('/Users/williamtudor/work-in-progress/work-in-progress-1/iOpt/Data/output/device_all.csv', 'w') as f:
    cursor.copy_expert(outputquery, f)

conn.close()

device_all_df = pd.read_csv(
    '/Users/williamtudor/work-in-progress/work-in-progress-1/iOpt/Data/output/device_all.csv', parse_dates=['time'])

device_all_df = device_all_df.merge(
    renfrewshire_properties, left_on='device', right_on='DeviceEUI', how='left')
list_of_fuel_pov = ["0 1/1 47C SYCAMORE AVENUE JOHNSTONE CASTLE JOHNSTONE PA5 0BN",
                    "0 1/1 ST JAMES COOP 9C UNDERWOOD LANE PAISLEY   PA1 2SL",
                    "0 1/2 10D MITCHELL AVENUE MOORPARK RENFREW PA4 8DU",
                    "0 12D   GEORGE COURT PAISLEY   PA1 2JS",
                    "0 2/2 2F MITCHELL AVENUE MOORPARK RENFREW PA4 8DT",
                    "0 23B  MACDOWALL STREET JOHNSTONE  PA5 8QH",
                    "0 4E  WALKINSHAW STREET JOHNSTONE  PA5 8AB",
                    "0 9E  GALLOWHILL COURT GALLOWHILL PAISLEY PA3 4NL",
                    "1 1/2 COTTON AVENUE LINWOOD  PA3 3EA",
                    "11 G/1  CLAVERING STREET EAST PAISLEY  PA1 2PH",
                    "11 G/1  GEORGE STREET PAISLEY  PA1 2HU",
                    "110 G/1  BREDILAND ROAD FOXBAR PAISLEY PA2 0HG",
                    "18 G/1  SANDHOLES STREET PAISLEY  PA1 2EQ",
                    "2 HARTFIELD TERRACE HUNTERHILL PAISLEY PA2 6UE",
                    "23     RENFIELD STREET RENFREW  PA4 8RN",
                    "25 G/1  MONTGOMERY ROAD GALLOWHILL PAISLEY PA3 4PP",
                    "267 GREENEND AVENUE JOHNSTONE  PA5 0LG",
                    "3 G/1   MORVEN AVENUE GLENBURN PAISLEY PA2 8DS",
                    "4 G/2  LEITCHLAND ROAD FOXBAR PAISLEY PA2 0HB",
                    "54 1/2 MAXWELLTON ROAD PAISLEY  PA1 2RF",
                    "57 BARGARRON DRIVE GALLOWHILL PAISLEY PA3 4LH",
                    "7 G/2  AUCHENTORLIE QUADRANT PAISLEY  PA1 1QY",
                    "85     BRIDGE OF WEIR ROAD LINWOOD  PA3 3ER"]

# 3. Filter device_all_df by required devices
pos_fuel_pov = []
k = 1
for i in list_of_fuel_pov:
    X = device_all_df.loc[device_all_df['Address'] == i]
    X = X.dropna()
    if len(X.index) > 9000:
        name = "iOpt/Data/Fuel_pov_data/fuel_pov_pos_raw/" + str(k) + ".csv"
        X.to_csv(name)
        k += 1
    #pos_fuel_pov.append(device_all_df.loc[device_all_df['address'] == i])

# occupied_1 = device_all_df.loc[(device_all_df['device'] == '70B3D55F20000166') | (device_all_df['device'] == '70B3D55F200002D7') |
#                               (device_all_df['device'] == '70B3D55F20000338')]
# occupied_1.to_csv('occupied_1.csv')
#
# occupied_2 = device_all_df.loc[(device_all_df['device'] == '70B3D55F20000315') | (
#    device_all_df['device'] == '70B3D55F200002FA')]
# occupied_2.to_csv('occupied_2.csv')
#
#occupied_3 = device_all_df.loc[(device_all_df['device'] == '70B3D55F20000311')]
# occupied_3.to_csv('occupied_3.csv')
#
# occupied_4 = device_all_df.loc[(device_all_df['device'] == '70B3D55F200002C4') | (device_all_df['device'] == '70B3D55F200002A6') |
#                               (device_all_df['device'] == '70B3D55F20000194')]
# occupied_4.to_csv('occupied_4.csv')
#
# occupied_5 = device_all_df.loc[(device_all_df['device'] == '70B3D55F200005E9') | (
#    device_all_df['device'] == '70B3D55F20000640')]
# occupied_5.to_csv('occupied_5.csv')
#
#
# void_1 = device_all_df.loc[(device_all_df['device'] == '70B3D55F20000848') | (device_all_df['device'] == '70B3D55F2000083F') |
#                           (device_all_df['device'] == '70B3D55F2000083E')]
# void_1.to_csv('void_1.csv')
#
# void_2 = device_all_df.loc[(device_all_df['device'] == '70B3D55F20000307') | (
#    device_all_df['device'] == '70B3D55F2000030E')]
# void_2.to_csv('void_2.csv')
#
# void_3 = device_all_df.loc[(device_all_df['device'] == '70B3D55F2000067F') | (device_all_df['device'] == '70B3D55F2000065B') |
#                           (device_all_df['device'] == '70B3D55F20000676')]
# void_3.to_csv('void_3.csv')
#
# void_4 = device_all_df.loc[(device_all_df['device'] == '70B3D55F20000520') | (
#    device_all_df['device'] == '70B3D55F200004FF')]
# void_4.to_csv('void_4.csv')
#
# void_5 = device_all_df.loc[(device_all_df['device'] == '70B3D55F20000153') | (
#    device_all_df['device'] == '70B3D55F2000016D')]
# void_5.to_csv('void_5.csv')
