import pandas as pd
from peak_detection import thresholding_algo
import datetime as dt
import os
import matplotlib.pyplot as plt
# Find timestamps for peaks
directory = "/Users/williamtudor/work-in-progress/iOpt/Data/Occupancy_Data"
readings = os.listdir(directory)
for reading in readings:
    X = pd.read_csv(directory + "/" + reading)
    X['time'] = pd.to_datetime(X['time']).dt.date
    last_month = (dt.datetime.now() - dt.timedelta(days=30)).date()
    X = X.loc[X['time'] > last_month]
    X_co2 = pd.to_numeric(X['co2'])
    indexes = [i for i in range(len(X_co2.index))]
    X_co2.index = indexes
    peak_detector = thresholding_algo(X_co2, lag=500, threshold=6, influence=0.5)
    no_visits = list(peak_detector['signals']).count(1)
    av_c02 = X_co2.mean()

    if no_visits > 0 and av_c02 < 800:
        print(reading + ": Occasional Occupancy")
    elif av_c02 < 800:
        print(reading + ": Unoccupied")
    else:
        print(reading + ": Occupied")
# find peaks above 4 std

# find av co2

# classify