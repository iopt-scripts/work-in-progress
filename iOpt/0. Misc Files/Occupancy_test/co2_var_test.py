import pandas as pd
import os
data_vac = "iOpt/Data/Occupancy_Data/Vacant"
data_occ = "iOpt/Data/Occupancy_Data/Occupied"
data_vac_csvs = os.listdir(data_vac)
data_occ_csvs = os.listdir(data_occ)
occ_variances = []
vac_variances = []
for data in data_vac_csvs:
    X = pd.read_csv(data_vac + '/' + data)
    devices = X['device'].unique()
    for device in devices:
        X = X.loc[(X['device'] == device)]
        X_co2 = X['co2']
        if len(X_co2) > 0:
            vac_variances.append(X_co2.std())
for data in data_occ_csvs:
    X = pd.read_csv(data_occ + '/' + data)
    devices = X['device'].unique()
    for device in devices:
        X = X.loc[(X['device'] == device)]
        X_co2 = X['co2']
        if len(X_co2) > 0:
            occ_variances.append(X_co2.std())
vac_var_av = sum(vac_variances)/len(vac_variances)
occ_var_av = sum(occ_variances)/len(occ_variances)
print("average occ variance: " + str(occ_var_av))
print("average vac variance: " + str(vac_var_av))
