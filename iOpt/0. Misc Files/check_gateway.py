import pandas as pd
import datetime as dt

property_csv = "Data/Gateway_issue_data/gateway_issues_for_will.csv"

if __name__ == "__main__":
    today = dt.datetime.now().date()
    week_ago = today - dt.timedelta(weeks=1)
    X = pd.read_csv(property_csv)    # csv name for checking
    devices = X['device'].unique()
    X['time'] = pd.to_datetime(X['time'])
    X = X.loc[(X['time'].dt.date > week_ago)]
    #X['relevant'] = X['time'].dt.date()
    issue_devices = []
    for device in devices:
        X_sub = X.loc[X["device"] == device]
        time_sub = list(X_sub.loc[X_sub.isna()['temperature'] == True]['time'])
        for i in range(len(time_sub)):
            if time_sub[i+4]- time_sub[i] <= dt.datetime.timedelta(minutes=90):
                issue_devices.append(device)
    print(issue_devices)

    #
    #
#

