import pandas as pd
import numpy as np
import datetime as dt
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import os
decay_means = {}
# Select directory of csvs you'd like ot analyse, to mitigate outside temperature effects the csvs should be for properties in a similar area
directory = "C:/Users/tomda/Documents/work-in-progress/iOpt/0. Misc Files/Data/Temp_decay_data"

csvs = os.listdir(directory)
# Choose dates for data, would recommend not using summer months
earliest = dt.date(2022, 10, 24)
# as a temperture differential between the outside and inside
latest = dt.date(2022, 11, 25)

# is needed to see heat loss.
address_by_device_id = {}  # CSVs need an address column or there will be an error!
for csv in csvs:    # Iterate through the csvs
    X = pd.read_csv(directory + "/" + csv)    # Get data
    add = X['Room Address'].unique()[0]
    device_ids = X['device_id'].unique()
    X['time'] = pd.to_datetime(X['time'])
    X['time'] = X['time'].dt.tz_localize(None)
    X['date'] = X['time'].dt.date   # Convert data to correct type
    dates = X['date'].unique()

    X = X.loc[(X['time'].dt.date > earliest) & (X['time'].dt.date < latest)]
    data_by_date = []

    for i in device_ids:   # Iterate through each devive in csv
        X_sub = X.loc[X['device_id'] == i]
        # Make dict to point to address from device_ids to easily find problem properties
        address_by_device_id[i] = add
        # Ensure data is arranged by date, may be unnecessary
        X_sub = X_sub.sort_values('time')
        for each_date in dates:     # Iterate through the data one day at a time
            if earliest < each_date < latest:
                X_sub_date = X_sub.loc[X_sub['date'] == each_date]
                X_sub_date = X_sub_date.loc[:, ['time', 'temperature']]
                # Find the peaks in the daily data
                peaks, heights = find_peaks(
                    X_sub_date['temperature'], height=17, distance=10)
                if len(peaks):
                    # The following code finds the first timestamp that is x degrees colder than the peak
                    mark = peaks[0]
                    # It does this by iterating through the temperature data, starting from
                    length_peaks = len(peaks)
                    # The current peak position and continuing until the necessary
                    length_readings = len(X_sub_date.index)
                    for k in range(length_peaks):   # Temp drop has been seen
                        # The mark variable tracks the current data point until one is found that is x degrees cooler
                        mark = peaks[k]
                        mark_temp = X_sub_date.iloc[peaks[k], 1]
                        while (k+1 < length_peaks and mark < peaks[k+1]) or (mark < length_readings):
                            # Change the 2 here to change the drop in temperature measured (corresponds to x above)
                            if X_sub_date.iloc[mark, 1] < X_sub_date.iloc[peaks[k], 1] - 2:
                                mark_temp = X_sub_date.iloc[mark, 1]
                                data_by_date.append({X_sub_date.iloc[peaks[k], 0]: X_sub_date.iloc[peaks[k], 1],
                                                     X_sub_date.iloc[mark, 0]: X_sub_date.iloc[mark, 1]})
                                break
                            mark += 1

        decay_rates = []
        for decays in data_by_date:
            # The following finds the rate at which the temperature falls from each peak in
            times = list(decays.keys())
            # the data and finds an average of these decay rates.
            temp_fall = decays[times[0]] - decays[times[1]]
            # Further work may be needed to remove outliers from scenarios like windows being opened
            time_range = (times[1] - times[0]).total_seconds()/3600
            decay_rate = temp_fall/time_range
            decay_rates.append(decay_rate)
        indexes = [i for i in range(len(decay_rates))]

        decay_rates = np.array(decay_rates)
        decay_rates = decay_rates[~np.isnan(decay_rates)]
        decay_mean = decay_rates.mean()
        if np.isnan(decay_mean) != True:
            decay_means[i] = decay_mean


fast_decay = []
decay_means_list = list(decay_means.values())
decay_mean = np.array(decay_means_list).mean()
decay_std = np.array(decay_means_list).std()

num_stds = 1
for i in list(decay_means.keys()):
    # Increase num_stds value to only isolate more outlying data
    if decay_means[i] > decay_mean + num_stds * decay_std:
        print(decay_means[i])
        fast_decay.append(i)
Y = []


for i in fast_decay:
    # Make a list of addresses corresponding to device_ids with outlying temp decays
    Y.append(address_by_device_id[i])

# Remove duplicate addresses (forming the set removes repeating addresses)
Y = list(set(Y))
Y = {'Addresses': Y}
if fast_decay:
    Y = pd.DataFrame(Y)
    print(Y.head())
    # Choose where to store csv of problem addresses
    Y.to_csv("Exit_Path.csv")
    
       
address_by_device_id = pd.DataFrame(address_by_device_id.items(), columns=['device_id', 'Address'])
decay_means = pd.DataFrame(decay_means.items(), columns=['device_id', 'Heat Decay'])
address_decay_means = address_by_device_id.merge(decay_means, on = 'device_id', how = 'left')

#address_decay_means = address_decay_means.merge(renfrewshire_properties_data, left_on = 'device_id', right_on = 'device_idEUI', how = 'left')
#address_decay_means = address_decay_means[['device_id', 'Address_x', 'Heat Decay', 'Name']].drop_duplicates(['Address_x', 'Name'])

#address_decay_means['Heat Decay'].mean()

address_decay_means.to_csv('heat_decay.csv')
