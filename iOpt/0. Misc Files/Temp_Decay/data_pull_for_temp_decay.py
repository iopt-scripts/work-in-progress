# -*- coding: utf-8 -*-
"""
Created on Fri Feb 18 14:02:56 2022

@author: tomda
"""


import psycopg2
import psycopg2.extras
import datetime 
from datetime import timedelta
import json
import time
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from sqlalchemy import create_engine
import pandasql as ps
from sklearn.cluster import DBSCAN
from collections import Counter
import seaborn as sns
from datetime import date
from dateutil.relativedelta import relativedelta

engine = create_engine('postgresql://jack:!tchyMemory76@3.8.27.220:5432/iopt')
rooms_df = pd.read_sql_table('Rooms', engine)
sensors_df = pd.read_sql_table('Sensors', engine)
properties_df = pd.read_sql_table('Properties', engine)
propertysurveys_df = pd.read_sql_table('PropertySurveys', engine)
addresses_df = pd.read_sql_table('Addresses', engine)

'''
propertysurveys_id = propertysurveys_df['PropertyId']
out = properties_df.query('Id in @propertysurveys_id')
'''

# 1. Remove comments on LandlordId you require.

# Renfrewshire
#LandlordId = '26bf6bb2-9f69-4596-adb2-6c7b82137cd1'
# Cornwall
#LandlordId = 'b10f0221-ed28-4dc1-bcef-9f0853bd75e7'
# Maryhill
#LandlordId = '01e96558-1e52-480b-9a4e-4bc955f83489'
# Great Places
#LandlordId = '4337e818-dcf4-473b-8580-c1fb4ae17b4b'
# Loreburn
#LandlordId = '8fb0fac8-ea47-480d-95ba-c6e1c8be16cd'
# Lewes | Eastbourne
#LandlordId = '2d4afd59-9599-461d-9bca-687d82fb72f7'
# NLC
#LandlordId = 'db8ad941-a28c-4a71-958d-a1dcba23f568'
#LiveWest
#LandlordId = 'f7e5e481-e96e-4844-9603-f0b5e6444cd3'

Landlord =  properties_df['LandlordId'] == LandlordId
Landlord_properties = properties_df[Landlord]

#create list if IDs for renfrewshire properties
renfrew_props_id = Landlord_properties['Id']
out_room = rooms_df.query('PropertyId in @renfrew_props_id')
sensors = out_room.loc[:,'SensorId']
out_sensor = sensors_df.query('Id in @sensors')
out_sensor['DeviceEUI'].value_counts()
devices = out_sensor.loc[:,'DeviceEUI']
device = tuple(devices)

today = datetime.datetime.today()
three_months_ago = date.today() + relativedelta(months=-3)
one_month_ago = date.today() + relativedelta(months=-1)
today = today.strftime('%Y-%m-%d %H:%M:%S')
one_month_ago = one_month_ago.strftime('%Y-%m-%d %H:%M:%S')
three_months_ago = three_months_ago.strftime('%Y-%m-%d %H:%M:S')

conn_string = "host='35.177.25.126' dbname='iopt' user='jack' password='col)Jam86'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# 2. Change time frame to desired window

query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE time BETWEEN '{one_month_ago}' AND '{today}' AND device IN {device} AND NOT (co2 = 65535)".format(device=device, today=today, one_month_ago=one_month_ago, three_months_ago=three_months_ago)
#query = "SELECT device, time, temperature, co2, humidity, battery, dewpoint FROM readings WHERE device IN {device} AND NOT (co2 = 65535)".format(device=device)

cursor.execute(query, [tuple(device)])
outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
print (cursor)
with open('device_all.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
        
conn.close()

# Create device_all_df variable with output
device_all_df = pd.read_csv('device_all.csv', parse_dates=['time'])

landlord_prop = properties_df.loc[properties_df['LandlordId'] == LandlordId]
landlord_prop = landlord_prop.merge(rooms_df, left_on = 'Id', right_on = 'PropertyId', how = 'inner')
landlord_prop = landlord_prop.merge(sensors_df, left_on = 'SensorId', right_on = 'Id', how = 'inner')
landlord_prop = landlord_prop.merge(addresses_df, left_on = 'AddressId', right_on = 'Id', how = 'left')
landlord_prop['Address'] = landlord_prop['AddressLine1'].map(str) + ' ' + landlord_prop['AddressLine2'].map(str) + ' ' + landlord_prop['City'].map(str) + ' ' + landlord_prop['Code'].map(str)
landlord_prop = landlord_prop.merge(device_all_df, left_on = 'DeviceEUI', right_on = 'device', how = 'inner')
landlord_prop = landlord_prop[['device', 'time', 'temperature', 'co2', 'humidity', 'battery', 'dewpoint', 'Address', 'Name', 'DeviceEUI']]


for i, g in landlord_prop.groupby('device'):
    g.to_csv('{}.csv'.format(i), header=True, index_label=False)
    
    
    
    
    
    