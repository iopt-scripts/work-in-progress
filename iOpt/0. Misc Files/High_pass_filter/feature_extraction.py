import pandas as pd
import numpy as np
from tsfresh import extract_features
from tsfresh.feature_selection.selection import select_features
from tsfresh.feature_extraction.settings import from_columns
from tsfresh.feature_extraction import ComprehensiveFCParameters
import datetime as dt


def select_feature_params(df):
    """
    Get a dictionary of parameter values for each feature
    so feature extraction for each dataframe uses the same parameters
    """
    print("select_feature_params input df: ", df.head())
    df = df.dropna(axis=1)
    X = df.loc[:, df.columns!='issue']
    y = df['issue']
    X_tsfresh_filtered = select_features(X, y)
    kind_to_fc_parameters = from_columns(X_tsfresh_filtered)
    return kind_to_fc_parameters

def get_features(sub_df, device, feature_dict='default'):
    """
    Extract time series features from a given dataframe,
    whose columns contain time series data
    :param sub_df:
    :param device:
    :param feature_dict:
    :return dataframe of features for each time series in dataframe:
    """
    sub_df = sub_df.drop('device', axis=1)  # Remove the device column as not useful for feature analysis

    sub_df['time'] = pd.to_datetime(sub_df['time']) # Convert types from csv to numeric or datetimes for analysis
    sub_df['temperature'] = pd.to_numeric(sub_df['temperature'])
    sub_df['co2'] = pd.to_numeric(sub_df['co2'])
    sub_df['humidity'] = pd.to_numeric(sub_df['humidity'])
    sub_df['battery'] = pd.to_numeric(sub_df['battery'])
    sub_df['dewpoint'] = pd.to_numeric(sub_df['dewpoint'])
    sub_df['time'] = sub_df['time'].dt.tz_localize(None)

    sub_df['id'] = sub_df['time'].dt.date # Create an ID such that all data taken from each 24h is grouped under 1 ID
    #sub_df['id'] = pd.to_datetime(sub_df['id'])
    sub_df = sub_df.sort_values(by='time', ascending=True) # Chronologically order each 24h time series

    # Extract features from each time series, returning a dataframe indexed by day with columns being the feature values
    if feature_dict == 'default':
        feature_dict = ComprehensiveFCParameters()
        features = extract_features(sub_df, column_id="id", column_sort="time",
                                    column_kind=None, column_value=None, default_fc_parameters=feature_dict)
    else:
        features = extract_features(sub_df, column_id="id", column_sort="time",
                                column_kind=None, column_value=None, kind_to_fc_parameters=feature_dict)
    num_rows_feat = len(features.index)
    labels_array = np.zeros(num_rows_feat)
    update_index = np.array([i for i in range(num_rows_feat)])
    features['id'] = features.index
    features = features.set_index(update_index)

    for i in range(num_rows_feat): # Label the data as having an issue or not
        if dt.date(2020, 11, 1) <= features.loc[i,'id'] <= dt.date(2020, 11, 11) or features.loc[i,'id'] >= dt.date(2021, 2, 1):
            labels_array[i] = 1
        else:
            labels_array[i] = 0
    # Convert id to string so can concatenate with device name to differentiate between devices
    features.index = features['id'].astype("string") + (" " + str(device))
    del features['id']
    # attach labels to denote if a 24h period of data corresponded to an issue
    features['issue'] = labels_array
    print(features.info())
    return features


if __name__ == "__main__":
    df = pd.read_csv("../Data/30Trewlaney_oct20.csv")
    device_names = df['device'].unique()
    features_and_labels = []
    feature_dict = select_feature_params(get_features(df.loc[(df['device'] == device_names[0])], device_names[0]))
    for i in device_names:
        sub_df = df.loc[(df['device'] == i)]
        sub_df = sub_df.dropna()
        features_and_labels.append(get_features(sub_df, i, feature_dict))
    list_of_columns = [i.columns for i in features_and_labels]
    final_data = pd.concat(features_and_labels, axis=0)
    final_data = final_data.dropna()
    # Check if feature selection + parameter dict is necessary for feature columns to be the same across devices


    final_data.to_csv("30_tralawney_features")




    # Export CSV












